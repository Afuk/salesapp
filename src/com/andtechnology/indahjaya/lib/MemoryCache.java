package com.andtechnology.indahjaya.lib;

import java.util.HashMap;
import java.util.Map;

/**
 * Cache class to transfer Objects activity temporary 
 * @author Timotius
 * @date Jul 29, 2011
 * 
 */
public class MemoryCache {
	private static HashMap<String, Object> cache = new HashMap<String, Object>();
	
	/**
	 * Put object to hashmap
	 * @param key
	 * @param value
	 */
	public static void put(String key,Object value) {
		cache.put(key, value);
	}
	
	/**
	 * Get the object and remove or keep the object after that
	 * @param key
	 * @param remove
	 * @return
	 */
	public static Object get(String key,boolean remove) {
		Object value = cache.get(key);
		if(remove && cache.containsKey(key))
			cache.remove(key);
		return value;
	}
	
	public static Object get(String key) {
		Object value = cache.get(key);		
		return value;
	}
	
	public static void clear(String key) {
		cache.remove(key);
	}
	
	/**
	 * Print all objects
	 */
	public static void printAll(){
		for (Map.Entry<String, Object> entry : cache.entrySet()) {
		    String key = entry.getKey();
		    Object value = entry.getValue();
		}
	}
}
