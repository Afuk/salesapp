package com.andtechnology.indahjaya.lib;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.CountDownTimer;

/**
 * http://stackoverflow.com/questions/3145089/what-is-the-simplest-and-most-robust-way-to-get-the-users-current-location-in-a/3145655#3145655
 *
 */
public class MyLocation {
	
	private boolean isNetwork;
	private boolean isGPS;
	LocationManager lm;
	LocationResult locationResult;
	private static LocationTimeout counter;
	
	public MyLocation(Context ctx) {
		lm = (LocationManager) ctx.getSystemService(Context.LOCATION_SERVICE);
	}	
	
	/**
	 * Set what listener will be used
	 * @param isNetwork
	 * @param isGPS
	 */
	public void setListener(boolean isNetwork, boolean isGPS){
		if (isGPS) {
			this.isGPS = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
		}
		if (isNetwork) {
			this.isNetwork = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
		}
	}
	
	/**
	 * Start to get location.
	 * @param minTime
	 * @param minDistance
	 * @param maxTimeSec
	 */
	public void start(long minTime, float minDistance, int maxTimeSec, LocationResult locationResult){
		this.locationResult = locationResult;
		if (isGPS) {
			lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, minTime, minDistance, locationListenerGps);
		}
		if (isNetwork) {
			lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, minTime, minDistance, locationListenerNetwork);
		}
		if (isGPS || isNetwork) {
			counter = new LocationTimeout(maxTimeSec, 1000);
			counter.start();
		} else {
			updateStatus(null);
		}
	}

	private void updateStatus(Location location){
		resetCounter();
		locationResult.gotLocation(location);
		lm.removeUpdates(locationListenerGps);
		lm.removeUpdates(locationListenerNetwork);
	}
	
	public void cancel(){
		updateStatus(null);
	}
	
	class LocationTimeout extends CountDownTimer {
		public LocationTimeout(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
		}

		@Override
		public void onTick(long millisUntilFinished) {
			
		}
		
		@Override
		public void onFinish() {
			updateStatus(null);
		}
	}
	
	private void resetCounter(){
		if (counter!=null) {
			counter.cancel();
		}
	}
	
	//Location Listener
	LocationListener locationListenerGps = new LocationListener() {
		public void onLocationChanged(Location location) {
			updateStatus(location);
		}

		public void onProviderDisabled(String provider) {
		}

		public void onProviderEnabled(String provider) {
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	};

	LocationListener locationListenerNetwork = new LocationListener() {
		public void onLocationChanged(Location location) {
			updateStatus(location);
		}

		public void onProviderDisabled(String provider) {
		}

		public void onProviderEnabled(String provider) {
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	};
	
	//Location Result
	public static abstract class LocationResult {
		public abstract void gotLocation(Location location);
	}
}