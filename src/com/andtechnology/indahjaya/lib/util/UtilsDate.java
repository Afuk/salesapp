package com.andtechnology.indahjaya.lib.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class UtilsDate {

	public static long getTimestamp(String dateTime){
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date parsedDate = dateFormat.parse(dateTime);
			return parsedDate.getTime();
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}			
	}
	
	
	//Date time manipulation
	public static String getDate(long timestamp){		
		return getStringFormat(timestamp, "dd MMM yyyy");				
	}
	
	public static String getHour(long timestamp){
		return getStringFormat(timestamp, "HH:mm");				
	}
	
	public static String getStringFormat(long timestamp){
		return getStringFormat(timestamp, "yyyy-MM-dd HH:mm:ss");
	}
	
	public static String getStringFormat(long timestamp, String format){
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat(format);
			Date parsedDate = new Date(timestamp);
			return dateFormat.format(parsedDate);
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}			
	}
}
