package com.andtechnology.indahjaya.lib.util;

import android.app.Activity;
import android.content.Intent;

import com.andtechnology.indahjaya.ui.Home;
import com.andtechnology.indahjaya.view.ActivityChangePassword;
import com.andtechnology.indahjaya.view.ActivityForgot;
import com.andtechnology.indahjaya.view.ActivityLogin;
import com.andtechnology.indahjaya.view.SplashScreen;

public class UtilsPointer {

	public static void goToLogin(Activity activity) {
		Intent intent = new Intent(activity, ActivityLogin.class);
		activity.startActivityForResult(intent, 901);
	}

	public static void goToForgot(Activity activity) {
		Intent intent = new Intent(activity, ActivityForgot.class);
		activity.startActivityForResult(intent, 902);
	}

	public static void goToMain(Activity activity) {
		Intent intent = new Intent(activity, Home.class);
		activity.startActivityForResult(intent, 903);
	}
	
	public static void goToChangePassword(Activity activity) {
		Intent intent = new Intent(activity, ActivityChangePassword.class);
		activity.startActivityForResult(intent, 904);
	}
	
	public static void goToSplashScreen(Activity activity) {
		Intent intent = new Intent(activity, SplashScreen.class);
		activity.startActivityForResult(intent, 905);
	}
}
