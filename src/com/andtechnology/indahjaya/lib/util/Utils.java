package com.andtechnology.indahjaya.lib.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.BitmapAjaxCallback;
import com.andtechnology.indahjaya.R;

public class Utils {
	
	 
	public final static Bitmap compressImage(String photo_url)
	{
		 Bitmap bmt=null;
		   BitmapFactory.Options options = new BitmapFactory.Options();
		   options.inSampleSize = 2;
		   Bitmap photo = BitmapFactory.decodeFile(photo_url, options);
		   if (photo.getWidth() >= photo.getHeight()) {
			    bmt = photo.createScaledBitmap(photo, 750,
					     (photo.getHeight() * 750) / photo.getWidth(), false);
			   } else {
				   bmt = photo.createScaledBitmap(photo,
			      (photo.getWidth() * 750) / photo.getHeight(), 750,
			      false);
			   }
		  
		   //finalImage = Utils.encodeTobase64(bmt).replace("\n", "").toString();
		   return bmt;
	}
	
	public final static String autoCreateIdModel(String user_id) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String formattedDate = df.format(new Date());

		String last_id = new SimpleDateFormat("yyyyMMddHHmmss",
				Locale.getDefault()).format(new Date());

		last_id += user_id;
		return md5(last_id);
	}

	public static boolean isOnline(Context ctx) {
		ConnectivityManager cm = (ConnectivityManager) ctx
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (cm.getActiveNetworkInfo() != null
				&& cm.getActiveNetworkInfo().isAvailable()
				&& cm.getActiveNetworkInfo().isConnected()) {
			return true;
		} else {
			return false;
		}
	}
	public static final String getTimeNow()
	{
		 SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		 return df.format(new Date());
	}
	public final static int colorParse(String string_color) {
		return Color.parseColor("" + string_color);
	}
	
	public static int getDisplayHeight(Context context) {
		WindowManager wm = (WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		int height = display.getHeight();

		return height;
	}

	public static int getDisplayWidth(Context context) {
		WindowManager wm = (WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		int width = display.getWidth();

		return width;
	}

	public static void loadImageUsingAquery(Activity activity,
			ImageView imageView, String url, int targetWidth) {
		AQuery aQuery = new AQuery(activity);
		aQuery.id(imageView).image(url, true, true, targetWidth, 0);

	}
	public static void loadImageAqueryAutoRotate(Activity activity,
			ImageView imageView, String url, int targetWidth)
	{
		if(url==null)
		{
			imageView.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_camera));
		}
		else
		{
		AQuery aq = new AQuery(activity);
		aq.id(imageView).image(url, true,
				true, targetWidth, 0,
				new BitmapAjaxCallback() {
					protected void callback(String url, ImageView iv,
							Bitmap bm,
							com.androidquery.callback.AjaxStatus status) {
						iv.setImageBitmap(Utils.setFixRotation(url,bm));
					};
				}.header("User-Agent", "android"));
		}
	}
	
	public final static Bitmap setFixRotation(String url,Bitmap bitmap)
	{
		ExifInterface ei;
		int orientation = 0;
		try {
			ei = new ExifInterface(url);
			orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_NORMAL);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		switch (orientation) {
		case ExifInterface.ORIENTATION_ROTATE_90:
			bitmap = RotateBitmap(bitmap, 90);
			break;
		case ExifInterface.ORIENTATION_ROTATE_180:
			bitmap = RotateBitmap(bitmap, 180);
			break;
		// etc.
		}
		return bitmap;
	}
	private static Bitmap RotateBitmap(Bitmap source, float angle) {
		Matrix matrix = new Matrix();
		matrix.postRotate(angle);
		return Bitmap.createBitmap(source, 0, 0, source.getWidth(),
				source.getHeight(), matrix, true);
	}


	public static boolean checkOnline(final Activity a) {
		if (isOnline(a)) {
			return true;
		} else {
			AlertDialog.Builder builder = new AlertDialog.Builder(a);
			builder.setTitle(R.string.internet_failed_title);
			builder.setMessage(R.string.internet_failed_content);
			builder.setCancelable(false);
			builder.setPositiveButton(android.R.string.ok,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							a.finish();
						}
					});
			builder.setCancelable(false);
			AlertDialog alert = builder.create();
			alert.show();
			return false;
		}
	}

	/**
	 * Deleting Images - cache
	 * 
	 * @param ctx
	 * @return
	 */
	public static boolean clearCache(Context ctx) {
		try {
			File cacheDir = ctx.getCacheDir();
			File[] files = cacheDir.listFiles();
			for (File f : files) {
				f.delete();
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Deleting internal database - sqlite
	 * 
	 * @param ctx
	 * @return
	 */
	public static boolean clearDatabase(Context ctx) {
		try {
			// new Customer().deleteAll(ctx);
			// new Job().deleteAll(ctx);
			// new JobDetail().deleteAll(ctx);
			// new Message().deleteAll(ctx);
			// new Product().deleteAll(ctx);
			// new Cart().deleteAll(ctx);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	// public static boolean clearPreference(Context ctx) {
	// User.clear(ctx);
	// try {
	// Editor editor =
	// PreferenceManager.getDefaultSharedPreferences(ctx).edit();
	// editor.clear();
	// editor.commit();
	// return true;
	// } catch (Exception e) {
	// return false;
	// }
	// }
	//
	// public static void clearAll(Context ctx) {
	// clearDatabase(ctx);
	// clearCache(ctx);
	// clearPreference(ctx);
	// SyncManager.reset(ctx);
	// }

	public static void showAlert(Activity a, String title, String errorMessage) {
		AlertDialog.Builder builder = new AlertDialog.Builder(a);
		builder.setTitle(title);
		builder.setMessage(errorMessage);
		builder.setCancelable(false);
		builder.setPositiveButton(R.string.ok,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}

	public static void showToast(Activity a, String message) {
		Toast.makeText(a, message, Toast.LENGTH_SHORT).show();
	}

	// public static void triggerNotification(Context ctx, String title) {
	// triggerNotification(ctx, title, "");
	// }

	// public static void triggerNotification(Context ctx, String title, String
	// message) {
	// NotificationManager notifyMgr = (NotificationManager)
	// ctx.getSystemService(Context.NOTIFICATION_SERVICE);
	// Notification notify = new Notification(R.drawable.ic_launcher, title,
	// System.currentTimeMillis());
	// // PendingIntent to launch our activity if the user selects it
	// PendingIntent i = PendingIntent.getActivity(ctx, 0, new Intent(ctx,
	// ActivityEmpty.class), 0);
	// // Set the info that show in the notification panel
	// notify.setLatestEventInfo(ctx, title, message, i);
	// // Value indicates the current number of events represented by the
	// // notification
	// notify.flags |= Notification.FLAG_AUTO_CANCEL;
	// // Send notification
	// notifyMgr.notify(ConstantUtil.NOTIFICATION_CODE, notify);
	// }
	//
	// public static DisplayImageOptions getDisplayOption() {
	// return new
	// DisplayImageOptions.Builder().showStubImage(R.drawable.no_image).showImageForEmptyUri(R.drawable.no_image)
	// .cacheInMemory().cacheOnDisc().displayer(new
	// RoundedBitmapDisplayer(0)).build();
	// }
	//
	// public static String getThumbnail(String fileImage) {
	// return ConstantNetwork.THUMBS_URL + fileImage;
	// }
	//
	// public static String getImage(String fileImage) {
	// return ConstantNetwork.IMAGE_URL + fileImage;
	// }
	//
	// public static void deleteCart(Context ctx) {
	// MemoryCache.clear(ConstantParameter.CART);
	// new Cart().deleteAll(ctx);
	// }

	/**
	 * http://stackoverflow.com/questions/843675/how-do-i-find-out-if-the-gps-of
	 * -an-android-device-is-enabled
	 * 
	 * @param ctx
	 * @return
	 */
	public static boolean isGPSEnable(Context ctx) {
		LocationManager locationManager = (LocationManager) ctx
				.getSystemService(Context.LOCATION_SERVICE);
		return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
	}

	public static boolean isNetworkEnable(Context ctx) {
		LocationManager locationManager = (LocationManager) ctx
				.getSystemService(Context.LOCATION_SERVICE);
		return locationManager
				.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
	}

	public static Hashtable<String, String> getHashFromJson(String json)
			throws JSONException {
		JSONObject jObject = new JSONObject(json);
		return getHashFromJson(jObject);
	}

	/**
	 * http://stackoverflow.com/questions/4407532/parse-json-object-with-string-
	 * and-value-only
	 * 
	 * @param json
	 * @return
	 * @throws JSONException
	 */
	public static Hashtable<String, String> getHashFromJson(JSONObject object)
			throws JSONException {
		Hashtable<String, String> hash = new Hashtable<String, String>();
		Iterator iter = object.keys();
		while (iter.hasNext()) {
			String key = (String) iter.next();
			String value = object.getString(key);
			hash.put(key, value);
		}
		return hash;
	}

	public static String getCurrency(double value) {
		return getCurrency(value, false);
	}

	public static String getCurrency(double value, boolean isBaseZero) {
		if (value < 1 && isBaseZero) {
			return "";
		}
		NumberFormat nf = NumberFormat.getNumberInstance(Locale.GERMAN);
		DecimalFormat df = (DecimalFormat) nf;
		df.applyPattern("###,###");
		String output = df.format(value);
		return output;
	}

	public static String encodeTobase64(Bitmap image) {
		Bitmap immagex = image;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
		byte[] b = baos.toByteArray();
		String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
		return imageEncoded;
	}

	public static final String md5(final String s) {
		final String MD5 = "MD5";
		try {
			// Create MD5 Hash
			MessageDigest digest = java.security.MessageDigest.getInstance(MD5);
			digest.update(s.getBytes());
			byte messageDigest[] = digest.digest();

			// Create Hex String
			StringBuilder hexString = new StringBuilder();
			for (byte aMessageDigest : messageDigest) {
				String h = Integer.toHexString(0xFF & aMessageDigest);
				while (h.length() < 2)
					h = "0" + h;
				hexString.append(h);
			}
			return hexString.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return "";
	}

	public static float convertDpToPixel(float dp, Context context) {
		Resources resources = context.getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
		float px = dp * (metrics.densityDpi / 160f);
		return px;
	}
	public static float convertPixelsToDp(float px, Context context) {
		Resources resources = context.getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
		float dp = px / (metrics.densityDpi / 160f);
		return dp;
	}
}
