package com.andtechnology.indahjaya.lib.util;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Log;

public class UtilsCamera {

	// directory name to store captured images and videos
	private static final String IMAGE_DIRECTORY_NAME = "Indah_Jaya_Camera";
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_VIDEO = 2;

	/**
	 * Creating file uri to store image/video
	 */
	public static Uri getOutputMediaFileUri(int type, Activity a, String Code) {		
		return Uri.fromFile(getOutputMediaFile(type, a, Code));
	}

	/*
	 * returning image / video
	 */
	private static File getOutputMediaFile(int type, Activity a, String Code) {

		// External sdcard location
		File mediaStorageDir;
		// if(storage == 1) { //hidden directory
		 mediaStorageDir = new
		 File(android.os.Environment.getExternalStorageDirectory(),"Android/data/com.andtechnology.indahjaya/"+IMAGE_DIRECTORY_NAME);
		// } else { //public history
//		mediaStorageDir = new File(
//				Environment
//						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
//				IMAGE_DIRECTORY_NAME);
		// }

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
						+ IMAGE_DIRECTORY_NAME + " directory");
				return null;
			}
		}

		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
				Locale.getDefault()).format(new Date());
		File mediaFile;
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ Code + ".jpg");
		} else if (type == MEDIA_TYPE_VIDEO) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "VID_" + timeStamp + ".mp4");
		} else {
			return null;
		}		

		return mediaFile;
	}

	/** Checking device has camera hardware or not **/
	public static boolean isDeviceSupportCamera(Activity activity) {
		if (activity.getPackageManager().hasSystemFeature(
				PackageManager.FEATURE_CAMERA)) {
			// this device has a camera
			return true;
		} else if (activity.getPackageManager().hasSystemFeature(
				PackageManager.FEATURE_CAMERA_FRONT)) {
			// this device has a camera
			return true;
		} else {
			// no camera on this device
			return false;
		}
	}
}
