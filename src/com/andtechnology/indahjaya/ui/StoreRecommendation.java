package com.andtechnology.indahjaya.ui;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.andtechnology.indahjaya.R;
import com.andtechnology.indahjaya.adapter.StoreListAdapter;
import com.andtechnology.indahjaya.database.DatabaseHandler;
import com.andtechnology.indahjaya.model.MappingSalesToCategoryProductModel;
import com.andtechnology.indahjaya.model.MappingSalesToStoreTargetModel;
import com.andtechnology.indahjaya.model.ModelStore;
import com.andtechnology.indahjaya.model.StoreRecommendationModel;
import com.andtechnology.indahjaya.utils.Commons;
import com.andtechnology.indahjaya.view.store.ActivityStoreRegistrationAction;
import com.andtechnology.indahjaya.view.store.ActivityStoreRegistrationView;

public class StoreRecommendation extends Fragment implements OnClickListener,
		OnItemClickListener {
	private View view;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.layout_store_recommendation,
				container, false);
		view = rootView;
		initView(rootView);
		initListView(rootView);
		return rootView;
	}

	public void initView(View view) {
		((ImageView) view.findViewById(R.id.iv_add)).setOnClickListener(this);

		DatabaseHandler db = new DatabaseHandler(getActivity());
		List<MappingSalesToStoreTargetModel> storeregistration = db
				.getAllMappingSalesToStoreTargetModel();
		List<StoreRecommendationModel> storeRecommendation = db
				.getAllStoreRecommendationKPI();

		SimpleDateFormat df = new SimpleDateFormat("yyyyMM");
		String formattedDate = df.format(new Date());
		int target = 0;
		if(storeregistration.size() > 0) {
			target = Integer.parseInt(storeregistration.get(0).get_target());
		}
		((TextView) view.findViewById(R.id.tvKPI)).setText("Target KPI "+formattedDate+"\n" + String.valueOf(storeRecommendation.size() 
				+ " of "
				+ target));

	}

	public void initListView(View view) {
		StoreListAdapter adapter = new StoreListAdapter(getActivity(), 0,
				getDataFromDatabase(view));
		ListView lvStoreList = (ListView) view.findViewById(R.id.lvStoreList);
		lvStoreList.setAdapter(adapter);
		lvStoreList.setOnItemClickListener(this);
		adapter.notifyDataSetChanged();

	}

	public ArrayList<ModelStore> getDataFromDatabase(View view) {
		DatabaseHandler db = new DatabaseHandler(getActivity());

		List<StoreRecommendationModel> storeregistration = db
				.getAllStoreRecommendation();
		if (storeregistration.size() == 0) {
			view.findViewById(R.id.EmptyIndicator).setVisibility(View.VISIBLE);
		} else {
			view.findViewById(R.id.EmptyIndicator)
					.setVisibility(View.INVISIBLE);
		}
		ArrayList<ModelStore> store_list = new ArrayList<ModelStore>();

		for (StoreRecommendationModel cn : storeregistration) {
			ModelStore model = new ModelStore();
			model.setId(cn.get_store_recommendation_id());
			model.setStore_name(cn.get_store_name());
			model.setStatus(String.valueOf(cn.get_status()));
			model.setDescription(cn.get_store_owner());
			store_list.add(model);
		}

		return store_list;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.iv_add:
			// FragmentManager fm = getFragmentManager();
			// FragmentTransaction fragmentTransaction = fm.beginTransaction();
			// fragmentTransaction.replace(R.id.llContainer, new
			// AddStoreRecommendation());
			// fragmentTransaction.commit();
			Intent i = new Intent(v.getContext(),
					ActivityStoreRegistrationAction.class);
			i.putExtra("getId", 0);
			startActivityForResult(i, Commons.FROM_STORE_REGISTRATION);

			break;

		default:
			break;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub

		ModelStore model = (ModelStore) parent.getAdapter().getItem(position);

		Intent i = new Intent(getActivity(),
				ActivityStoreRegistrationView.class);
		i.putExtra("getId", model.getId());
		// i.putExtra("model_store", model);
		startActivityForResult(i, 908);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Commons.RESULT_OK_FRAGMENT)
			initListView(view);
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		initView(view);
		initListView(view);
	}

}
