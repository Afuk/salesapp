package com.andtechnology.indahjaya.ui.customview;

import com.andtechnology.indahjaya.R;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class CustomDialogFragment extends DialogFragment {
	String title, description;
	boolean isYesNo;
	YesNoAction yesNoListener;
	SingleAction okeListener;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.layout_dialog_fragment,
				container, false);

		rootView.setLayoutParams(new ViewGroup.LayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.MATCH_PARENT));

		TextView tvTitle = (TextView) rootView.findViewById(R.id.tvTitle);
		TextView tvText1 = (TextView) rootView.findViewById(R.id.tvText1);

		LinearLayout llYesNO = (LinearLayout) rootView
				.findViewById(R.id.llYesNO);
		LinearLayout llOK = (LinearLayout) rootView.findViewById(R.id.llOK);

		tvTitle.setText(title);
		tvText1.setText(description);
		
		((TextView)rootView.findViewById(R.id.tvNo)).setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				yesNoListener.onNo(v);

			}
		});
		((TextView)rootView.findViewById(R.id.tvYes)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				yesNoListener.onYes(v);

			}
		});
		((TextView)rootView.findViewById(R.id.tvOk)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				okeListener.OK(v);
			}
		});
		if (!isYesNo) {
			llYesNO.setVisibility(View.GONE);
			llOK.setVisibility(View.VISIBLE);	
			okeListener.OK((TextView)rootView.findViewById(R.id.tvOk));			

		} else {
			
			llOK.setVisibility(View.GONE);
			llYesNO.setVisibility(View.VISIBLE);
			
		}
		return rootView;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		Dialog dialog = super.onCreateDialog(savedInstanceState);

		// request a window without the title
		dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//		dialog.getWindow()
//				.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		Drawable d = new ColorDrawable(Color.BLACK);
		d.setAlpha(120);
		dialog.getWindow().setBackgroundDrawable(d);
		dialog.setCanceledOnTouchOutside(false);
		dialog.setCancelable(false);

		// setStyle( DialogFragment.STYLE_NORMAL, R.style.CustomDialog );
		return dialog;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setStyle(STYLE_NO_FRAME, android.R.style.Theme_Holo_Light_Dialog);
	}
	

	public void setTitle(String title) {
		this.title = title;
	}

	public void setYesNo(boolean isYesNo) {
		this.isYesNo = isYesNo;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public void setYesNoListener(YesNoAction yesNoAction)
	{
		this.yesNoListener =yesNoAction;
	}
	public void setOkListener(SingleAction listener)
	{
		this.okeListener =listener;
	}
   
	public interface YesNoAction {
		void onYes(View view);
		void onNo(View view);

	}

	interface SingleAction {
           void OK(View view);
	}
}
