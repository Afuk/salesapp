package com.andtechnology.indahjaya.ui;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.andtechnology.indahjaya.R;
import com.andtechnology.indahjaya.adapter.ListFragmentActiveJobsAdapter;
import com.andtechnology.indahjaya.database.DatabaseHandler;
import com.andtechnology.indahjaya.lib.util.GPSTracker;
import com.andtechnology.indahjaya.lib.util.SessionManagement;
import com.andtechnology.indahjaya.lib.util.SphericalUtils;
import com.andtechnology.indahjaya.lib.util.Utils;
import com.andtechnology.indahjaya.model.MappingStoreModel;
import com.andtechnology.indahjaya.model.NavDrawerMenuModel;
import com.andtechnology.indahjaya.ui.service.APIParam;
import com.andtechnology.indahjaya.utils.Commons;
import com.andtechnology.indahjaya.view.journey.activejobs.ActivityActiveJobs;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class TodayStoreActiveJobs extends Fragment {

	MappingStoreModel model;

	ArrayList<MappingStoreModel> mappingStoreModel;
	ListView lvActiveJobs;
	View getfinalrootView = null;

	// initGPS
	GPSTracker gpsTracker;
	private String latitude, longitude;
	private String latitude_store, longitude_store;

	// service
	// initial service
	// ProgressDialog loading;

	private HashMap<String, String> parameters;
	private int apiIndex;
	private boolean has_service = false;
	private Messenger mService = null;
	private Messenger mMessenger = new Messenger(new UpdateCoordinatHandler(
			this));
	ServiceConnection scConnection = new ServiceConnection() {

		@Override
		public void onServiceDisconnected(ComponentName componentName) {
			mService = null;
		}

		@Override
		public void onServiceConnected(ComponentName componentName,
				IBinder binder) {
			mService = new Messenger(binder);
			doAPI(apiIndex);
		}
	};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(
				R.layout.layout_today_store_active_job, container, false);
		initDate(rootView);
		initializeBindService();
		getfinalrootView = rootView;
		LoadList(rootView);
		gpsTracker = new GPSTracker(getActivity());

		return rootView;
	}

	public void LoadList(View view) {
		SimpleDateFormat dfg = new SimpleDateFormat("yyyy-MM-dd");
		SessionManagement session = new SessionManagement(getActivity());
		HashMap<String, String> user = session.getUserDetails();

		mappingStoreModel = new ArrayList<MappingStoreModel>();
		lvActiveJobs = (ListView) view.findViewById(R.id.lvActiveJobs);
		DatabaseHandler db = new DatabaseHandler(getActivity());
		List<MappingStoreModel> mappingstoremodel = db.getAllMappingStore(0,
				user.get(session.USER_ID), dfg.format(new Date()));
		if (mappingstoremodel.size() == 0) {
			view.findViewById(R.id.EmptyIndicator).setVisibility(View.VISIBLE);
		} else {
			view.findViewById(R.id.EmptyIndicator)
					.setVisibility(View.INVISIBLE);
		}

		// setting the nav drawer list adapter
		ListFragmentActiveJobsAdapter adapter = new ListFragmentActiveJobsAdapter(
				getActivity(), (ArrayList<MappingStoreModel>) mappingstoremodel);
		lvActiveJobs.setAdapter(adapter);
		lvActiveJobs.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				/* final MappingStoreModel */model = (MappingStoreModel) parent
						.getAdapter().getItem(position);
				AlertDialog.Builder builder = new AlertDialog.Builder(
						getActivity());
				builder.setTitle(R.string.confirmation);
				builder.setMessage(R.string.check_in_confirmation);
				builder.setPositiveButton(R.string.yes,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								GPSTracker gpsTracker = new GPSTracker(
										getActivity());
								if (!gpsTracker.canGetLocation()) {
									gpsTracker.turnOnGps();
								} else {

									if (model.get_latitude() == null
											|| model.get_latitude().toString()
													.length() == 1
											|| model.get_longtitude()
													.toString().length() == 1
											|| model.get_longtitude() == null) {
										// model.set_latitude(String
										// .valueOf(gpsTracker.latitude));
										// model.set_longtitude(String
										// .valueOf(gpsTracker.longitude));
										/*
										 * model.set_latitude(String
										 * .valueOf(gpsTracker.latitude));
										 * model.set_longtitude(String
										 * .valueOf(gpsTracker.longitude));
										 * setParameter(model);
										 * doAPI(APIParam.API_011);
										 */

										alertForUpdateCoordinat();
										return;
									} else {
										Double latitude = gpsTracker
												.getLatitude();
										Double longitude = gpsTracker
												.getLongitude();
										double from[] = new double[2];
										from[0] = Double.parseDouble(model
												.get_latitude());
										from[1] = Double.parseDouble(model
												.get_longtitude());
										int maxDistance = Integer
												.parseInt(model.get_radius()) * 100;
										if (isValidDistance(from, maxDistance))
											gotoActivity(model);
									}

								}
							}
						});
				builder.setNegativeButton(R.string.no, null);
				AlertDialog alert = builder.create();
				alert.show();
			}
		});

	}

	public void gotoActivity(MappingStoreModel model) {
		SimpleDateFormat df = new SimpleDateFormat("EEEE, dd MMMM yyyy");
		final String formattedDate = df.format(new Date());
		final Intent i = new Intent(getActivity(), ActivityActiveJobs.class);
		i.putExtra("getTitle", model.get_store_name());
		i.putExtra("getId", model.get_store_id());
		i.putExtra("getFinalId", 1);
		i.putExtra("getDate", formattedDate);
		i.putExtra("latitude_store", "" + model.get_latitude());
		i.putExtra("longitude_store", "" + model.get_longtitude());
		i.putExtra("mapping_store_id", "" + model.get_mapping_store_id());
		i.putExtra("progress_status", "" + model.get_progress_status());
		i.putExtra("radius", "" + model.get_radius());
		i.putExtra("date_checkin", "" + Utils.getTimeNow());

		startActivityForResult(i, 901);
	}

	public void initDate(View view) {
		SimpleDateFormat df = new SimpleDateFormat("EEEE, dd MMMM yyyy");
		final String formattedDate = df.format(new Date());

		TextView tv_today = (TextView) view.findViewById(R.id.tvToday);
		tv_today.setText(formattedDate);
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		LoadList(getfinalrootView);
	}

	public void doApi() {
		parameters = new HashMap<String, String>();
		parameters.put("user_sales_id", "");
		parameters.put("store_id", "");
		parameters.put("longtitude", "");
		parameters.put("latitude", "");
	}

	private void initializeBindService() {

		if (Utils.isOnline(getActivity())) {
			if (mService == null) {
				has_service = true;
				getActivity().bindService(Commons.iService, scConnection,
						Context.BIND_AUTO_CREATE);
			}
		}
		// else {
		// doAPI(apiIndex);
		// }
	}

	private void doAPI(int apiIndex) {
		// progressHUD = ProgressHUD.show(getApplicationContext(), "", true,
		// false, null);

		try {
			Message msg = Message.obtain(null, apiIndex);
			Bundle bundle = new Bundle();
			bundle.putSerializable("parameters", parameters);
			bundle.putBoolean("for_httpost", true);
			msg.setData(bundle);
			msg.replyTo = mMessenger;
			mService.send(msg);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	private static class UpdateCoordinatHandler extends Handler {
		WeakReference<TodayStoreActiveJobs> screen;

		public UpdateCoordinatHandler(TodayStoreActiveJobs myScreen) {
			screen = new WeakReference<TodayStoreActiveJobs>(myScreen);
		}

		@Override
		public void handleMessage(Message msg) {
			Log.i("TAG", msg.toString());
			switch (msg.what) {

			// API BLM ADA
			case APIParam.API_011:
				screen.get().getResponseUpdateLatlong(msg);
				break;

			default:
				break;
			}

		}
	}

	private void getResponseUpdateLatlong(Message msg) {
		HashMap<Object, Object> response = (HashMap<Object, Object>) msg
				.getData().getSerializable("response");

		Integer result = (Integer) response.get("response");
		switch (result) {
		case Commons.SUCCESS:
			DatabaseHandler db = new DatabaseHandler(getActivity());
			db.updateLatLongMappingStore(model.get_mapping_store_id(),
					model.get_latitude(), model.get_longtitude());
			gotoActivity(model);
			break;
		case Commons.ERROR:
			Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
			// loading.dismiss();
			break;
		default:

			break;
		}
		// progressHUD.dismiss();

	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		if (has_service) {
			getActivity().unbindService(scConnection);
			getActivity().stopService(Commons.iService);
		}
		super.onDestroy();
	}

	public void setParameter(MappingStoreModel model) {
		parameters = new HashMap<String, String>();
		parameters.put("user_sales_id", "" + model.get_user_sales_id());
		parameters.put("store_id", "" + model.get_store_id());
		parameters.put("longtitude", "" + model.get_latitude());
		parameters.put("latitude", "" + model.get_longtitude());
	}

	public void alertForUpdateCoordinat() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(R.string.confirmation);
		builder.setMessage("This store don't have coordinate, are you sure to update coordinate for this store?");
		builder.setPositiveButton(R.string.yes,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						GPSTracker gpsTracker = new GPSTracker(getActivity());
						if (!gpsTracker.canGetLocation()) {
							gpsTracker.turnOnGps();
							Toast.makeText(getActivity(),
									"Please active your GPS!",
									Toast.LENGTH_SHORT).show();
						} else {

							if (Utils.isOnline(getActivity())) {

								initializeBindService();

								model.set_latitude(String
										.valueOf(gpsTracker.latitude));
								model.set_longtitude(String
										.valueOf(gpsTracker.longitude));
								setParameter(model);
								doAPI(APIParam.API_011);
								return;
							} else {
								Toast.makeText(
										getActivity(),
										"For Update this coordinate you must have connection",
										Toast.LENGTH_SHORT).show();

							}

						}
					}
				});
		builder.setNegativeButton(R.string.no, null);
		AlertDialog alert = builder.create();
		alert.show();
	}

	public boolean isValidDistance(double from[], int max_distance) {
		if (from[0] == 0 && from[1] == 0) {
			if (latitude == null || longitude == null) {
				Toast.makeText(
						getActivity(),
						"Failed,your position latlong 0, Please check your gps",
						Toast.LENGTH_SHORT).show();
				return false;
			}
			Toast.makeText(
					getActivity(),
					"Latlong store awal masih kosong jadi diperbolehkan upload.",
					Toast.LENGTH_SHORT).show();
			return true;
		} else {

			if (model.get_longtitude() == null || model.get_latitude() == null) {
				Toast.makeText(
						getActivity(),
						"Failed,your position latlong 0, Please check your gps",
						Toast.LENGTH_SHORT).show();
				return true;
			} else {

				double to[] = new double[2];
				to[0] = Double.parseDouble(latitude_store);
				to[1] = Double.parseDouble(longitude_store);
				int distance = (int) SphericalUtils.computeDistanceBetween(
						from, to);
				return distance <= max_distance;
			}
		}
	}
}
