package com.andtechnology.indahjaya.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.andtechnology.indahjaya.R;
import com.andtechnology.indahjaya.adapter.FragmentPromoInformationAdapter;
import com.andtechnology.indahjaya.database.DatabaseHandler;
import com.andtechnology.indahjaya.model.CategoryProductModel;
import com.andtechnology.indahjaya.model.NavDrawerMenuModel;
import com.andtechnology.indahjaya.model.PromoModel;
import com.andtechnology.indahjaya.model.SubCategoryProductModel;
import com.andtechnology.indahjaya.view.promoinfo.ActivityProductInformationDetail;

public class PromoInfo extends Fragment {
	ArrayList<NavDrawerMenuModel> navDrawerMenu;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final View rootView = inflater.inflate(R.layout.layout_promo_info, container,
				false);
		// initListView(rootView);
		final DatabaseHandler db = new DatabaseHandler(rootView.getContext());
		List<CategoryProductModel> categoryProduct = db.getAllCategoryProduct();

		final HashMap<String, Integer> finalresult = new HashMap<String, Integer>();
		final HashMap<String, Integer> finalresult2 = new HashMap<String, Integer>();
		List<String> list = new ArrayList<String>();
		for (CategoryProductModel cn : categoryProduct) {
			list.add(cn.get_category_product_name());
			finalresult.put(cn.get_category_product_name(), cn.get_category_product_id());
		}

		ArrayAdapter<String> adapters = new ArrayAdapter(getActivity(),
				android.R.layout.simple_spinner_item, list);
		adapters.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		((Spinner) rootView.findViewById(R.id.sp_category)).setAdapter(adapters);
		final Spinner sp_category = ((Spinner) rootView.findViewById(R.id.sp_category));
		final Spinner sp_sub_category = ((Spinner) rootView.findViewById(R.id.sp_sub_category));

			

			sp_category.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parent, View view,
						int position, long id) {
					// TODO Auto-generated method stub
					
					String category_id = finalresult.get(sp_category.getSelectedItem().toString()).toString();
					List<SubCategoryProductModel> subcategoryProduct = db.getAllSubCategoryProduct(category_id);
					List<String> list2 = new ArrayList<String>();
					for (SubCategoryProductModel cn : subcategoryProduct) {
						list2.add(cn.get_sub_category_product_name());
						finalresult2.put(cn.get_sub_category_product_name(), cn.get_sub_category_product_id());
					}
					
					ArrayAdapter<String> adapters2 = new ArrayAdapter(getActivity(),
							android.R.layout.simple_spinner_item, list2);
					adapters2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					((Spinner) rootView.findViewById(R.id.sp_sub_category)).setAdapter(adapters2);
					
						

				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
					// TODO Auto-generated method stub

				}

			});

			sp_sub_category.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parent, View view,
						int position, long id) {
					// TODO Auto-generated method stub					
					String sub_category_id = finalresult2.get(sp_sub_category.getSelectedItem().toString()).toString();
					LoadList(rootView, sub_category_id);
				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
					// TODO Auto-generated method stub

				}

			});
		return rootView;
	}

	// public void initListView(View view)
	// {
	// PromoListAdapter adapter = new PromoListAdapter(getActivity(), 0,
	// getDataFromDatabase(view));
	// ListView lvPromoList=(ListView)view.findViewById(R.id.lvPromoList);
	// lvPromoList.setAdapter(adapter);
	// lvPromoList.setOnItemClickListener(this);
	// adapter.notifyDataSetChanged();
	//
	// }

	// public ArrayList<NavDrawerMenuModel> getDataFromDatabase(View view)
	// {
	// DatabaseHandler db = new DatabaseHandler(getActivity());
	//
	// List<PromoModel> promo = db.getAllPromo();
	// if(promo.size() == 0) {
	// view.findViewById(R.id.EmptyIndicator).setVisibility(View.VISIBLE);
	// } else {
	// view.findViewById(R.id.EmptyIndicator).setVisibility(View.INVISIBLE);
	// }
	//
	// ArrayList<NavDrawerMenuModel> navDrawerMenu = new
	// ArrayList<NavDrawerMenuModel>();
	//
	//
	// for (PromoModel cn : promo) {
	// NavDrawerMenuModel model =new NavDrawerMenuModel();
	// model.setTitle(cn.get_promo_name());
	// model.setDescription(cn.get_description());
	// model.setImage_name(cn.get_images());
	// model.setId(cn.get_promo_id());
	// navDrawerMenu.add(model);
	// }
	//
	//
	// return navDrawerMenu;
	// }

	public void LoadList(View rootView, String sub_category_id) {
		TextView title_text = (TextView) rootView.findViewById(R.id.title_text);

		navDrawerMenu = new ArrayList<NavDrawerMenuModel>();
		ListView mDrawerList = (ListView) rootView
				.findViewById(R.id.lvPromoList);

		NavDrawerMenuModel model;

		DatabaseHandler db = new DatabaseHandler(rootView.getContext());
		List<PromoModel> promo = db.getAllPromo(sub_category_id);
		if (promo.size() == 0) {
			rootView.findViewById(R.id.EmptyIndicator).setVisibility(
					View.VISIBLE);
		} else {
			rootView.findViewById(R.id.EmptyIndicator).setVisibility(
					View.INVISIBLE);
		}

		for (PromoModel cn : promo) {
			model = new NavDrawerMenuModel();
			model.setTitle(cn.get_promo_name());
			model.setDescription(cn.get_description());
			model.setImage_name(cn.get_images());
			model.setId(cn.get_promo_id());
			model.setStart_date(cn.get_start_date());
			model.setEnd_date(cn.get_end_date());
			model.setId(cn.get_promo_id());
			navDrawerMenu.add(model);
		}

		// setting the nav drawer list adapter
		FragmentPromoInformationAdapter adapter = new FragmentPromoInformationAdapter(
				getActivity(), navDrawerMenu);
		mDrawerList.setAdapter(adapter);
		adapter.notifyDataSetChanged();

		mDrawerList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				NavDrawerMenuModel model = (NavDrawerMenuModel) parent
						.getAdapter().getItem(position);

				Intent i = new Intent(v.getContext(),
						ActivityProductInformationDetail.class);
				i.putExtra("getTitle", model.getTitle());
				i.putExtra("getDescription", model.getDescription());
				i.putExtra("getImage_name", model.getImage_name());
				i.putExtra("getStart_date", model.getStart_date());
				i.putExtra("getEnd_date", model.getEnd_date());
				startActivityForResult(i, 904);

			}
		});

	}

}
