package com.andtechnology.indahjaya.ui;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

import com.andtechnology.indahjaya.R;
import com.andtechnology.indahjaya.adapter.MarketSurveyListAdapter;
import com.andtechnology.indahjaya.database.DatabaseHandler;
import com.andtechnology.indahjaya.model.MarketSurveyModel;
import com.andtechnology.indahjaya.utils.Commons;
import com.andtechnology.indahjaya.view.MarketSurveyDetail;

public class MarketSurveyList extends Fragment implements OnClickListener,
		OnItemClickListener {
	private View view;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.layout_market_survei, container,
				false);
		view =rootView;
		initView(rootView);
		initListView(rootView);
		return rootView;
	}

	public void initView(View view) {
		((ImageView) view.findViewById(R.id.iv_add)).setOnClickListener(this);
	}

	public void initListView(View view) {
		MarketSurveyListAdapter adapter = new MarketSurveyListAdapter(getActivity(), 0,
				getDataFromDatabase(view));
		ListView lvMarketSurveyList = (ListView) view.findViewById(R.id.lvMarketSurveyList);
		lvMarketSurveyList.setAdapter(adapter);
		lvMarketSurveyList.setOnItemClickListener(this);
		adapter.notifyDataSetChanged();

	}

	public ArrayList<MarketSurveyModel> getDataFromDatabase(View view) {
		DatabaseHandler db = new DatabaseHandler(getActivity());

		List<MarketSurveyModel> market_survey_list = db
				.getAllMarketSurvey();
		if (market_survey_list.size() == 0) {
			view.findViewById(R.id.EmptyIndicator).setVisibility(View.VISIBLE);
		} else {
			view.findViewById(R.id.EmptyIndicator)
					.setVisibility(View.INVISIBLE);
		}
		
		return (ArrayList<MarketSurveyModel>) market_survey_list;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.iv_add:
			
			Intent i = new Intent(v.getContext(),
					MarketSurveyDetail.class);
			i.putExtra("id", 0);
			i.putExtra("isView",false);
			startActivityForResult(i, Commons.FROM_MARKET_SURVEY_LIST);

			break;

		default:
			break;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {

		MarketSurveyModel model = (MarketSurveyModel) parent.getAdapter().getItem(position);

		Intent i = new Intent(getActivity(),
				MarketSurveyDetail.class);
		i.putExtra("id", model.getMarket_intelligence_id());
		i.putExtra("isView",true);

		startActivityForResult(i, Commons.FROM_MARKET_SURVEY_LIST);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Commons.RESULT_OK_FRAGMENT)
			initListView(view);
	}

	@Override
	public void onStart() {
		super.onStart();
		initView(view);
		initListView(view);
	}
}
