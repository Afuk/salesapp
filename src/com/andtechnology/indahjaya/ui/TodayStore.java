package com.andtechnology.indahjaya.ui;

import com.andtechnology.indahjaya.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class TodayStore extends Fragment implements OnClickListener {
	private ImageView ivJobsDone, ivDraftJobs, ivActiveJobs;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.layout_today_store_main,
				container, false);
		initButton(rootView);
		return rootView;
	}
    public void startFragment(View view)
    {
    	FragmentManager fm = getFragmentManager();
		FragmentTransaction fragmentTransaction = fm.beginTransaction();
    	fragmentTransaction.replace(R.id.llContainerChild,
				new TodayStoreActiveJobs());
		fragmentTransaction.commit();
		ivActiveJobs.setVisibility(View.VISIBLE);
		ivJobsDone.setVisibility(View.INVISIBLE);
		ivDraftJobs.setVisibility(View.INVISIBLE);
    }
	public void initButton(View view) {
		((TextView) view.findViewById(R.id.tvActiveJobs))
				.setOnClickListener(this);
		((TextView) view.findViewById(R.id.tvDraftJobs))
				.setOnClickListener(this);
		((TextView) view.findViewById(R.id.tvJobsDone))
				.setOnClickListener(this);
		ivJobsDone = (ImageView) view.findViewById(R.id.ivJobsDone);
		ivDraftJobs = (ImageView) view.findViewById(R.id.ivDraftJobs);
		ivActiveJobs = (ImageView) view.findViewById(R.id.ivActiveJobs);
		startFragment(view);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		FragmentManager fm = getFragmentManager();
		FragmentTransaction fragmentTransaction = fm.beginTransaction();
		switch (v.getId()) {
		case R.id.tvActiveJobs:
			fragmentTransaction.replace(R.id.llContainerChild,
					new TodayStoreActiveJobs());
			fragmentTransaction.commit();
			ivActiveJobs.setVisibility(View.VISIBLE);
			ivJobsDone.setVisibility(View.INVISIBLE);
			ivDraftJobs.setVisibility(View.INVISIBLE);

			break;
		case R.id.tvDraftJobs:
			fragmentTransaction.replace(R.id.llContainerChild,
					new TodayStoreDraftJobs());
			fragmentTransaction.commit();
			ivActiveJobs.setVisibility(View.INVISIBLE);
			ivDraftJobs.setVisibility(View.VISIBLE);
			ivJobsDone.setVisibility(View.INVISIBLE);

			break;
		case R.id.tvJobsDone:
			fragmentTransaction.replace(R.id.llContainerChild,
					new TodayStoreDoneJobs());
			fragmentTransaction.commit();
			ivActiveJobs.setVisibility(View.INVISIBLE);
			ivDraftJobs.setVisibility(View.INVISIBLE);
			ivJobsDone.setVisibility(View.VISIBLE);

			break;
		default:
			break;
		}
	}

}
