package com.andtechnology.indahjaya.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.andtechnology.indahjaya.R;
import com.andtechnology.indahjaya.lib.util.UtilsPointer;
import com.andtechnology.indahjaya.view.framework.FragmentFramework;

public class Setting extends FragmentFramework{
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.layout_setting,
				container, false);
		
		LinearLayout llChangePassword = (LinearLayout) rootView.findViewById(R.id.llChangePassword);
		llChangePassword.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				UtilsPointer.goToChangePassword(activity);
				
			}
		});
		return rootView;
	}

}
