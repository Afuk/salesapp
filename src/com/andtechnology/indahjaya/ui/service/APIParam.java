package com.andtechnology.indahjaya.ui.service;

import java.util.HashMap;
import java.util.Map.Entry;

import com.andtechnology.indahjaya.utils.Commons;

public class APIParam {
	
	/** syncLogin */
	public final static int API_001 = 1;

	/** sysc_count_message */
	public final static int API_002 = 2;

	/** sysc_message */
	public final static int API_003 = 3;

	/** sysc_update_message */
	public final static int API_004 = 4;

	/** insert store recommendation */
	public final static int API_005 = 5;
	
	/** insert_mapping sales to store */
	public final static int API_006 = 6;
	
	/** insert_activities */
	public final static int API_007 = 7;
	
	/** check_updated_data */
	public final static int API_010 = 10;
	
	/** check_updated_data */
	public final static int API_011 = 11;
	
	/** Insert Market Intelligent */
	public final static int API_012 = 12;
  


	public static String urlBuilder(int apiIndex,
			HashMap<String, String> parameters) {
		// String url = Commons.BASE_URL;
//		String url = PreferenceConnector.readString(Commons.context,
//				PreferenceConnector.URL, "");
		String url =Commons.ProductionHttp
				+ APIParam.getAPIUrl(apiIndex);
		if (parameters == null) {
			return url;
		}
		int i = 0;
		for (Entry<String, String> entry : parameters.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			int size = key.length() + value.length() + 2;
			StringBuilder sbParameter = new StringBuilder(size);
			if (i == 0) {
				url += sbParameter.append("?").append(key).append("=")
						.append(value).toString();
			} else {
				url += sbParameter.append("&").append(key).append("=")
						.append(value).toString();
			}
			i++;
		}
		return url;
	}

	public static String getAPIUrl(int apiIndex) {
		String apiUrl = "";
		switch (apiIndex) {
		case APIParam.API_001:
			apiUrl = "/api/syncLogin";
			break;
		case APIParam.API_002:
			apiUrl = "/api/sysc_count_message";
			break;
		case APIParam.API_003:
			apiUrl = "/api/sysc_message";
			break;
		case APIParam.API_004:
			apiUrl = "/api/sysc_update_message";
			break;
		case APIParam.API_005:
			apiUrl = "/api/insert_store_recommendation";
			break;
		case APIParam.API_006:
			apiUrl = "/api/mapping_sales_to_store";
			break;
		case APIParam.API_007:
			apiUrl = "/api/insert_activities";
			break;
		case APIParam.API_010:
			apiUrl = "/api/check_updated_data";
			break;
		case APIParam.API_011:
			apiUrl = "/api/update_coordinate_store";
			break;
		case APIParam.API_012:
			apiUrl = "/api/insert_market_intelligence";
			break;
			
		 default:
			apiUrl = "wrong url";
			break;

		}
		return apiUrl;
	}
}