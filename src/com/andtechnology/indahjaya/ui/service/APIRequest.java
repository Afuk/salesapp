package com.andtechnology.indahjaya.ui.service;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.os.Bundle;
import android.util.Log;

import com.andtechnology.indahjaya.utils.Commons;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;

public class APIRequest {
	// I think this is still GET Request
	@SuppressWarnings("unchecked")
	public HashMap<Object, Object> getAPIRequest(int apiIndex,
			HashMap<String, String> parameters, boolean hasMultiEntity,
			Bundle bundle) {
		HashMap<Object, Object> result = new HashMap<Object, Object>();
		try {
			Log.i("TAG", APIParam.urlBuilder(apiIndex, parameters));

			Log.i("TAG", "MULTI ENTITY : " + hasMultiEntity);
			HttpResponse response;
			HttpClient myClient = new DefaultHttpClient();
			HttpConnectionParams.setConnectionTimeout(myClient.getParams(),
					10000);
			HttpConnectionParams.setSoTimeout(myClient.getParams(), 10000);

			Log.i("AAA",
					"param CONNECTION_TIMEOUT: "
							+ myClient.getParams().getParameter(
									HttpConnectionParams.CONNECTION_TIMEOUT));
			Log.i("AAA",
					"param SO_TIMEOUT: "
							+ myClient.getParams().getParameter(
									HttpConnectionParams.SO_TIMEOUT));

			HttpPost myConnection = new HttpPost(APIParam.urlBuilder(apiIndex,
					parameters));
			// TODO BUAT MULTIPART ENTITY
			if (hasMultiEntity) {
				Log.i("TAG", "MASUK ME");

				String[] arrKey = bundle.getStringArray("key_string");
				String file_type = bundle.getString("type_file");
				boolean from_verify = false;
				if (bundle.containsKey("verify")) {
					from_verify = bundle.getBoolean("verify");
					Log.i("AAA", "verify : " + from_verify);
				}
				Log.i("TAG", "file_type : " + file_type);
				if (file_type.equalsIgnoreCase(".png")) {
					Log.i("TAG", "cuma gambar");
					MultipartEntity mpEntity = new MultipartEntity(
							HttpMultipartMode.BROWSER_COMPATIBLE);
					for (int i = 0; i < arrKey.length; i++) {
						Log.i("TAG", "arrkey : " + arrKey[i]);
						Log.i("TAG",
								"arrkey : " + bundle.getString(arrKey[i] + i));
						if (arrKey[i].toString().length() > 0) {
							byte[] bytearr = bundle.getByteArray(arrKey[i]);

							Log.i("TAG", "verify : " + bytearr);
							if (bytearr != null) {
								if (from_verify) {
									Log.i("TAG", "verify : test ");
									mpEntity.addPart(
											arrKey[i],
											new ByteArrayBody(bytearr, bundle
													.getString(arrKey[i] + i)));
									Log.i("AAA",
											"mpentity : " + mpEntity.hashCode());
								} else {
									mpEntity.addPart(
											arrKey[i],
											new ByteArrayBody(bundle
													.getByteArray(arrKey[i]),
													arrKey[i] + file_type));
								}
							}
						}
					}
					myConnection.setEntity(mpEntity);
				} else {
					MultipartEntity mpEntity = new MultipartEntity(
							HttpMultipartMode.BROWSER_COMPATIBLE);
					Log.i("TAG", "ME : mpEntity : " + mpEntity);
					for (int i = 0; i < arrKey.length; i++) {
						Log.i("TAG", "arrkey : " + arrKey[i]);
						if (arrKey[i].toString().length() > 0) {
							File file = new File(
									bundle.getStringArray(arrKey[i])[1]);
							Log.i("TAG", "ME : arrKey[i] : " + arrKey[i]);
							FileBody fileBody = new FileBody(file,
									bundle.getStringArray(arrKey[i])[2]);
							Log.i("TAG",
									"key : "
											+ bundle.getStringArray(arrKey[i])[0]);
							mpEntity.addPart(
									bundle.getStringArray(arrKey[i])[0],
									fileBody);
						}
					}
					myConnection.setEntity(mpEntity);
				}
			}
			try {
				response = myClient.execute(myConnection);
				String JSONString = EntityUtils.toString(response.getEntity(),
						"UTF-8");

				if (isValidJSON(JSONString)) {
					result = new ObjectMapper().readValue(JSONString,
							HashMap.class);
					if (result.get("response").equals("success")) {
						result.put("response", Commons.SUCCESS);
					} else if (result.get("response").equals("error")) {
						result.put("response", Commons.ERROR);
					} else {
						// TODO INVALID VALUE
					}
				} else {
					// TODO INVALID JSON
				}

			} catch (ClientProtocolException e) {
				result.put("response",Commons.ERROR);
				e.printStackTrace();
			} catch (IOException e) {
				result.put("response", Commons.ERROR);
				e.printStackTrace();
			}
		} catch (Exception e) {
			result.put("response", Commons.ERROR);
			e.printStackTrace();
		}
		Log.i("TAG", "result balikan : " + result);
		return result;
	}

	// for api request using POST
	@SuppressWarnings("unchecked")
	public HashMap<Object, Object> getAPIRequest(int apiIndex,
			HashMap<String, String> parameters, boolean hasMultiEntity,
			Bundle bundle, boolean for_httpost) {
		HashMap<Object, Object> result = new HashMap<Object, Object>();
		try {
			// Log.i("TAG", APIParam.urlBuilder(apiIndex, parameters));
			HttpResponse response;
			HttpClient myClient = new DefaultHttpClient();
			// String url = PreferenceConnector.readString(Commons.context,
			// PreferenceConnector.URL, "");
			String url;
			// Log.i("TAG", "url : " + (url + APIParam.getAPIUrl(apiIndex)));
			HttpPost myConnection = new HttpPost(Commons.ProductionHttp
					+ APIParam.getAPIUrl(apiIndex));

			MultipartEntity mpEntity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);
			for (Entry<String, String> entry : parameters.entrySet()) {
				String key = entry.getKey();
				String value = entry.getValue();
				Log.i("AAA", "key : " + key);
				Log.i("AAA", "value : " + value);

				Charset chars = Charset.forName("UTF-8");
				mpEntity.addPart(key, new StringBody(value, chars));

			}

			// TODO BUAT MULTIPART ENTITY
			if (hasMultiEntity) {
				Log.i("TAG", "MASUK ME");

				String[] arrKey = bundle.getStringArray("key_string");
				String file_type = bundle.getString("type_file");
				Log.i("TAG", "file_type : " + file_type);
				if (file_type.equalsIgnoreCase(".png")) {
					Log.i("TAG", "cuma gambar");
					for (int i = 0; i < arrKey.length; i++) {
						Log.i("TAG", "arrkey : " + arrKey[i]);
						if (arrKey[i].toString().length() > 0) {
							byte[] bytearr = bundle.getByteArray(arrKey[i]);
							Log.i("AAA", "bytearr : " + bytearr);
							if (bytearr != null) {
								mpEntity.addPart(arrKey[i], new ByteArrayBody(
										bundle.getByteArray(arrKey[i]),
										arrKey[i] + file_type));
							}
						}
					}
				} else {

					Log.i("AAA", "arrKey[0] : " + arrKey[0]);
					Log.i("TAG", "key0 : "
							+ bundle.getStringArray(arrKey[0])[0]);
					Log.i("TAG", "path0 : "
							+ bundle.getStringArray(arrKey[0])[1]);
					Log.i("TAG",
							"mimetype0 : "
									+ bundle.getStringArray(arrKey[0])[2]);
					for (int i = 0; i < arrKey.length; i++) {
						Log.i("TAG", "arrkey : " + arrKey[i]);
						if (arrKey[i].toString().length() > 0) {
							File file = new File(
									bundle.getStringArray(arrKey[i])[1]);
							Log.i("TAG", "ME : arrKey[i] : " + arrKey[i]);
							FileBody fileBody = new FileBody(file,
									bundle.getStringArray(arrKey[i])[2]);
							Log.i("TAG",
									"key : "
											+ bundle.getStringArray(arrKey[i])[0]);
							Log.i("TAG",
									"path : "
											+ bundle.getStringArray(arrKey[i])[1]);
							Log.i("TAG",
									"mimetype : "
											+ bundle.getStringArray(arrKey[i])[2]);
							Log.i("AAA", "file : " + file.getPath());
							Log.i("AAA", "filebody : " + fileBody);
							mpEntity.addPart(
									bundle.getStringArray(arrKey[i])[0],
									fileBody);
						}
					}
				}
			}
			myConnection.setEntity(mpEntity);
			try {
				response = myClient.execute(myConnection);
				String JSONString = EntityUtils.toString(response.getEntity(),
						"UTF-8");
				Log.i("TAG", "jsonstring : " + JSONString);
				if (isValidJSON(JSONString)) {
					result = new ObjectMapper().readValue(JSONString,
							HashMap.class);
					if (result.get("response").equals("success")) {

						result.put("response", Commons.SUCCESS);
					} else if (result.get("response").equals("error")) {
						result.put("response", Commons.ERROR);
					} else {
						// TODO INVALID VALUE
					}
				} else {
					// TODO INVALID JSON
				}

			} catch (ClientProtocolException e) {
				result.put("response", Commons.ERROR);
				e.printStackTrace();
			} catch (IOException e) {
				result.put("response", Commons.ERROR);
				e.printStackTrace();
			}
		} catch (Exception e) {
			result.put("response", Commons.ERROR);
			e.printStackTrace();
		}
		Log.i("TAG", "result balikan : " + result);
		return result;
	}
	
	
	
	
	@SuppressWarnings("unchecked")
	public HashMap<Object, Object> getApiRequestLocation(int apiIndex,
			HashMap<String, String> parameters) {
		HashMap<Object, Object> result = new HashMap<Object, Object>();
		try {
			// Log.i("TAG", APIParam.urlBuilder(apiIndex, parameters));
			HttpResponse response;
			HttpClient myClient = new DefaultHttpClient();
			// String url = PreferenceConnector.readString(Commons.context,
			// PreferenceConnector.URL, "");
			String url;
			// Log.i("TAG", "url : " + (url + APIParam.getAPIUrl(apiIndex)));
			HttpPost myConnection = new HttpPost(Commons.ProductionHttp
					+ APIParam.getAPIUrl(apiIndex));

			MultipartEntity mpEntity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			for (Entry<String, String> entry : parameters.entrySet()) {
				String key = entry.getKey();
				String value = entry.getValue();
				Log.i("AAA", "key : " + key);
				Log.i("AAA", "value : " + value);

				Charset chars = Charset.forName("UTF-8");
				mpEntity.addPart(key, new StringBody(value, chars));

			}

			// TODO BUAT MULTIPART ENTITY
			
			myConnection.setEntity(mpEntity);
			try {
				response = myClient.execute(myConnection);
				String JSONString = EntityUtils.toString(response.getEntity(),
						"UTF-8");
				Log.i("TAG", "jsonstring : " + JSONString);
				if (isValidJSON(JSONString)) {
					result = new ObjectMapper().readValue(JSONString,
							HashMap.class);
					if (result.get("response").equals("success")) {
						result.put("response", Commons.SUCCESS);
					} else if (result.get("response").equals("error")) {
						result.put("response", Commons.ERROR);
					} else {
						// TODO INVALID VALUE
					}
				} else {
					// TODO INVALID JSON
				}

			} catch (ClientProtocolException e) {
				result.put("response", Commons.ERROR);
				e.printStackTrace();
			} catch (IOException e) {
				result.put("response", Commons.ERROR);
				e.printStackTrace();
			}
		} catch (Exception e) {
			result.put("response", Commons.ERROR);
			e.printStackTrace();
		}
		Log.i("TAG", "result balikan : " + result);
		return result;
	}

	

	@SuppressWarnings("unchecked")
	public HashMap<Object, Object> getAPIRequest(int apiIndex, JSONObject json,
			HashMap<String, String> parameters, boolean hasMultiEntity,
			Bundle bundle) {
		HashMap<Object, Object> result = new HashMap<Object, Object>();
		try {
			Log.i("TAG", APIParam.urlBuilder(apiIndex, parameters));
			HttpResponse response;
			HttpClient myClient = new DefaultHttpClient();
			HttpPost myConnection = new HttpPost(APIParam.urlBuilder(apiIndex,
					parameters));
			Log.i("TAG", "JSON " + json.toString());
			myConnection.setHeader("json", json.toString());
			StringEntity se = new StringEntity(json.toString(), "UTF-8");
			se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
					"application/json"));
			myConnection.setEntity(se);
			// TODO BUAT MULTIPART ENTITY
			if (hasMultiEntity) {
				Log.i("TAG", "MASUK ME");

				String[] arrKey = bundle.getStringArray("key_string");
				String file_type = bundle.getString("type_file");
				Log.i("TAG", "file_type : " + file_type);
				if (file_type.equalsIgnoreCase(".png")) {
					Log.i("TAG", "cuma gambar");
					MultipartEntity mpEntity = new MultipartEntity(
							HttpMultipartMode.BROWSER_COMPATIBLE);
					for (int i = 0; i < arrKey.length; i++) {
						Log.i("TAG", "arrkey : " + arrKey[i]);
						if (arrKey[i].toString().length() > 0) {
							mpEntity.addPart(arrKey[i], new ByteArrayBody(
									bundle.getByteArray(arrKey[i]), arrKey[i]
											+ file_type));
						}
					}
					myConnection.setEntity(mpEntity);
				} else {
					MultipartEntity mpEntity = new MultipartEntity(
							HttpMultipartMode.BROWSER_COMPATIBLE);
					Log.i("TAG", "ME : mpEntity : " + mpEntity);
					for (int i = 0; i < arrKey.length; i++) {
						Log.i("TAG", "arrkey : " + arrKey[i]);
						if (arrKey[i].toString().length() > 0) {
							File file = new File(
									bundle.getStringArray(arrKey[i])[1]);
							Log.i("TAG", "ME : arrKey[i] : " + arrKey[i]);
							FileBody fileBody = new FileBody(file,
									bundle.getStringArray(arrKey[i])[2]);
							Log.i("TAG",
									"key : "
											+ bundle.getStringArray(arrKey[i])[0]);
							mpEntity.addPart(
									bundle.getStringArray(arrKey[i])[0],
									fileBody);
						}
					}
					myConnection.setEntity(mpEntity);
				}
			}
			try {
				response = myClient.execute(myConnection);
				String JSONString = EntityUtils.toString(response.getEntity(),
						"UTF-8");
				Log.i("TAG", "Response " + JSONString);
				if (isValidJSON(JSONString)) {
					result = new ObjectMapper().readValue(JSONString,
							HashMap.class);
					if (result.get("response").equals("success")) {
						result.put("response",Commons.SUCCESS);
						result.put("data", result.get("data"));
					} else if (result.get("response").equals("error")) {
						result.put("response", Commons.ERROR);
					} else {
						// TODO INVALID VALUE
					}
				} else {
					// TODO INVALID JSON
				}

			} catch (ClientProtocolException e) {
				result.put("response", Commons.ERROR);
				e.printStackTrace();
			} catch (IOException e) {
				result.put("response", Commons.ERROR);
				e.printStackTrace();
			}
		} catch (Exception e) {
			result.put("response", Commons.ERROR);
			e.printStackTrace();
		}
		return result;
	}

	public boolean isValidJSON(final String json) {
		boolean valid = false;
		try {
			final JsonParser parser = new ObjectMapper().getFactory()
					.createParser(json);
			Log.i("TAG", "jsonparser : " + parser.getText());
			while (parser.nextToken() != null) {
			}
			valid = true;
		} catch (JsonParseException jpe) {
			jpe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}

		return valid;
	}
}