package com.andtechnology.indahjaya.ui.service;

import java.lang.ref.WeakReference;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import com.andtechnology.indahjaya.utils.Commons;

public class IndahJayaService extends Service {
	private final Messenger mService = new Messenger(new ServiceHandler(this));
	private APIRequest apiRequest;

	@Override
	public void onCreate() {
		super.onCreate();
		apiRequest = new APIRequest();
		Log.i("TAG", "service oncreate");
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return mService.getBinder();
	}

	static class ServiceHandler extends Handler {
		WeakReference<IndahJayaService> screen;

		public ServiceHandler(IndahJayaService myScreen) {
			screen = new WeakReference<IndahJayaService>(myScreen);
		}

		@Override
		public void handleMessage(Message msg) {
			Log.i("TAG", "MESSAGE " + msg.getData());
			screen.get().runAPIRequest(msg);
		}
	}

	public void runAPIRequest(Message msg) {
		final int apiIndex = msg.what;
		final Bundle bundle = msg.getData();
		final Messenger mReplyTo = msg.replyTo;

		new Thread(new Runnable() {
			@SuppressWarnings("unchecked")
			@Override
			public void run() {
				HashMap<String, String> parameters = (HashMap<String, String>) bundle
						.getSerializable("parameters");
				HashMap<Object, Object> response = null;
				if (bundle.getInt("type") == Commons.POST_WITH_JSON) {
					JSONObject object = null;
					try {
						object = new JSONObject(bundle.getString("json"));
					} catch (JSONException e) {
						e.printStackTrace();
					}
					response = apiRequest.getAPIRequest(apiIndex, object,
							parameters, bundle.getBoolean("MultiEntity"),
							bundle.getBundle("bundle"));
				} else if (bundle.getBoolean("for_httpost")) {
						response = apiRequest.getAPIRequest(apiIndex, parameters,
							bundle.getBoolean("MultiEntity"),
							bundle.getBundle("bundle"),
							bundle.getBoolean("for_httpost"));
				} else {
					response = apiRequest.getAPIRequest(apiIndex, parameters,
							bundle.getBoolean("MultiEntity"),
							bundle.getBundle("bundle"));
				}
				Message message = Message.obtain(null, apiIndex);
				Bundle bMessage = new Bundle();
				bMessage.putSerializable("response", response);
				message.setData(bMessage);
				try {
					mReplyTo.send(message);
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}
		}).start();
	}

}