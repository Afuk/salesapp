package com.andtechnology.indahjaya.ui;

import java.util.HashMap;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andtechnology.indahjaya.R;
import com.andtechnology.indahjaya.database.DatabaseHandler;
import com.andtechnology.indahjaya.lib.util.GPSTracker;
import com.andtechnology.indahjaya.lib.util.SessionManagement;
import com.andtechnology.indahjaya.lib.util.UtilsPointer;
import com.andtechnology.indahjaya.service.MyService;
import com.andtechnology.indahjaya.ui.customview.CustomDialogFragment;
import com.andtechnology.indahjaya.utils.Commons;
import com.andtechnology.indahjaya.view.journey.ActivityAddJobs;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingFragmentActivity;

public class Home extends SlidingFragmentActivity implements OnClickListener {
	public int test = 10;
	private Context context;
	private int notif_flag = 0;

	@Override
	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		context = this;
		setContentView(R.layout.layout_main_menu);
		startService(new Intent(getBaseContext(), MyService.class));
		setBehindContentView(R.layout.layout_side_main_menu);

		initSliding();
		initView();
		initGPSTracking();
		if (getIntent().getExtras() != null) {
			if (getIntent().getExtras().containsKey("NOTIFICATION_FLAG")) {
				notif_flag = getIntent().getExtras()
						.getInt("NOTIFICATION_FLAG");
				if (notif_flag == 1) {
					FragmentManager fragmentManager = getSupportFragmentManager();
					fragmentManager.beginTransaction()
							.replace(R.id.llContainer, new Inbox()).commit();
					NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
					notificationManager.cancelAll();
				}
			} else {
				startFragment();
			}
		} else {
			startFragment();
		}
	}

	public void initGPSTracking() {
		GPSTracker gpsTracker = new GPSTracker(this);
		if (!gpsTracker.canGetLocation()) {
			gpsTracker.turnOnGps();

		}
	}

	public void startFragment() {
		FragmentManager fragmentManager = getSupportFragmentManager();
		fragmentManager.beginTransaction()
				.replace(R.id.llContainer, new TodayStore()).commit();
	}

	public void initView() {
		((ImageView) findViewById(R.id.ivToogle)).setOnClickListener(this);
		((LinearLayout) findViewById(R.id.llStoreRecommendation))
				.setOnClickListener(this);

		((LinearLayout) findViewById(R.id.llTodayStore))
				.setOnClickListener(this);
		((LinearLayout) findViewById(R.id.llMarketSurvey))
		.setOnClickListener(this);
		((LinearLayout) findViewById(R.id.llPromoInfo))
				.setOnClickListener(this);
		((LinearLayout) findViewById(R.id.llInbox)).setOnClickListener(this);
		DatabaseHandler db = new DatabaseHandler(getApplicationContext());
		int message = db.getMessageCount();
		((TextView) findViewById(R.id.tvCount)).setText("" + message);
		((LinearLayout) findViewById(R.id.llSetting)).setOnClickListener(this);

		((ImageView) findViewById(R.id.ivAddActivity))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent i = new Intent(v.getContext(),
								ActivityAddJobs.class);
						i.putExtra("getId", 0);
						startActivityForResult(i, Commons.FROM_ADD_JOBS);

					}
				});

		((LinearLayout) findViewById(R.id.llLogout))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						final CustomDialogFragment dialog = new CustomDialogFragment();
						dialog.setTitle("Confirmation : ");
						dialog.setDescription("Do you want logout?");
						dialog.setYesNo(true);
						dialog.setYesNoListener(new CustomDialogFragment.YesNoAction() {

							@Override
							public void onYes(View view) {
								// TODO Auto-generated method stub
								SessionManagement session = new SessionManagement(
										getBaseContext());
								HashMap<String, String> user = session
										.getUserDetails();
								session.clearLogin();
								DatabaseHandler db = new DatabaseHandler(
										getApplicationContext());
								db.deletelastupdate();
								stopService(new Intent(getBaseContext(),
										MyService.class));
								UtilsPointer.goToLogin((Activity) context);
								finish();
							}

							@Override
							public void onNo(View view) {
								// TODO Auto-generated method stub
								dialog.dismiss();
							}
						});
						dialog.show(getSupportFragmentManager(), "dialog");
					}
				});

	}

	public void initSliding() {
		SlidingMenu sm = getSlidingMenu();
		sm.setShadowWidthRes(R.dimen.shadow_width);
		sm.setShadowDrawable(R.drawable.shadow);
		sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		sm.setFadeDegree(0.35f);
		// sm.setBehindWidth(250);
		sm.setTouchModeAbove(SlidingMenu.LEFT);

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		toggle();
		FragmentManager fragmentManager = getSupportFragmentManager();

		((ImageView) findViewById(R.id.ivAddActivity)).setVisibility(View.GONE);
		switch (v.getId()) {
		case R.id.ivToogle:

			break;
		case R.id.llStoreRecommendation:

			fragmentManager.beginTransaction()
					.replace(R.id.llContainer, new StoreRecommendation())
					.commit();

			break;

		case R.id.llTodayStore:
			((ImageView) findViewById(R.id.ivAddActivity))
					.setVisibility(View.VISIBLE);
			fragmentManager.beginTransaction()
					.replace(R.id.llContainer, new TodayStore()).commit();

			break;
			
		case R.id.llMarketSurvey:

			fragmentManager.beginTransaction()
					.replace(R.id.llContainer, new MarketSurveyList()).commit();

			break;

		case R.id.llPromoInfo:
			fragmentManager.beginTransaction()
					.replace(R.id.llContainer, new PromoInfo()).commit();
			break;

		case R.id.llInbox:
			fragmentManager.beginTransaction()
					.replace(R.id.llContainer, new Inbox()).commit();

			break;

		case R.id.llSetting:
			fragmentManager.beginTransaction()
					.replace(R.id.llContainer, new Setting()).commit();

			break;

		default:
			break;
		}
	}

}
