package com.andtechnology.indahjaya.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;

import com.andtechnology.indahjaya.R;
import com.andtechnology.indahjaya.adapter.FragmentInboxAdapter;
import com.andtechnology.indahjaya.database.DatabaseHandler;
import com.andtechnology.indahjaya.lib.util.SessionManagement;
import com.andtechnology.indahjaya.lib.util.Utils;
import com.andtechnology.indahjaya.model.MessageModel;
import com.andtechnology.indahjaya.model.NavDrawerMenuModel;
import com.andtechnology.indahjaya.view.framework.FragmentFramework;
import com.andtechnology.indahjaya.view.inbox.ActivityInboxDetail;
import com.andtechnology.indahjaya.view.journey.activejobs.FragmentActivities;

public class Inbox extends FragmentFramework{
	View publicRootView;
	ArrayList<NavDrawerMenuModel> navDrawerMenu;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.layout_inbox,
				container, false);
		loadData(rootView);
		publicRootView = rootView;
		return rootView;
	}
	
	public void loadData(View rootView) {
		DatabaseHandler db = new DatabaseHandler(rootView.getContext());
		SessionManagement session = new SessionManagement(activity);
		HashMap<String, String> user = session.getUserDetails();
		List<MessageModel> message = db.getAllMessage();
		if(message.size() == 0) {
			rootView.findViewById(R.id.EmptyIndicator).setVisibility(View.VISIBLE);
		} else {
			rootView.findViewById(R.id.EmptyIndicator).setVisibility(View.INVISIBLE);			
		}

		navDrawerMenu = new ArrayList<NavDrawerMenuModel>();
		ListView mDrawerList = (ListView) rootView.findViewById(R.id.lvInboxList);

		NavDrawerMenuModel model;

		for (MessageModel cn : message) {
			model = new NavDrawerMenuModel();
			model.setId(cn.get_message_id());
			model.setDate(cn.get_date_change());
			model.setSubject("Subject  :  "+cn.get_subject());
			model.setFrom("FROM     :  " + cn.get_super_user_id() + " - " + cn.get_super_user_name());
			model.setDescription(cn.get_body());
			model.setFlag(cn.get_status());
			navDrawerMenu.add(model);
		}
		// setting the nav drawer list adapter
		FragmentInboxAdapter adapter = new FragmentInboxAdapter(activity, navDrawerMenu);
		mDrawerList.setAdapter(adapter);

		mDrawerList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				NavDrawerMenuModel model = (NavDrawerMenuModel) parent
						.getAdapter().getItem(position);
				
				Intent i = new Intent(v.getContext(), ActivityInboxDetail.class);
				i.putExtra("getId", model.getId());
				i.putExtra("getFlag", model.getFlag());
				i.putExtra("getSubject", model.getSubject());
				i.putExtra("getDate", model.getDate());
				i.putExtra("getFrom", model.getFrom());
				i.putExtra("getDescription", model.getDescription());
				startActivityForResult(i, 909);

			}
		});
		
	}

	
	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		navDrawerMenu.removeAll(navDrawerMenu);
		loadData(publicRootView);
	}
    

}
