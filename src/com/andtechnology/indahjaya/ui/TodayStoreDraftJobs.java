package com.andtechnology.indahjaya.ui;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.andtechnology.indahjaya.R;
import com.andtechnology.indahjaya.adapter.ListFragmentActiveJobsAdapter;
import com.andtechnology.indahjaya.database.DatabaseHandler;
import com.andtechnology.indahjaya.lib.util.SessionManagement;
import com.andtechnology.indahjaya.lib.util.Utils;
import com.andtechnology.indahjaya.model.MappingStoreModel;
import com.andtechnology.indahjaya.model.NavDrawerMenuModel;
import com.andtechnology.indahjaya.view.journey.activejobs.ActivityActiveJobs;
import com.andtechnology.indahjaya.view.journey.activejobs.ActivityActiveJobsView;

public class TodayStoreDraftJobs extends Fragment {
	 String formattedDate;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.layout_today_store_draft_job, container,
				false);		
		initDate(rootView);
		return rootView;
	}
	
	public void initDate(View view)
	{
		SimpleDateFormat df = new SimpleDateFormat("EEEE, dd MMMM yyyy");
		formattedDate = df.format(new Date());
		TextView tv_today = (TextView) view.findViewById(R.id.tvToday);
		tv_today.setText(formattedDate);
		LoadList(view);
	}
	
	public void LoadList(View view) {

		NavDrawerMenuModel model;
		SessionManagement session = new SessionManagement(getActivity());
		HashMap<String, String> user = session.getUserDetails();
		DatabaseHandler db = new DatabaseHandler(getActivity());
		SimpleDateFormat dfg = new SimpleDateFormat("yyyy-MM-dd");
		List<MappingStoreModel> mappingstoremodel = db.getAllMappingStore(1, user.get(session.USER_ID),dfg.format(new Date()));
		
//		mappingstoremodel.addAll(db.getAllMappingStore(2, user.get(session.USER_ID),dfg.format(new Date())));
	//	List<MappingStoreModel> mappingstoremodel2 = ;
		if(mappingstoremodel.size() == 0) {
			view.findViewById(R.id.EmptyIndicator).setVisibility(View.VISIBLE);
        } else {
        	view.findViewById(R.id.EmptyIndicator).setVisibility(View.INVISIBLE);            
        }
		ArrayList<MappingStoreModel>	navDrawerMenu = new ArrayList<MappingStoreModel>();
		ListView mDrawerList = (ListView) view.findViewById(R.id.lvDraftJobs);
		
		
		ListFragmentActiveJobsAdapter adapter = new ListFragmentActiveJobsAdapter(getActivity(), (ArrayList<MappingStoreModel>)mappingstoremodel);
		mDrawerList.setAdapter(adapter);
		adapter.notifyDataSetChanged();
		
		mDrawerList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				
				MappingStoreModel model = (MappingStoreModel) parent
						.getAdapter().getItem(position);
				Intent i = null;
				//if(model.getFlag().equals("1")) {
					i = new Intent(v.getContext(), ActivityActiveJobs.class);
			//	} else {
//					i = new Intent(v.getContext(), ActivityActiveJobsView.class);	
				//}
//				i.putExtra("getTitle", model.get_store_id());
//				i.putExtra("getId", model.getIds());
//				i.putExtra("getFinalId", model.getId());
//				i.putExtra("getDate", formattedDate);
				
					i.putExtra("getTitle",
							model.get_store_name());
					i.putExtra("getId", model.get_store_id());
					i.putExtra("getFinalId", 1);
					i.putExtra("getDate", formattedDate);
					i.putExtra("latitude_store",
							"" + model.get_latitude());
					i.putExtra("longitude_store",
							"" + model.get_longtitude());
					i.putExtra("mapping_store_id",
							"" + model.get_mapping_store_id());
					i.putExtra("progress_status",
							"" + model.get_progress_status());
					i.putExtra("date_checkin",
							"" + Utils.getTimeNow());
				
				startActivityForResult(i, 901);
			}
		});
		
	}

}