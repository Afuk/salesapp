package com.andtechnology.indahjaya;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.andtechnology.indahjaya.builder.SpinnerBuilder;
import com.andtechnology.indahjaya.database.DatabaseHandler;
import com.andtechnology.indahjaya.model.QuestionModel;
import com.andtechnology.indahjaya.model.TypeActivitiesAnswerModel;
import com.andtechnology.indahjaya.model.TypeActivitiesModel;
import com.andtechnology.indahjaya.model.TypeActivitiesQuestionModel;
import com.andtechnology.indahjaya.view.framework.ActivityFramework;

public class AddQuestion extends ActivityFramework {

	private QuestionModel questionModel = new QuestionModel();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_question);
		initialTypeActSpinner();

		initButton();
	}

	public void initButton() {
		((Button) findViewById(R.id.btnAdd))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (questionModel.getType_activity_id() == 0
								|| questionModel.getQuestion_id() == 0
								|| questionModel.getAnswer_id() == 0) {
							Toast.makeText(getApplicationContext(),
									"All field must be selected!",
									Toast.LENGTH_SHORT).show();
						} else {
							Intent i = new Intent();
							i.putExtra("question_model", questionModel);
							setResult(RESULT_OK, i);
							finish();
						}
					}
				});

		ImageView iv_back = (ImageView) findViewById(R.id.iv_back);
		iv_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	public void initialTypeActSpinner() {
		final SpinnerBuilder sFactory = new SpinnerBuilder(activity);

		DatabaseHandler db = new DatabaseHandler(activity);
		List<TypeActivitiesModel> typeActivitiesList = new ArrayList<TypeActivitiesModel>();
		TypeActivitiesModel select = new TypeActivitiesModel();
		select.set_type_activities_id(0);
		select.set_type_activities_name("-Select-");
		typeActivitiesList.add(select);
		typeActivitiesList.addAll(db.getAllTypeActivities());

		sFactory.createSpinner(R.id.spTypeActivity, typeActivitiesList);
		final Spinner spTypeActivity = (Spinner) findViewById(R.id.spTypeActivity);
		spTypeActivity.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub

				TypeActivitiesModel typeActivity = (TypeActivitiesModel) parent
						.getAdapter().getItem(position);
				questionModel.setType_activity_id(typeActivity
						.get_type_activities_id());
				questionModel.setType_activity_value(typeActivity
						.get_type_activities_name());
				int typeActivities = typeActivity.get_type_activities_id();
				initialQuestionSpinner(typeActivities);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}

		});
	}

	public void initialQuestionSpinner(int typeActivity) {
		final SpinnerBuilder sFactory = new SpinnerBuilder(activity);

		DatabaseHandler db = new DatabaseHandler(activity);

		TypeActivitiesQuestionModel select = new TypeActivitiesQuestionModel();
		select.set_type_activities_question_id(0);
		select.set_type_activities_question_name("-Select-");

		List<TypeActivitiesQuestionModel> typeQuestionList = new ArrayList<TypeActivitiesQuestionModel>();
		typeQuestionList.add(select);
		typeQuestionList.addAll(db.getAllTypeActivitiesQuestion(typeActivity));

		sFactory.createSpinner(R.id.spQuestion, typeQuestionList);
		final Spinner spQuestion = (Spinner) findViewById(R.id.spQuestion);
		spQuestion.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				TypeActivitiesQuestionModel typeQuestion = (TypeActivitiesQuestionModel) parent
						.getAdapter().getItem(position);
				questionModel.setQuestion_id(typeQuestion
						.get_type_activities_question_id());
				questionModel.setQuestion_value(typeQuestion
						.get_type_activities_question_name());
				initialAnswerSpinner(typeQuestion
						.get_type_activities_question_id());

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}

		});
	}

	public void initialAnswerSpinner(int typeQuestion) {
		final SpinnerBuilder sFactory = new SpinnerBuilder(activity);

		DatabaseHandler db = new DatabaseHandler(activity);

		TypeActivitiesAnswerModel select = new TypeActivitiesAnswerModel();
		select.set_type_activities_answer_id(0);
		select.set_type_activities_answer_name("-Select-");

		List<TypeActivitiesAnswerModel> typeAnswerList = new ArrayList<TypeActivitiesAnswerModel>();
		typeAnswerList.add(select);
		typeAnswerList.addAll(db.getAllTypeActivitiesAnswer(typeQuestion));

		sFactory.createSpinner(R.id.spAnswer, typeAnswerList);
		final Spinner spAnswer = (Spinner) findViewById(R.id.spAnswer);
		spAnswer.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				TypeActivitiesAnswerModel typeAnswer = (TypeActivitiesAnswerModel) parent
						.getAdapter().getItem(position);
				questionModel.setAnswer_id(typeAnswer
						.get_type_activities_answer_id());
				questionModel.setAnswer_value(typeAnswer
						.get_type_activities_answer_name());
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}

		});
	}
}
