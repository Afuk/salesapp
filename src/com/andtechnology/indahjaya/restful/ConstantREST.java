package com.andtechnology.indahjaya.restful;

public final class ConstantREST {
	
	public final static String ProductionHttp="http://110.93.14.85:8080/indahjaya/index.php";
	public final static String ImagePath = "http://110.93.14.85:8080/indahjaya/asset/promo_photo/";
	public final static String ImagePath1 = "http://110.93.14.85:8080/indahjaya/asset/activity_photo/";
	public final static String ImagePath2 = "http://110.93.14.85:8080/indahjaya/asset/outlet_recommendation_photo/";
	
	
	
	//TODO CHANGE ProductionHttp FOR API PRODUCTION
	public final static String API1 = ProductionHttp + "/api/syncLogin";
	public final static String API2 = ProductionHttp + "/api/sysc_count_message";
	public final static String API3 = ProductionHttp + "/api/sysc_message";
	public final static String API4 = ProductionHttp + "/api/sysc_update_message";
	public final static String API5 = ProductionHttp + "/api/insert_final_activities";
	public final static String API7 = ProductionHttp + "/api/insert_market_intelligence";
	public final static String API8 = ProductionHttp + "/api/insert_detail_market_intelligence";
	public final static String API9 = ProductionHttp + "/api/insert_store_registration";
	public final static String API10 = ProductionHttp + "/api/check_updated_data";
	public final static String API16 = ProductionHttp + "/api/change_password";

	public final static String API5_PMR = ProductionHttp + "/api/insert_final_activities_prm";
	public final static String API6_PMR = ProductionHttp + "/api/insert_detail_activities_prm";
	public final static String API14_PMR = ProductionHttp + "/api/insert_sales_prm";
	public final static String API15_PMR = ProductionHttp + "/api/insert_stock_report_prm";
	
	
}
