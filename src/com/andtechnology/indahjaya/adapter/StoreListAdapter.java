package com.andtechnology.indahjaya.adapter;

import java.util.ArrayList;
import java.util.List;

import com.andtechnology.indahjaya.R;
import com.andtechnology.indahjaya.model.ModelStore;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class StoreListAdapter extends ArrayAdapter<ModelStore> {
	ArrayList<ModelStore> store_list;

	public StoreListAdapter(Context context, int textViewResourceId,
			ArrayList<ModelStore> store_list) {
		super(context, textViewResourceId, store_list);
		// TODO Auto-generated constructor stub
		this.store_list = store_list;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return store_list.size();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView==null)
			convertView =LayoutInflater.from(getContext()).inflate(R.layout.layout_store_list_child, null);
		
		final ModelStore model =store_list.get(position);
		((TextView)convertView.findViewById(R.id.tvStore_Name)).setText(model.getStore_name());
		((TextView)convertView.findViewById(R.id.tvDescription)).setText(model.getDescription());

		String status = null;
		if(model.getStatus().equals("2")) {
			status = "Published";		
		} else if(model.getStatus().equals("1") || model.getStatus().equals("0")) {	
			status = "Draft";
		} else if(model.getStatus().equals("102")) {	
			status = "Approve";
		} else if(model.getStatus().equals("101")) {	
			status = "Reject";
		} else if(model.getStatus().equals("103")) {	
			status = "Order";
		}
		((TextView)convertView.findViewById(R.id.tvStatus)).setText(status);
		return convertView;
	}
}
