package com.andtechnology.indahjaya.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.andtechnology.indahjaya.model.NavDrawerMenuModel;
import com.andtechnology.indahjaya.R;

public class NavDrawerMenuAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<NavDrawerMenuModel> navDrawerItems;

	public NavDrawerMenuAdapter(Context context,
			ArrayList<NavDrawerMenuModel> navDrawerItems) {
		this.context = context;
		this.navDrawerItems = navDrawerItems;
	}

	@Override
	public int getCount() {
		return navDrawerItems.size();
	}

	@Override
	public Object getItem(int position) {
		return navDrawerItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater mInflater = (LayoutInflater) context
					.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = LayoutInflater.from(context).inflate(
					R.layout.drawer_list_menu, null);

		}
		NavDrawerMenuModel modelNavDrawer = navDrawerItems.get(position);
		ImageView imgIcon = (ImageView) convertView.findViewById(R.id.ivIconMenu);
		TextView txtTitle = (TextView) convertView.findViewById(R.id.tvTitleMenu);
		TextView tvCount = (TextView) convertView.findViewById(R.id.tvCount);
		
		txtTitle.setText(modelNavDrawer.getTitle());
		imgIcon.setImageResource(modelNavDrawer.getIcon());
		if(modelNavDrawer.getCount().equals("")) {
			tvCount.setVisibility(View.GONE);			
		} else {
			tvCount.setVisibility(View.VISIBLE);	
			tvCount.setText(modelNavDrawer.getCount());
			
		}

		return convertView;
	}

}