package com.andtechnology.indahjaya.adapter;

import java.util.ArrayList;
import java.util.List;

import com.andtechnology.indahjaya.R;
import com.andtechnology.indahjaya.model.QuestionModel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class QuestionListAdapter extends ArrayAdapter<QuestionModel>{

	ArrayList<QuestionModel> question_list;
	public QuestionListAdapter(Context context, int resource,
			ArrayList<QuestionModel> objects) {
		super(context, resource, objects);
		question_list =objects;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null)
        	convertView =LayoutInflater.from(getContext()).inflate(R.layout.layout_question_list_ch, null);
		QuestionModel model = question_list.get(position);
		((TextView)convertView.findViewById(R.id.tvTypeActivity)).setText(""+model.getType_activity_value());
		((TextView)convertView.findViewById(R.id.tvQuestion)).setText(""+model.getQuestion_value());
		((TextView)convertView.findViewById(R.id.tvAnswer)).setText(""+model.getAnswer_value());
      
        return convertView;
	}

}
