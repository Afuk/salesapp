package com.andtechnology.indahjaya.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.andtechnology.indahjaya.model.NavDrawerMenuModel;
import com.andtechnology.indahjaya.model.StoreRecommendationModel;
import com.andtechnology.indahjaya.R;

public class FragmentStoreRegistrationAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<StoreRecommendationModel> navDrawerItems;

	public FragmentStoreRegistrationAdapter(Context context,
			ArrayList<StoreRecommendationModel> navDrawerItems) {
		this.context = context;
		this.navDrawerItems = navDrawerItems;
	}

	@Override
	public int getCount() {
		return navDrawerItems.size();
	}

	@Override
	public Object getItem(int position) {
		return navDrawerItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater mInflater = (LayoutInflater) context
					.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = LayoutInflater.from(context).inflate(
					R.layout.drawer_list_market_intelligence, null);
		}
		
		StoreRecommendationModel modelNavDrawer = navDrawerItems.get(position);
		TextView tv_Store_Name = (TextView) convertView
				.findViewById(R.id.tv_Store_Name);
		TextView tv_description = (TextView) convertView
				.findViewById(R.id.tv_description);
		TextView tv_status = (TextView) convertView
				.findViewById(R.id.tv_status);
		String status;
		if(modelNavDrawer.get_proggress_status() == 3) {
			status = "Published";		
		} else {	
			status = "Draft";
		}
		tv_Store_Name.setText("Store Name : " + modelNavDrawer.get_store_name());
		tv_description.setText("Store Owner : " + modelNavDrawer.get_store_owner());
		tv_status.setText("Status : " + status);

		return convertView;
	}

}