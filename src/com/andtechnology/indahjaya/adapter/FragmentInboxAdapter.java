package com.andtechnology.indahjaya.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.andtechnology.indahjaya.model.NavDrawerMenuModel;
import com.andtechnology.indahjaya.R;

public class FragmentInboxAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<NavDrawerMenuModel> navDrawerItems;

	public FragmentInboxAdapter(Context context,
			ArrayList<NavDrawerMenuModel> navDrawerItems) {
		this.context = context;
		this.navDrawerItems = navDrawerItems;
	}

	@Override
	public int getCount() {
		return navDrawerItems.size();
	}

	@Override
	public Object getItem(int position) {
		return navDrawerItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater mInflater = (LayoutInflater) context
					.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = LayoutInflater.from(context).inflate(
					R.layout.drawer_list_inbox, null);
		}

		NavDrawerMenuModel modelNavDrawer = navDrawerItems.get(position);
		TextView tv_date = (TextView) convertView
				.findViewById(R.id.tv_date);
		TextView tv_subject = (TextView) convertView
				.findViewById(R.id.tv_subject);
		TextView tv_from = (TextView) convertView
				.findViewById(R.id.tv_from);
		TextView tv_status = (TextView) convertView
				.findViewById(R.id.tv_status);
		tv_date.setText(modelNavDrawer.getDate());
		tv_subject.setText(modelNavDrawer.getSubject());
		tv_from.setText(modelNavDrawer.getFrom());
		if(modelNavDrawer.getFlag().equals("0")) {
			tv_status.setText("New");
		} else {
			tv_status.setText("");			
		}

		return convertView;
	}

}