package com.andtechnology.indahjaya.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.andtechnology.indahjaya.lib.util.Utils;
import com.andtechnology.indahjaya.model.ActivitiesModel;
import com.andtechnology.indahjaya.model.MappingSalesToStoreModel;
import com.andtechnology.indahjaya.model.NavDrawerMenuModel;
import com.andtechnology.indahjaya.R;

public class ListActivitiesAddJobs extends BaseAdapter {

	private Context context;
	private ArrayList<MappingSalesToStoreModel> navDrawerItems;

	public ListActivitiesAddJobs(Context context,
			ArrayList<MappingSalesToStoreModel> navDrawerItems) {
		this.context = context;
		this.navDrawerItems = navDrawerItems;
	}

	@Override
	public int getCount() {
		return navDrawerItems.size();
	}

	@Override
	public Object getItem(int position) {
		return navDrawerItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater mInflater = (LayoutInflater) context
					.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = LayoutInflater.from(context).inflate(
					R.layout.drawer_list_active_jobs, null);

		}
		MappingSalesToStoreModel modelNavDrawer = navDrawerItems.get(position);
		ImageView imgIcon = (ImageView) convertView
				.findViewById(R.id.ivIconMenu);
		TextView tv_Oulite_Id = (TextView) convertView
				.findViewById(R.id.tv_Oulite_Id);
		TextView tv_Store_Name = (TextView) convertView
				.findViewById(R.id.tv_Store_Name);
		TextView tv_Address = (TextView) convertView
				.findViewById(R.id.tv_Address);
		tv_Oulite_Id.setText("Store ID : " + modelNavDrawer.get_store_id());
		tv_Store_Name
				.setText("Store Name : " + modelNavDrawer.get_store_name());
		tv_Address.setText("Address : " + modelNavDrawer.get_address());
		return convertView;
	}

}