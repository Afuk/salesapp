package com.andtechnology.indahjaya.adapter;
import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.andtechnology.indahjaya.R;
import com.andtechnology.indahjaya.lib.util.Utils;
import com.andtechnology.indahjaya.model.SubCategoryProductModel;

public class SpinnerCustomAdapter extends ArrayAdapter<SubCategoryProductModel> {
	
	private Context context;
	private int resource;
	private ArrayList<SubCategoryProductModel> listCategory;

	public SpinnerCustomAdapter(Context context, int resource,
			ArrayList<SubCategoryProductModel> objects) {
		super(context, resource, objects);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.resource = resource;
		this.listCategory = objects;
	}
	
	@Override
	  public View getDropDownView(int position, View convertView,
	    ViewGroup parent) {
	   // TODO Auto-generated method stub
	   return getCustomView(position, convertView, parent);
	  }

//	  @Override
//	  public View getView(int position, View convertView, ViewGroup parent) {
//	   // TODO Auto-generated method stub
//	   return getCustomView(position, convertView, parent);
//	  }

	  public View getCustomView(int position, View convertView, ViewGroup parent) {
	   // TODO Auto-generated method stub
	   //return super.getView(position, convertView, parent);
        if(convertView==null)
        	convertView=LayoutInflater.from(context).inflate(R.layout.layout_spinner_child, null);

	   TextView label=(TextView)convertView.findViewById(R.id.tvSpinner);
	   label.setText(listCategory.get(position).get_sub_category_product_name());
	  /* label.setHeight((int) Utils.convertDpToPixel(40, context));
	   label.setTextSize(16);
	   if(position == 0){
		   label.setHeight(0);
		   label.setVisibility(View.GONE);
	   }*/
	   
	   // Hide scroll bar because it appears sometimes unnecessarily, this does not prevent scrolling
	   parent.setVerticalScrollBarEnabled(false);
	   
	   return convertView;
	  } 
}
