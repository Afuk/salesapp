package com.andtechnology.indahjaya.adapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.andtechnology.indahjaya.R;
import com.andtechnology.indahjaya.lib.util.Utils;
import com.andtechnology.indahjaya.model.NavDrawerMenuModel;
import com.andtechnology.indahjaya.restful.ConstantREST;

public class FragmentPromoInformationAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<NavDrawerMenuModel> navDrawerItems;

	public FragmentPromoInformationAdapter(Context context,
			ArrayList<NavDrawerMenuModel> navDrawerMenu) {
		this.context = context;
		this.navDrawerItems = navDrawerMenu;
	}

	@Override
	public int getCount() {
		return navDrawerItems.size();
	}

	@Override
	public Object getItem(int position) {
		return navDrawerItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater mInflater = (LayoutInflater) context
					.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = LayoutInflater.from(context).inflate(
					R.layout.drawer_list_product_information, null);
		}

		final NavDrawerMenuModel modelNavDrawer = navDrawerItems.get(position);
		TextView tv_program_name = (TextView) convertView
				.findViewById(R.id.tv_program_name);
		final ImageView ivIconMenu = (ImageView) convertView
				.findViewById(R.id.ivIconMenus);

		AQuery aQuery = new AQuery(context);
		aQuery.id(ivIconMenu).image(
				ConstantREST.ImagePath + modelNavDrawer.getImage_name(), true,
				true, Utils.getDisplayWidth(context), 0);

		SimpleDateFormat dfg = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
		Calendar c = Calendar.getInstance();
		try {
			c.setTime(dfg.parse(modelNavDrawer.getStart_date()));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Calendar d = Calendar.getInstance();
		try {
			d.setTime(dfg.parse(modelNavDrawer.getEnd_date()));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		tv_program_name.setText("Promo : " + modelNavDrawer.getTitle() + "\n"
				+ df.format(c.getTime()) + " - " + df.format(d.getTime()));

		// final String images = modelNavDrawer.getImage_name();
		// new Thread(new Runnable() {
		// public void run() {
		// File file = new File(ConstantPATH.PATH2+images);
		// if (file.exists()) {
		// ivIconMenu.setImageURI(Uri.parse(new
		// File(ConstantPATH.PATH2+images).toString()));
		// }
		// }
		// }).run();

		return convertView;
	}

}