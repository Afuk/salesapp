package com.andtechnology.indahjaya.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.andtechnology.indahjaya.R;
import com.andtechnology.indahjaya.model.MappingStoreModel;

public class ListFragmentActiveJobsAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<MappingStoreModel> navDrawerItems;

	public ListFragmentActiveJobsAdapter(Context context,
			ArrayList<MappingStoreModel> navDrawerItems) {
		this.context = context;
		this.navDrawerItems = navDrawerItems;
	}

	@Override
	public int getCount() {
		return navDrawerItems.size();
	}

	@Override
	public Object getItem(int position) {
		return navDrawerItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
//		if (convertView == null) {
			LayoutInflater mInflater = (LayoutInflater) context
					.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = LayoutInflater.from(context).inflate(
					R.layout.drawer_list_active_jobs, null);

//		}
		MappingStoreModel modelNavDrawer = navDrawerItems.get(position);
		ImageView imgIcon = (ImageView) convertView
				.findViewById(R.id.ivIconMenu);
		TextView tv_Oulite_Id = (TextView) convertView
				.findViewById(R.id.tv_Oulite_Id);
		TextView tv_Store_Name = (TextView) convertView
				.findViewById(R.id.tv_Store_Name);
		TextView tv_Address = (TextView) convertView
				.findViewById(R.id.tv_Address);

		SimpleDateFormat dfg = new SimpleDateFormat("yyyy-MM-dd");
		Log.d("date : ", ""+modelNavDrawer.get_date()+" - "+dfg.format(new Date()));
		if(modelNavDrawer.get_date().equals(dfg.format(new Date()))) {

		} else {
			((LinearLayout) convertView.findViewById(R.id.rlDrawlistmenu)).setBackgroundColor(Color.parseColor("#EEEEEE"));

			SimpleDateFormat df = new SimpleDateFormat("EEEE, dd MMMM yyyy");
		    Calendar c = Calendar.getInstance();
	        try {
				c.setTime(dfg.parse(modelNavDrawer.get_date()));
			} catch (java.text.ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
			String formattedDate = df.format(c.getTime()).toString();
			((TextView) convertView.findViewById(R.id.tv_Task)).setText(formattedDate);
//			((TextView) convertView.findViewById(R.id.tv_Task)).setVisibility(View.GONE);
		}
		tv_Oulite_Id.setText("Store ID : " + modelNavDrawer.get_store_id());
		tv_Store_Name.setText("Store Name : " + modelNavDrawer.get_store_name());
		tv_Address.setText("Address : " + modelNavDrawer.get_address());

		return convertView;
	}

}