package com.andtechnology.indahjaya.adapter;

import java.util.ArrayList;
import java.util.List;

import com.andtechnology.indahjaya.R;
import com.andtechnology.indahjaya.model.MarketSurveyModel;
import com.andtechnology.indahjaya.model.ModelStore;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MarketSurveyListAdapter extends ArrayAdapter<MarketSurveyModel> {
	ArrayList<MarketSurveyModel> market_survey_list;

	public MarketSurveyListAdapter(Context context, int textViewResourceId,
			ArrayList<MarketSurveyModel> market_survey_list) {
		super(context, textViewResourceId, market_survey_list);
		// TODO Auto-generated constructor stub
		this.market_survey_list = market_survey_list;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return market_survey_list.size();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView==null)
			convertView =LayoutInflater.from(getContext()).inflate(R.layout.layout_market_survey_list_child, null);
		
		final MarketSurveyModel model =market_survey_list.get(position);
		((TextView)convertView.findViewById(R.id.tvImportir)).setText(model.getImportir());
		((TextView)convertView.findViewById(R.id.tvProductName)).setText(model.getProduct_name());

		String status;
		if(model.getStatus().equals("2")) {
			status = "Published";		
		} else {	
			status = "Draft";
		}
		((TextView)convertView.findViewById(R.id.tvStatus)).setText(status);
		return convertView;
	}
}
