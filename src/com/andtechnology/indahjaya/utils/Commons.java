package com.andtechnology.indahjaya.utils;

import android.content.Intent;

public class Commons {

	public final static int FROM_CAMERA_ONE = 1;
	public final static int FROM_CAMERA_TWO = 2;
	public final static int FROM_CAMERA_THREE = 3;
	public final static int FROM_CAMERA_FOUR = 4;
	public final static int FROM_CAMERA_FIVE = 5;

	public final static int RESULT_OK_FRAGMENT = -1;
	public final static String ProductionHttp = "http://110.93.14.85:8080/indahjaya/index.php";
	
	public final static String HttpAssetOutletRegistration = "http://110.93.14.85:8080/indahjaya/asset/outlet_recommendation_photo/";
	public final static String HttpAssetActivityPhoto = "http://110.93.14.85:8080/indahjaya/asset/activity_photo/";
	public final static String HttpAssetPromo = "http://110.93.14.85:8080/indahjaya/asset/promo_photo/";
	public final static String HttpAssetMarketSurvey = "http://110.93.14.85:8080/indahjaya/asset/market_survey_photo/";

	public static Intent iService = null;
	public static int POST_WITH_JSON = 1;
	public final static int SUCCESS = 1;
	public final static int ERROR = 0;
	public final static int FROM_STORE_REGISTRATION = 908;
	public final static int FROM_CATEGORY_LIST = 909;
	public final static int FROM_ADD_JOBS = 910;
	public final static int ADD_QUESTION = 910;
	public final static int FROM_MARKET_SURVEY_LIST = 911;


	

}
