package com.andtechnology.indahjaya.imagemanager;
 
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import com.andtechnology.indahjaya.restful.ConstantPATH;

import android.graphics.Bitmap;
import android.os.Environment;
  
public class UtilsImages {
	private static final String IMAGE_DIRECTORY_NAME = "Product_Image";
    public static void CopyStream(InputStream is, OutputStream os)
    {
        final int buffer_size=1024;
        try
        {
            byte[] bytes=new byte[buffer_size];
            for(;;)
            {
              int count=is.read(bytes, 0, buffer_size);
              if(count==-1)
                  break;
              os.write(bytes, 0, count);
            }
        }
        catch(Exception ex){}
    }


	public void createDirectoryAndSaveFile(final Bitmap imageToSave, final String fileName) {
	    File direct = new File(ConstantPATH.PATH2);
	    OutputStream outStream = null;
	    if (!direct.exists()) {
	    	
	        File wallpaperDirectory = new File(ConstantPATH.PATH2);
	        wallpaperDirectory.mkdirs();
	    }

	    File file = new File(new File(ConstantPATH.PATH2), fileName);
	    if (file.exists()) {
	        file.delete();
	    }
	    
	    try {	    	
	    	outStream = new FileOutputStream(file);
	        imageToSave.compress(Bitmap.CompressFormat.PNG, 100, outStream);
	        outStream.flush();
	        outStream.close();
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
		
		
	}


	public void createDirectoryAndSaveFile2(final Bitmap imageToSave, final String fileName) {
	    File direct = new File(ConstantPATH.PATH1);
	    OutputStream outStream = null;
	    if (!direct.exists()) {
	    	
	        File wallpaperDirectory = new File(ConstantPATH.PATH1);
	        wallpaperDirectory.mkdirs();
	    }

	    File file = new File(new File(ConstantPATH.PATH1), fileName);
	    if (file.exists()) {
	        file.delete();
	    }
	    
	    try {	    	
	    	outStream = new FileOutputStream(file);
	        imageToSave.compress(Bitmap.CompressFormat.PNG, 20, outStream);
	        outStream.flush();
	        outStream.close();
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
		
		
	}
}