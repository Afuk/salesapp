package com.andtechnology.indahjaya.imagemanager;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.andtechnology.indahjaya.R;

public class ImageLoader {

	private MemoryCache memoryCache = new MemoryCache();
	private FileCache fileCache;
	private Bitmap.Config imageConfig;
	private int reqSize;
	private Map<ImageView, String> imageViews = Collections
			.synchronizedMap(new WeakHashMap<ImageView, String>());
	private ExecutorService executorService;
	private Context context;

	public ImageLoader(Context context) {
		fileCache = new FileCache(context);
		executorService = Executors.newFixedThreadPool(5);
		this.context = context;
	}

	final int stub_id = R.drawable.ic_notif;

	public void DisplayImage(String url, ImageView imageView,
			Bitmap.Config imageConfig, int type) {
		imageView.setImageDrawable(context.getResources().getDrawable(
				R.drawable.ic_notif));
		imageView.setEnabled(false);
		imageViews.put(imageView, url);
		WeakReference<Bitmap> bitmap = new WeakReference<Bitmap>(
				memoryCache.get(url));
		this.imageConfig = imageConfig;
		this.reqSize = imageView.getMeasuredWidth();
		if (bitmap.get() != null) {
			imageView.setImageBitmap(bitmap.get());
			imageView.setEnabled(true);
		} else {
			queuePhoto(url, imageView, type);
		}
		imageView.setVisibility(View.VISIBLE);
	}

	private void queuePhoto(String url, ImageView imageView, int type) {
		PhotoToLoad p = new PhotoToLoad(url, imageView);
		executorService.submit(new PhotosLoader(p, type));
	}

	public Bitmap getBitmap(String url) {
		File f = fileCache.getFile(url);

		// from SD cache
		Bitmap b = decodeFile(f);
		if (b != null)
			return b;

		// from web
		try {
			Bitmap bitmap = null;
			URL imageUrl = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) imageUrl
					.openConnection();
			conn.setConnectTimeout(30000);
			conn.setReadTimeout(30000);
			conn.setInstanceFollowRedirects(true);
			InputStream is = conn.getInputStream();
			OutputStream os = new FileOutputStream(f);
			UtilsImages.CopyStream(is, os);
			os.close();
			// bitmap = decodeFile(f);
			return bitmap;
		} catch (Exception ex) {
			ex.printStackTrace();
			//Log.i("TAG", "url error, bitmap not found");
			return null;
		}
	}

	public Bitmap getBitmapNoResize(String url) {
		File f = fileCache.getFile(url);
		Bitmap bitmap = null;

		try {
			URL imageUrl = new URL(url);
			HttpURLConnection conn;
			conn = (HttpURLConnection) imageUrl.openConnection();
			conn.setConnectTimeout(30000);
			conn.setReadTimeout(30000);
			conn.setInstanceFollowRedirects(true);
			InputStream is = conn.getInputStream();
			OutputStream os = new FileOutputStream(f);
			UtilsImages.CopyStream(is, os);
			os.close();
			return bitmap;
		} catch (IOException e) {

			e.printStackTrace();
			return null;
		}

	}

	// decodes image and scales it to reduce memory consumption
	private Bitmap decodeFile(File f) {
		try {
			// decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(f), null, o);

			// Find the correct scale value. It should be the power of 2.
			final int REQUIRED_SIZE = 40;
			int width_tmp = o.outWidth, height_tmp = o.outHeight;
			int scale = 1;
			while (true) {
				if (width_tmp / 2 < REQUIRED_SIZE
						|| height_tmp / 2 < REQUIRED_SIZE)
					break;
				width_tmp /= 2;
				height_tmp /= 2;
				scale *= 2;
			}

			// decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
		} catch (FileNotFoundException e) {
		}
		return null;
	}

	public Bitmap getBitmap2(String url) {
		
		File f = fileCache.getFile(url);

		// from SD cache
		Bitmap b = decodeFile2(f);
		if (b != null)
			return b;

		// from web
		try {
			Bitmap bitmap = null;
			URL imageUrl = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) imageUrl
					.openConnection();
			conn.setConnectTimeout(30000);
			conn.setReadTimeout(30000);
			conn.setInstanceFollowRedirects(true);
			InputStream is = conn.getInputStream();
			OutputStream os = new FileOutputStream(f);
			UtilsImages.CopyStream(is, os);
			os.close();
			bitmap = decodeFile2(f);
			return bitmap;
		} catch (Exception ex) {
			ex.printStackTrace();
			//Log.i("TAG", "url error, bitmap not found");
			return null;
		}
	}

	public Bitmap getBitmapWithTry(String url) throws IOException {
		File f = fileCache.getFile(url);

		// from SD cache
		Bitmap b = decodeFile2(f);
		if (b != null)
			return b;

		// from web
		Bitmap bitmap = null;
		URL imageUrl = new URL(url);
		HttpURLConnection conn = (HttpURLConnection) imageUrl.openConnection();
		conn.setConnectTimeout(30000);
		conn.setReadTimeout(30000);
		conn.setInstanceFollowRedirects(true);
		InputStream is = conn.getInputStream();
		OutputStream os = new FileOutputStream(f);
		UtilsImages.CopyStream(is, os);
		os.close();
		bitmap = decodeFile2(f);
		return bitmap;
	}

	private Bitmap getBitmapFromUri2(Uri uri) throws FileNotFoundException,
			IOException {
		int REQUIRED_SIZE = reqSize;
		// File file = new File(uri.toString());
		// FileInputStream input = new FileInputStream(file);
		InputStream input = context.getContentResolver().openInputStream(uri);

		BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
		onlyBoundsOptions.inJustDecodeBounds = true;
		onlyBoundsOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;
		BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
		if ((onlyBoundsOptions.outWidth == -1)
				|| (onlyBoundsOptions.outHeight == -1))
			return null;

		int scale = 1;
		if (onlyBoundsOptions.outHeight > REQUIRED_SIZE
				|| onlyBoundsOptions.outWidth > REQUIRED_SIZE) {
			scale = (int) Math.pow(
					2,
					(int) Math.round(Math.log(REQUIRED_SIZE
							/ (double) Math.max(onlyBoundsOptions.outHeight,
									onlyBoundsOptions.outWidth))
							/ Math.log(0.5)));
		}

		BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
		bitmapOptions.inSampleSize = scale;
		bitmapOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;
		input = context.getContentResolver().openInputStream(uri);
		Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);
		input.close();
		return bitmap;
	}

	// decodes image and scales it to reduce memory consumption
	private Bitmap decodeFile2(File f) {
		try {
			// Decode image size
			int REQUIRED_SIZE = reqSize;
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			o.inPurgeable = true;
			o.inPreferredConfig = imageConfig;

			FileInputStream fis = new FileInputStream(f);
			BitmapFactory.decodeStream(fis, null, o);
			try {
				fis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

			int scale = 1;
//			if (o.outHeight > REQUIRED_SIZE || o.outWidth > REQUIRED_SIZE) {
//				scale = (int) Math.pow(
//						2,
//						(int) Math.round(Math.log(REQUIRED_SIZE
//								/ (double) Math.max(o.outHeight, o.outWidth))
//								/ Math.log(0.5)));
//			}

			// decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			o2.inPurgeable = true;
			o2.inPreferredConfig = imageConfig;
			WeakReference<Bitmap> bitmap = new WeakReference<Bitmap>(
					BitmapFactory
							.decodeStream(new FileInputStream(f), null, o2));
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			bitmap.get().compress(Bitmap.CompressFormat.PNG, 100, out);
			bitmap.get().recycle();
			bitmap = null;
			WeakReference<Bitmap> bitmap2 = new WeakReference<Bitmap>(
					BitmapFactory.decodeByteArray(out.toByteArray(), 0,
							out.toByteArray().length));
			return bitmap2.get();
		} catch (FileNotFoundException e) {
		}
		return null;
	}

	// Task for the queue
	private class PhotoToLoad {
		public String url;
		public ImageView imageView;

		public PhotoToLoad(String u, ImageView i) {
			url = u;
			imageView = i;
		}
	}

	class PhotosLoader implements Runnable {
		PhotoToLoad photoToLoad;
		int type;

		PhotosLoader(PhotoToLoad photoToLoad, int type) {
			this.photoToLoad = photoToLoad;
			this.type = type;
		}

		@Override
		public void run() {
			if (imageViewReused(photoToLoad))
				return;

			Bitmap bmp = null;
			if (!photoToLoad.url.contains("file://")) {
				try {
					bmp = getBitmapWithTry(photoToLoad.url);
				} catch (IOException e) {
					bmp = BitmapFactory.decodeResource(context.getResources(),
							R.drawable.ic_notif);
					e.printStackTrace();
				}
			} else {
				try {
					bmp = getBitmapFromUri2(Uri.parse(photoToLoad.url));
				} catch (FileNotFoundException e) {
					bmp = BitmapFactory.decodeResource(context.getResources(),
							R.drawable.ic_notif);
					e.printStackTrace();
				} catch (IOException e) {
					bmp = BitmapFactory.decodeResource(context.getResources(),
							R.drawable.ic_notif);
					e.printStackTrace();
				}
			}
			memoryCache.put(photoToLoad.url, bmp);
			if (imageViewReused(photoToLoad))
				return;
			BitmapDisplayer bd = new BitmapDisplayer(bmp, photoToLoad, type);
			Activity a = (Activity) photoToLoad.imageView.getContext();
			a.runOnUiThread(bd);
		}
	}

	boolean imageViewReused(PhotoToLoad photoToLoad) {
		String tag = imageViews.get(photoToLoad.imageView);
		if (tag == null || !tag.equals(photoToLoad.url))
			return true;
		return false;
	}

	// Used to display bitmap in the UI thread
	class BitmapDisplayer implements Runnable {
		Bitmap bitmap;
		PhotoToLoad photoToLoad;
		int type;

		public BitmapDisplayer(Bitmap b, PhotoToLoad p, int type) {
			bitmap = b;
			photoToLoad = p;
			this.type = type;
		}

		@Override
		public void run() {
			if (imageViewReused(photoToLoad))
				return;
			if (bitmap != null) {
				photoToLoad.imageView.setImageBitmap(bitmap);
				photoToLoad.imageView.setEnabled(true);
			} else {
				switch (type) {

				default:
					break;
				}
			}

		}
	}

	public void clearCache() {
		memoryCache.clear();
		fileCache.clear();
	}

}
