package com.andtechnology.indahjaya.view.framework;

import java.util.HashMap;

import android.app.Activity;
import android.os.Bundle;

import com.andtechnology.indahjaya.lib.util.SessionManagement;
import com.andtechnology.indahjaya.lib.util.Utils;
import com.andtechnology.indahjaya.lib.util.UtilsPointer;

public class BaseActivity extends Activity {

	protected Activity activity;
	protected HashMap<String, String> user_session;
	

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeSystem();
        initializeGUI();
    }
    
    private void initializeSystem(){
    	activity = this;
		SessionManagement session = new SessionManagement(activity);
		user_session = session.getUserDetails();
		if(user_session.get(session.USER_ID) == null) {
			UtilsPointer.goToLogin(activity);
		} else {
			UtilsPointer.goToMain(activity);
		}
	}
	
	private void initializeGUI(){
		
	}
}