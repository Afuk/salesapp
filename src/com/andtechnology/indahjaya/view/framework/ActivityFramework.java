package com.andtechnology.indahjaya.view.framework;

import java.util.HashMap;

import android.app.Activity;
import android.os.Bundle;

import com.andtechnology.indahjaya.lib.util.SessionManagement;
import com.andtechnology.indahjaya.lib.util.UtilsPointer;

public class ActivityFramework extends Activity {

	protected Activity activity;
	//protected HashMap<String, String> user_session;
	protected SessionManagement session;
	protected String user_id;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeSystem();
        initializeGUI();
    }
    
    private void initializeSystem(){
    	activity = this;
		session = new SessionManagement(activity);
		HashMap<String, String> user_session = session.getUserDetails();
		if(user_session.get(session.USER_ID) == null) {
			UtilsPointer.goToLogin(activity);
		}
		user_id=user_session.get(session.USER_ID);
	}
	
	private void initializeGUI(){
		
	}
}