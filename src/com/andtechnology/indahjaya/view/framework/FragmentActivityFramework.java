package com.andtechnology.indahjaya.view.framework;

import java.util.HashMap;

import com.andtechnology.indahjaya.lib.util.SessionManagement;
import com.andtechnology.indahjaya.lib.util.UtilsPointer;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class FragmentActivityFramework extends FragmentActivity {

	protected Activity activity;
	protected HashMap<String, String> user_session;
	

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeSystem();
        initializeGUI();
    }
    
    private void initializeSystem(){
    	activity = this;
		SessionManagement session = new SessionManagement(activity);
		HashMap<String, String> user_session = session.getUserDetails();
		if(user_session.get(session.USER_ID).equals(null)) {
			UtilsPointer.goToLogin(activity);
		}
	}
	
	private void initializeGUI(){
		
	}

	@Override
	public void onBackPressed() {
        setResult(RESULT_OK);
        finish();
	}
}