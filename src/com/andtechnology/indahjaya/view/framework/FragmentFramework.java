package com.andtechnology.indahjaya.view.framework;

import java.util.HashMap;

import com.andtechnology.indahjaya.lib.util.SessionManagement;
import com.andtechnology.indahjaya.lib.util.UtilsPointer;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

public class FragmentFramework extends Fragment {

	protected Activity activity;
	protected View view;
	protected HashMap<String, String> user_session;
	protected String user_id;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initializeSystem();
	}

	private void initializeSystem() {
		activity = getActivity();
		SessionManagement session = new SessionManagement(activity);
		HashMap<String, String> user_session = session.getUserDetails();
		if(user_session.get(session.USER_ID) == null) {
			UtilsPointer.goToLogin(activity);
		}
		user_id=user_session.get(session.USER_ID);

	}

	
}