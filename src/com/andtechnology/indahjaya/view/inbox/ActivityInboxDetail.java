package com.andtechnology.indahjaya.view.inbox;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.andtechnology.indahjaya.database.DatabaseHandler;
import com.andtechnology.indahjaya.lib.util.JSONParser;
import com.andtechnology.indahjaya.lib.util.SessionManagement;
import com.andtechnology.indahjaya.lib.util.Utils;
import com.andtechnology.indahjaya.restful.ConstantREST;
import com.andtechnology.indahjaya.view.framework.FragmentActivityFramework;
import com.andtechnology.indahjaya.R;

public class ActivityInboxDetail extends FragmentActivityFramework{
	private static String url = ConstantREST.API4;
	
	ProgressDialog pDialog;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_inbox_detail);

		Intent i = getIntent();
		if(i.getStringExtra("getFlag").equals("0")){
			DatabaseHandler db = new DatabaseHandler(getApplicationContext());
			if (Utils.isOnline(activity)) {
				db.updatemessage(i.getIntExtra("getId",0));
				new UpdateMessage().execute(String.valueOf(i.getIntExtra("getId",0)));
			}
		}
		
		TextView title_text = (TextView) findViewById(R.id.title_text);
		TextView tv_date = (TextView) findViewById(R.id.tv_date);
		TextView tv_subject = (TextView) findViewById(R.id.tv_subject);
		TextView tv_from = (TextView) findViewById(R.id.tv_from);
		TextView tv_description = (TextView) findViewById(R.id.tv_description);
		tv_date.setText(i.getStringExtra("getDate"));
		tv_subject.setText(i.getStringExtra("getSubject"));
		tv_from.setText(i.getStringExtra("getFrom"));
		tv_description.setText(Html.fromHtml(i.getStringExtra("getDescription")));

		ImageView iv_back = (ImageView) findViewById(R.id.iv_back);
		
		iv_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
		        setResult(RESULT_OK);
				finish();
			}
		});
	}
	


	/**
	 * Async task class to get json by making HTTP call
	 * */
	private class UpdateMessage extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog
			pDialog = new ProgressDialog(activity);
			pDialog.setMessage("Please wait...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			// Creating service handler class instance
			SessionManagement session = new SessionManagement(activity);
			HashMap<String, String> user = session.getUserDetails();

			List<NameValuePair> pairs = new ArrayList<NameValuePair>();
			pairs.add(new BasicNameValuePair("message_id", params[0]));

			JSONParser jp = new JSONParser();
			return jp.getJSONFromUrl(url, pairs);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			JSONObject obj = null;
			
			try {
				obj = new JSONObject(result);
				
				pDialog.dismiss();

			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}
	}
}
