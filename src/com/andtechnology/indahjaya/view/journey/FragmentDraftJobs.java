package com.andtechnology.indahjaya.view.journey;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.andtechnology.indahjaya.R;
import com.andtechnology.indahjaya.adapter.ListFragmentActiveJobsAdapter;
import com.andtechnology.indahjaya.database.DatabaseHandler;
import com.andtechnology.indahjaya.lib.util.SessionManagement;
import com.andtechnology.indahjaya.model.MappingStoreModel;
import com.andtechnology.indahjaya.model.NavDrawerMenuModel;
import com.andtechnology.indahjaya.view.framework.FragmentFramework;
import com.andtechnology.indahjaya.view.journey.activejobs.ActivityActiveJobs;
import com.andtechnology.indahjaya.view.journey.activejobs.ActivityActiveJobsView;

public class FragmentDraftJobs extends FragmentFramework {
	DatabaseHandler db;
	ArrayList<MappingStoreModel> navDrawerMenu;
	ListView mDrawerList;
	Calendar c;
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_draft_jobs,
				container, false);


		c = Calendar.getInstance();

		SimpleDateFormat df = new SimpleDateFormat("EEEE, dd MMMM yyyy");
		final String formattedDate = df.format(new Date());
		TextView tv_today = (TextView) rootView.findViewById(R.id.tv_today);
		tv_today.setText(formattedDate);

		db = new DatabaseHandler(rootView.getContext());


		navDrawerMenu = new ArrayList<MappingStoreModel>();
		mDrawerList = (ListView) rootView.findViewById(R.id.lv_store);

		mDrawerList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				NavDrawerMenuModel model = (NavDrawerMenuModel) parent
						.getAdapter().getItem(position);
				Intent i = null;
				if(model.getFlag().equals("1")) {
					i = new Intent(v.getContext(), ActivityActiveJobs.class);
				} else {
					i = new Intent(v.getContext(), ActivityActiveJobsView.class);	
				}
				i.putExtra("getTitle", model.getTitle());
				i.putExtra("getId", model.getIds());
				i.putExtra("getFinalId", model.getId());
				i.putExtra("getDate", formattedDate);
				startActivityForResult(i, 901);
			}
		});
		
		
		return rootView;
	}
	
	public void LoadList() {

		NavDrawerMenuModel model;
		SessionManagement session = new SessionManagement(activity);
		HashMap<String, String> user = session.getUserDetails();

		SimpleDateFormat dfg = new SimpleDateFormat("yyyy-MM-dd");
		List<MappingStoreModel> mappingstoremodel = db.getAllMappingStore(1, user.get(session.USER_ID),dfg.format(new Date()));
		
		mappingstoremodel.addAll(db.getAllMappingStore(2, user.get(session.USER_ID),dfg.format(new Date())));
	//	List<MappingStoreModel> mappingstoremodel2 = ;
		if(mappingstoremodel.size() == 0) {
            activity.findViewById(R.id.EmptyIndicator).setVisibility(View.VISIBLE);
        } else {
            activity.findViewById(R.id.EmptyIndicator).setVisibility(View.INVISIBLE);            
        }

//		for (MappingStoreModel cn : mappingstoremodel) {
//			model = new NavDrawerMenuModel();
//			model.setTitle(cn.get_store_name());
//			model.setIds(cn.get_store_id());
//			model.setId(cn.get_mapping_store_id());
//			model.setAddress(cn.get_address());
//			model.setFlag("1");
//			navDrawerMenu.add(model);
//		}
//
//		for (MappingStoreModel cn : mappingstoremodel2) {
//			model = new NavDrawerMenuModel();
//			model.setTitle(cn.get_store_name());
//			model.setIds(cn.get_store_id());
//			model.setId(cn.get_mapping_store_id());
//			model.setAddress(cn.get_address());
//			navDrawerMenu.add(model);
//			model.setFlag("2");
//		}

		// setting the nav drawer list adapter
		ListFragmentActiveJobsAdapter adapter = new ListFragmentActiveJobsAdapter(activity, (ArrayList<MappingStoreModel>)mappingstoremodel);
		mDrawerList.setAdapter(adapter);
		
	}

	
	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		navDrawerMenu.removeAll(navDrawerMenu);
		LoadList();
	}
	
	
}
