package com.andtechnology.indahjaya.view.journey;

import java.io.File;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.andtechnology.indahjaya.R;
import com.andtechnology.indahjaya.adapter.ListActivitiesAddJobs;
import com.andtechnology.indahjaya.database.DatabaseHandler;
import com.andtechnology.indahjaya.lib.util.SessionManagement;
import com.andtechnology.indahjaya.lib.util.Utils;
import com.andtechnology.indahjaya.model.CategoryProductModel;
import com.andtechnology.indahjaya.model.LastUpdateModel;
import com.andtechnology.indahjaya.model.MappingSalesToCategoryProductModel;
import com.andtechnology.indahjaya.model.MappingSalesToStoreModel;
import com.andtechnology.indahjaya.model.MappingSalesToStoreTargetModel;
import com.andtechnology.indahjaya.model.MappingStoreModel;
import com.andtechnology.indahjaya.model.MessageModel;
import com.andtechnology.indahjaya.model.NavDrawerMenuModel;
import com.andtechnology.indahjaya.model.PromoModel;
import com.andtechnology.indahjaya.model.SubCategoryProductModel;
import com.andtechnology.indahjaya.model.TypeActivitiesAnswerModel;
import com.andtechnology.indahjaya.model.TypeActivitiesModel;
import com.andtechnology.indahjaya.model.TypeActivitiesQuestionModel;
import com.andtechnology.indahjaya.restful.ConstantPATH;
import com.andtechnology.indahjaya.ui.service.APIParam;
import com.andtechnology.indahjaya.ui.service.IndahJayaService;
import com.andtechnology.indahjaya.utils.Commons;
import com.andtechnology.indahjaya.view.framework.ActivityFramework;

public class ActivityAddJobs extends ActivityFramework {
	android.support.v4.app.Fragment fragment = null;

	// Try new System API
	ProgressDialog loading;
	private HashMap<String, String> parameters;
	private int apiIndex;
	private boolean has_service = false;
	private Messenger mService = null;
	private Messenger mMessenger = new Messenger(new ActivityAddJobsHandler(
			this));
	ServiceConnection scConnection = new ServiceConnection() {

		@Override
		public void onServiceDisconnected(ComponentName componentName) {
			mService = null;
		}

		@Override
		public void onServiceConnected(ComponentName componentName,
				IBinder binder) {
			mService = new Messenger(binder);
			doAPI(apiIndex);
		}
	};

	public void initialService() {
		if (Commons.iService == null) {
			Commons.iService = new Intent(getApplicationContext(),
					IndahJayaService.class);
			startService(Commons.iService);

		}
	}

	public void doAddJobs(String store_id) {
		if (Utils.isOnline(getApplicationContext())) {
			loading.setMessage("Please wait...");
			loading.setCancelable(false);
			loading.show();
			parameters = new HashMap<String, String>();
			apiIndex = APIParam.API_006;
			parameters.put("user_sales_id", user_id);
			parameters.put("store_id", store_id);
			initializeBindService(apiIndex);
		} else {
			Utils.showToast(activity, "Aplikasi ini membutuhkan koneksi internet");
		}
	}

	public void doSync() {

		parameters = new HashMap<String, String>();
		apiIndex = APIParam.API_010;
		DatabaseHandler db = new DatabaseHandler(getApplicationContext());

		SessionManagement session = new SessionManagement(activity);
		HashMap<String, String> user = session.getUserDetails();
		if (db.getLastUpdated() == null) {
			// db.deleteall();
			File file = new File(ConstantPATH.PATH1);
			if (!file.exists()) {
				File wallpaperDirectory = new File(ConstantPATH.PATH1);
				wallpaperDirectory.mkdirs();
			}
			String[] myFiles;

			myFiles = file.list();
			for (int i = 0; i < myFiles.length; i++) {
				File myFile = new File(file, myFiles[i]);
				myFile.delete();
			}

			File file2 = new File(ConstantPATH.PATH2);
			if (!file2.exists()) {
				File wallpaperDirectory = new File(ConstantPATH.PATH2);
				wallpaperDirectory.mkdirs();
			}
			String[] myFiles2;

			myFiles2 = file2.list();
			for (int i = 0; i < myFiles2.length; i++) {
				File myFile2 = new File(file2, myFiles2[i]);
				myFile2.delete();
			}

			parameters.put("date", "0000-00-00");
			parameters.put("user_id", user.get(session.USER_ID));
		} else {
			parameters.put("date", db.getLastUpdated());
			parameters.put("user_id", user.get(session.USER_ID));
		}

		initializeBindService(apiIndex);
	}

	private void initializeBindService(int apiIndex) {
		if (mService == null) {
			has_service = true;
			bindService(Commons.iService, scConnection,
					Context.BIND_AUTO_CREATE);
		} else {
			doAPI(apiIndex);
		}
	}

	private void doAPI(int apiIndex) {
		// progressHUD = ProgressHUD.show(getApplicationContext(), "", true,
		// false, null);

		try {
			Message msg = Message.obtain(null, apiIndex);
			Bundle bundle = new Bundle();
			bundle.putSerializable("parameters", parameters);
			bundle.putBoolean("for_httpost", true);
			bundle.putBoolean("wrong_formatJson", true);
			msg.setData(bundle);
			msg.replyTo = mMessenger;
			mService.send(msg);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	private static class ActivityAddJobsHandler extends Handler {
		WeakReference<ActivityAddJobs> screen;

		public ActivityAddJobsHandler(ActivityAddJobs myScreen) {
			screen = new WeakReference<ActivityAddJobs>(myScreen);
		}

		@Override
		public void handleMessage(Message msg) {
			Log.i("TAG", msg.toString());
			switch (msg.what) {
			case APIParam.API_006:
				screen.get().getResponseAddJobs(msg);
				break;
			case APIParam.API_010:
				screen.get().getResponseSync(msg);
				break;
			default:
				break;
			}

		}
	}

	private void getResponseAddJobs(Message msg) {
		HashMap<Object, Object> response = (HashMap<Object, Object>) msg
				.getData().getSerializable("response");
		Log.i("TAG", "result : " + response);

		Integer result = (Integer) response.get("response");
		switch (result) {
		case Commons.SUCCESS:
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					doSync();
				}
			});
			break;
		case Commons.ERROR:
			Toast.makeText(getApplicationContext(),
					"" + response.get("message"), Toast.LENGTH_SHORT).show();
			loading.dismiss();
			break;
		default:

			break;
		}
		// progressHUD.dismiss();

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_jobs);
		loading = new ProgressDialog(activity);

		Calendar c = Calendar.getInstance();

		SimpleDateFormat df = new SimpleDateFormat("EEEE, dd MMMM yyyy");
		final String formattedDate = df.format(new Date());

		TextView tv_today = (TextView) findViewById(R.id.tv_today);
		tv_today.setText("Selected Your New Jobs\n" + formattedDate);

		initialService();
		LoadData();
	}

	public void LoadData() {
		TextView title_text = (TextView) findViewById(R.id.title_text);
		title_text.setText("ADD JOBS");

		ArrayList<NavDrawerMenuModel> navDrawerMenu = new ArrayList<NavDrawerMenuModel>();

		navDrawerMenu = new ArrayList<NavDrawerMenuModel>();
		ListView mDrawerList = (ListView) findViewById(R.id.lv_store);

		NavDrawerMenuModel model;

		DatabaseHandler db = new DatabaseHandler(getApplicationContext());
		SessionManagement session = new SessionManagement(activity);
		HashMap<String, String> user = session.getUserDetails();
		List<MappingSalesToStoreModel> store = db.getAllMappingSalesToStore();
		if (store.size() == 0) {
			findViewById(R.id.EmptyIndicator).setVisibility(View.VISIBLE);
		} else {
			findViewById(R.id.EmptyIndicator).setVisibility(View.INVISIBLE);
		}

		// setting the nav drawer list adapter
		ListActivitiesAddJobs adapter = new ListActivitiesAddJobs(activity,
				(ArrayList<MappingSalesToStoreModel>) store);
		mDrawerList.setAdapter(adapter);

		mDrawerList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				final MappingSalesToStoreModel model = (MappingSalesToStoreModel) parent
						.getAdapter().getItem(position);

				AlertDialog.Builder builder = new AlertDialog.Builder(activity);
				builder.setTitle(R.string.confirmation);
				builder.setMessage("Are you sure want to add this store?");
				builder.setPositiveButton(R.string.yes,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								doAddJobs(model.get_store_id());
							}
						});
				builder.setNegativeButton(R.string.no, null);
				AlertDialog alert = builder.create();
				alert.show();
				// doSync();

			}
		});

		ImageView iv_back = (ImageView) findViewById(R.id.iv_back);
		iv_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == RESULT_OK) {
			if (requestCode == Commons.FROM_STORE_REGISTRATION)
				LoadData();
		}
	}

	private void getResponseSync(Message msg) {
		HashMap<Object, Object> response = (HashMap<Object, Object>) msg
				.getData().getSerializable("response");
		Log.i("TAG", "result : " + response);
		Integer result = (Integer) response.get("response");
		switch (result) {
		case Commons.SUCCESS:
			DatabaseHandler db = new DatabaseHandler(getApplicationContext());
			int flag = 0;

			ArrayList<HashMap<Object, Object>> mapping_store = (ArrayList<HashMap<Object, Object>>) response
					.get("mapping_store");
			SessionManagement session = new SessionManagement(activity);

			for (HashMap<Object, Object> store : mapping_store) {
				String test = (String) store.get("activities_id");

				MappingStoreModel model = new MappingStoreModel();
				model.set_mapping_store_id(Integer.parseInt((String) store
						.get("mapping_store_id")));
				model.set_user_rm_id((String) store.get("user_rm_id"));
				model.set_user_sales_id((String) store.get("user_sales_id"));
				model.set_store_id((String) store.get("store_id"));
				model.set_date((String) store.get("date"));
				model.set_user_change((String) store.get("user_change"));
				model.set_date_change((String) store.get("date_change"));
				model.set_stsrc((String) store.get("stsrc"));
				model.set_activities_id((String) store.get("activities_id"));
				model.set_region_id((String) store.get("region_id"));
				model.set_store_name((String) store.get("store_name"));
				model.set_address((String) store.get("address"));
				model.set_longtitude((String) store.get("longtitude"));
				model.set_latitude((String) store.get("latitude"));
				model.set_radius((String) store.get("radius"));
				if (test != null) {
					model.set_progress_status(2);
				} else {
					model.set_progress_status(0);
				}
				db.addMappingStore(model);

				flag = 1;

			}

			ArrayList<HashMap<Object, Object>> category_product = (ArrayList<HashMap<Object, Object>>) response
					.get("category_product");
			for (HashMap<Object, Object> category_product_result : category_product) {
				CategoryProductModel model = new CategoryProductModel();
				model.set_category_product_id(Integer
						.parseInt((String) category_product_result
								.get("category_product_id")));
				model.set_category_product_name((String) category_product_result
						.get("category_product_name"));
				model.set_user_change((String) category_product_result
						.get("user_change"));
				model.set_date_change((String) category_product_result
						.get("date_change"));
				model.set_stsrc((String) category_product_result.get("stsrc"));
				db.addCategoryProduct(model);

				flag = 1;
			}

			ArrayList<HashMap<Object, Object>> promo = (ArrayList<HashMap<Object, Object>>) response
					.get("promo");
			for (HashMap<Object, Object> promo_result : promo) {
				PromoModel model = new PromoModel();
				model.set_promo_id(Integer.parseInt((String) promo_result
						.get("promo_id")));
				model.set_region_id(Integer.parseInt((String) promo_result
						.get("region_id")));
				model.set_sub_category_product_id(Integer
						.parseInt((String) promo_result
								.get("sub_category_product_id")));
				model.set_promo_name((String) promo_result.get("promo_name"));
				model.set_start_date((String) promo_result.get("start_date"));
				model.set_end_date((String) promo_result.get("end_date"));
				model.set_description((String) promo_result.get("description"));
				model.set_images((String) promo_result.get("images"));
				model.set_user_change((String) promo_result.get("user_change"));
				model.set_date_change((String) promo_result.get("date_change"));
				model.set_stsrc((String) promo_result.get("stsrc"));
				db.addPromo(model);

				flag = 1;
			}

			ArrayList<HashMap<Object, Object>> sub_category_product = (ArrayList<HashMap<Object, Object>>) response
					.get("sub_category_product");
			for (HashMap<Object, Object> sub_category_product_result : sub_category_product) {
				SubCategoryProductModel model = new SubCategoryProductModel();
				model.set_sub_category_product_id(Integer
						.parseInt((String) sub_category_product_result
								.get("sub_category_product_id")));
				model.set_category_product_id(Integer
						.parseInt((String) sub_category_product_result
								.get("category_product_id")));
				model.set_sub_category_product_name((String) sub_category_product_result
						.get("sub_category_product_name"));
				model.set_description((String) sub_category_product_result
						.get("description"));
				model.set_images((String) sub_category_product_result
						.get("images"));
				model.set_user_change((String) sub_category_product_result
						.get("user_change"));
				model.set_date_change((String) sub_category_product_result
						.get("date_change"));
				model.set_stsrc((String) sub_category_product_result
						.get("stsrc"));
				db.addSubCategoryProduct(model);

				flag = 1;
			}

			ArrayList<HashMap<Object, Object>> type_activities = (ArrayList<HashMap<Object, Object>>) response
					.get("type_activities");
			for (HashMap<Object, Object> type_activities_result : type_activities) {
				TypeActivitiesModel model = new TypeActivitiesModel();
				model.set_type_activities_id(Integer
						.parseInt((String) type_activities_result
								.get("type_activities_id")));
				model.set_type_activities_name((String) type_activities_result
						.get("type_activities_name"));
				model.set_user_change((String) type_activities_result
						.get("user_change"));
				model.set_date_change((String) type_activities_result
						.get("date_change"));
				model.set_stsrc((String) type_activities_result.get("stsrc"));
				db.addTypeActivities(model);

				flag = 1;
			}

			ArrayList<HashMap<Object, Object>> type_activities_question = (ArrayList<HashMap<Object, Object>>) response
					.get("type_activities_question");
			for (HashMap<Object, Object> type_activities_question_result : type_activities_question) {
				TypeActivitiesQuestionModel model = new TypeActivitiesQuestionModel();
				model.set_type_activities_question_id(Integer
						.parseInt((String) type_activities_question_result
								.get("type_activities_question_id")));
				model.setType_activities_id(Integer
						.parseInt((String) type_activities_question_result
								.get("type_activities_id")));
				model.set_type_activities_question_name((String) type_activities_question_result
						.get("type_activities_question_name"));
				model.set_user_change((String) type_activities_question_result
						.get("user_change"));
				model.set_date_change((String) type_activities_question_result
						.get("date_change"));
				model.set_stsrc((String) type_activities_question_result
						.get("stsrc"));
				db.addTypeActivitiesQuestion(model);

				flag = 1;
			}

			ArrayList<HashMap<Object, Object>> type_activities_answer = (ArrayList<HashMap<Object, Object>>) response
					.get("type_activities_answer");
			for (HashMap<Object, Object> type_activities_answer_result : type_activities_answer) {
				TypeActivitiesAnswerModel model = new TypeActivitiesAnswerModel();
				model.set_type_activities_answer_id(Integer
						.parseInt((String) type_activities_answer_result
								.get("type_activities_answer_id")));
				model.set_type_activities_question_id(Integer
						.parseInt((String) type_activities_answer_result
								.get("type_activities_question_id")));
				model.set_type_activities_answer_name((String) type_activities_answer_result
						.get("type_activities_answer_name"));
				model.set_user_change((String) type_activities_answer_result
						.get("user_change"));
				model.set_date_change((String) type_activities_answer_result
						.get("date_change"));
				model.set_stsrc((String) type_activities_answer_result
						.get("stsrc"));
				db.addTypeActivitiesAnswer(model);

				flag = 1;
			}

			// TODO bikin db mapping sales to category product
			ArrayList<HashMap<Object, Object>> mapping_sales_to_category_product = (ArrayList<HashMap<Object, Object>>) response
					.get("mapping_sales_to_category_product");
			for (HashMap<Object, Object> mapping_sales_to_category_product_result : mapping_sales_to_category_product) {
				MappingSalesToCategoryProductModel model = new MappingSalesToCategoryProductModel();
				model.set_mapping_sales_to_category_product_id(Integer
						.parseInt((String) mapping_sales_to_category_product_result
								.get("mapping_sales_to_category_product_id")));
				model.set_user_sales_id((String) mapping_sales_to_category_product_result
						.get("user_sales_id"));
				model.set_category_product_id((String) mapping_sales_to_category_product_result
						.get("category_product_id"));
				model.set_user_change((String) mapping_sales_to_category_product_result
						.get("user_change"));
				model.set_date_change((String) mapping_sales_to_category_product_result
						.get("date_change"));
				model.set_stsrc((String) mapping_sales_to_category_product_result
						.get("stsrc"));
				db.addMappingSalesToCategoryProduct(model);

				flag = 1;
			}

			ArrayList<HashMap<Object, Object>> mapping_sales_to_store = (ArrayList<HashMap<Object, Object>>) response
					.get("mapping_sales_to_store");
			for (HashMap<Object, Object> mapping_sales_to_store_result : mapping_sales_to_store) {
				MappingSalesToStoreModel model = new MappingSalesToStoreModel();
				model.set_mapping_sales_to_store_id(Integer
						.parseInt((String) mapping_sales_to_store_result
								.get("mapping_sales_to_store_id")));
				model.set_user_sales_id((String) mapping_sales_to_store_result
						.get("user_sales_id"));
				model.set_store_id((String) mapping_sales_to_store_result
						.get("store_id"));
				model.set_user_change((String) mapping_sales_to_store_result
						.get("user_change"));
				model.set_date_change((String) mapping_sales_to_store_result
						.get("date_change"));
				model.set_stsrc((String) mapping_sales_to_store_result
						.get("stsrc"));
				model.set_store_name((String) mapping_sales_to_store_result
						.get("store_name"));
				model.set_address((String) mapping_sales_to_store_result
						.get("address"));
				db.addMappingSalesToStore(model);

				flag = 1;
			}

			ArrayList<HashMap<Object, Object>> mapping_sales_to_store_target = (ArrayList<HashMap<Object, Object>>) response
					.get("mapping_sales_to_store_target");
			for (HashMap<Object, Object> mapping_sales_to_store_target_result : mapping_sales_to_store_target) {
				MappingSalesToStoreTargetModel model = new MappingSalesToStoreTargetModel();
				model.set_mapping_sales_to_store_target_id(Integer
						.parseInt((String) mapping_sales_to_store_target_result
								.get("mapping_sales_to_store_target_id")));
				model.set_user_sales_id((String) mapping_sales_to_store_target_result
						.get("user_sales_id"));
				model.set_period((String) mapping_sales_to_store_target_result
						.get("period"));
				model.set_target((String) mapping_sales_to_store_target_result
						.get("target"));
				model.set_user_change((String) mapping_sales_to_store_target_result
						.get("user_change"));
				model.set_date_change((String) mapping_sales_to_store_target_result
						.get("date_change"));
				model.set_stsrc((String) mapping_sales_to_store_target_result
						.get("stsrc"));
				db.addMappingSalesToStoreTarget(model);

				flag = 1;
			}

			ArrayList<HashMap<Object, Object>> message = (ArrayList<HashMap<Object, Object>>) response
					.get("message");
			for (HashMap<Object, Object> message_result : message) {
				MessageModel model = new MessageModel();

				model.set_message_id(Integer.parseInt((String) message_result
						.get("message_id")));
				model.set_super_user_id((String) message_result
						.get("super_user_id"));
				model.set_super_user_name((String) message_result
						.get("super_user_name"));
				model.set_user_id((String) message_result.get("user_id"));
				model.set_user_name((String) message_result.get("user_name"));
				model.set_subject((String) message_result.get("subject"));
				model.set_body((String) message_result.get("body"));
				model.set_status((String) message_result.get("status"));
				model.set_date((String) message_result.get("date"));
				model.set_user_change((String) message_result
						.get("user_change"));
				model.set_date_change((String) message_result
						.get("date_change"));
				model.set_stsrc((String) message_result.get("stsrc"));
				db.addMessage(model);

				flag = 1;
			}

			if (flag == 1) {

				SimpleDateFormat df = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss");
				String formattedDate = df.format(new Date());
				db.addLastUpdate(new LastUpdateModel(formattedDate));
			}

			loading.dismiss();
			finish();
			break;
		case Commons.ERROR:
			loading.dismiss();

			Toast.makeText(getApplicationContext(),
					"" + response.get("message"), Toast.LENGTH_SHORT).show();

			break;
		default:

			break;
		}
		// progressHUD.dismiss();

	}

	@Override
	protected void onDestroy() {
		if (has_service) {
			unbindService(scConnection);
			stopService(Commons.iService);
		}
		super.onDestroy();
	}

}
