package com.andtechnology.indahjaya.view.journey;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.andtechnology.indahjaya.R;
import com.andtechnology.indahjaya.adapter.ListFragmentActiveJobsAdapter;
import com.andtechnology.indahjaya.database.DatabaseHandler;
import com.andtechnology.indahjaya.lib.util.SessionManagement;
import com.andtechnology.indahjaya.model.MappingStoreModel;
import com.andtechnology.indahjaya.model.NavDrawerMenuModel;
import com.andtechnology.indahjaya.view.framework.FragmentFramework;
import com.andtechnology.indahjaya.view.journey.activejobs.ActivityActiveJobsView;

public class FragmentJobsDone extends FragmentFramework {

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_jobs_done,
				container, false);

		
		SimpleDateFormat df = new SimpleDateFormat("EEEE, dd MMMM yyyy");
//
//		TextView tv_today = (TextView) rootView.findViewById(R.id.tv_today);
//		tv_today.setText(formattedDate);

		DatabaseHandler db = new DatabaseHandler(rootView.getContext());
		SessionManagement session = new SessionManagement(activity);
		HashMap<String, String> user = session.getUserDetails();
		SimpleDateFormat dfg = new SimpleDateFormat("yyyy-MM-dd");
		List<MappingStoreModel> date = db.getAllDateMappingStore(3, user.get(session.USER_ID));
		
		 final HashMap<String, String> finaldate = new HashMap<String, String>();
        List<String> list = new ArrayList<String>();
		for (MappingStoreModel cn : date) {
		    Calendar c = Calendar.getInstance();
	        try {
				c.setTime(dfg.parse(cn.get_date()));
			} catch (java.text.ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
	        
	        finaldate.put(df.format(c.getTime()).toString(), dfg.format(c.getTime()));
	        list.add(df.format(c.getTime()).toString());
		}

		
		ArrayAdapter<String> adapters = new ArrayAdapter(activity, android.R.layout.simple_spinner_item, list);
		adapters.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		((Spinner) rootView.findViewById(R.id.sp_date)).setAdapter(adapters);
		
		final Spinner sp_date = (Spinner) rootView.findViewById(R.id.sp_date);
		final String formattedDate = String.valueOf(sp_date.getSelectedItem());


		final ListView mDrawerList = (ListView) rootView.findViewById(R.id.lv_store);
        sp_date.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
//				Log.d("data", finaldate.get(String.valueOf(sp_date.getSelectedItem())));
				NavDrawerMenuModel model;
				final ArrayList<NavDrawerMenuModel> navDrawerMenu  = new ArrayList<NavDrawerMenuModel>();

				DatabaseHandler db = new DatabaseHandler(view.getContext());
				SessionManagement session = new SessionManagement(activity);
				HashMap<String, String> user = session.getUserDetails();
				
				List<MappingStoreModel> mappingstoremodel = db.getAllMappingStore(3, user.get(session.USER_ID),finaldate.get(String.valueOf(sp_date.getSelectedItem())));
				if(mappingstoremodel.size() == 0) {
					activity.findViewById(R.id.EmptyIndicator).setVisibility(View.VISIBLE);
		        } else {
		        	activity.findViewById(R.id.EmptyIndicator).setVisibility(View.INVISIBLE);            
		        }
				ListFragmentActiveJobsAdapter adapter = new ListFragmentActiveJobsAdapter(activity, (ArrayList<MappingStoreModel>)mappingstoremodel);

				adapter.notifyDataSetChanged();
				// setting the nav drawer  list adapter
				mDrawerList.setAdapter(adapter);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
        
        });

		mDrawerList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				NavDrawerMenuModel model = (NavDrawerMenuModel) parent
						.getAdapter().getItem(position);
				Intent i = new Intent(v.getContext(), ActivityActiveJobsView.class);
				i.putExtra("getTitle", model.getTitle());
				i.putExtra("getId", model.getIds());
				i.putExtra("getFinalId", model.getId());
				i.putExtra("getDate", formattedDate);
				startActivityForResult(i, 901);
			}
		});
	    
		
		
		
		
		return rootView;
	}
}
