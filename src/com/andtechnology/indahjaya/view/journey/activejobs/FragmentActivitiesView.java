package com.andtechnology.indahjaya.view.journey.activejobs;

import java.util.ArrayList;
import java.util.HashMap;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.andtechnology.indahjaya.R;
import com.andtechnology.indahjaya.database.DatabaseHandler;
import com.andtechnology.indahjaya.view.framework.FragmentFramework;

public class FragmentActivitiesView extends FragmentFramework{
	View comment;
	DatabaseHandler db;
	ArrayList<TextView> allResultText;
	ArrayList<CheckBox> allResultCheckType1;
	ArrayList<CheckBox> allResultCheckType2;
	String finalLastId;
	String finalLastId2;
	int getFinalId;
	int finalFlag;
	String getId,getTitle;
	String UserId;

	String update_activities_id = "";
	int update_mapping_store_id = 0;
	String update_before_visit = "";
	String update_after_visit = "";
	String update_before_dummy_demo = "";
	String update_after_dummy_demo = "";
	String update_before_posm = "";
	String update_after_posm = "";
	
	String update_detail_activities_id;
	String update_type_dummy_demo_id;
	String update_type_product_id;
	HashMap<Integer, Integer> dummy_demo;
	
	private Uri fileUri = null;
	String finalImageVisitBefore, finalImageVisitAfter, finalImageDummyBefore, finalImageDummyAfter, finalImagePOSMBefore, finalImagePOSMAfter;
	/** Capturing Camera Image will lauch camera app requrest image capture **/

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_activities_view,
				container, false);
		return rootView;
	}
	
//	public void LoadView(View rootView){		
//		final Bundle data = getArguments();
//		TextView tv_Oulite_Id = (TextView) rootView.findViewById(R.id.tv_Oulite_Id);
//		TextView tv_Store_Name = (TextView) rootView.findViewById(R.id.tv_Store_Name);
//		tv_Oulite_Id.setText("Store ID : " + data.getString("getId"));
//		tv_Store_Name.setText("Store Name : " + data.getString("getTitle"));
//		
//		getId = data.getString("getId");
//		getTitle = data.getString("getTitle");
//		getFinalId = data.getInt("getFinalId");
//		
//		HashMap<String, Integer> type_product = new HashMap<String, Integer>();
//		HashMap<String, Integer> type_live_demo = new HashMap<String, Integer>();
//
//
//		ImageView iv_before_visit = (ImageView) rootView.findViewById(R.id.iv_before_visit);
//		ImageView iv_after_visit = (ImageView) rootView.findViewById(R.id.iv_after_visit);
//
//		ImageView iv_before_Live_demo = (ImageView) rootView.findViewById(R.id.iv_before_Live_demo);
//		ImageView iv_after_Live_demo = (ImageView) rootView.findViewById(R.id.iv_after_Live_demo);
//
//		ImageView iv_before_POSM = (ImageView) rootView.findViewById(R.id.iv_before_POSM);
//		ImageView iv_after_POSM = (ImageView) rootView.findViewById(R.id.iv_after_POSM);		
//		
//		List<ActivitiesModel> activitys = db.getDataActivities(getFinalId);
//		for (ActivitiesModel cn : activitys) {
//			update_activities_id = cn.get_activities_id();
//			update_mapping_store_id = cn.get_mapping_store_id();
//			update_before_visit = cn.get_before_visit();
//			update_after_visit = cn.get_after_visit();
//			update_before_dummy_demo = cn.get_before_dummy_demo();
//			update_after_dummy_demo = cn.get_after_dummy_demo();
//			update_before_posm = cn.get_before_posm();
//			update_after_posm = cn.get_after_posm();
//
//			Matrix matrix = new Matrix();
//			Bitmap photoUpload = null;
//			
//        	File file1 = new File(ConstantPATH.PATH1+update_before_visit);
//        	if (file1.exists()) {            	
//    			Bitmap photo = BitmapFactory
//    					.decodeFile(ConstantPATH.PATH1+update_before_visit);
//    			if (photo.getWidth() >= photo.getHeight()) {
//    				photoUpload = photo.createScaledBitmap(photo, 384,
//    						(photo.getHeight() * 384) / photo.getWidth(), false);
//    			} else {
//    				photoUpload = photo.createScaledBitmap(photo,
//    						(photo.getWidth() * 384) / photo.getHeight(), 384,
//    						false);
//    			}
//    			  matrix.postRotate(getImageOrientation(ConstantPATH.PATH1+update_before_visit));
//    			  Bitmap rotatedBitmap = Bitmap.createBitmap(photoUpload, 0, 0, photoUpload.getWidth(),
//    					  photoUpload.getHeight(), matrix, true);
//    			  iv_before_visit.setImageBitmap(rotatedBitmap);
//        	}
//
//        	File file2 = new File(ConstantPATH.PATH1+update_after_visit);
//        	if (file2.exists()) {          	
//    			Bitmap photo = BitmapFactory
//    					.decodeFile(ConstantPATH.PATH1+update_after_visit);
//    			if (photo.getWidth() >= photo.getHeight()) {
//    				photoUpload = photo.createScaledBitmap(photo, 384,
//    						(photo.getHeight() * 384) / photo.getWidth(), false);
//    			} else {
//    				photoUpload = photo.createScaledBitmap(photo,
//    						(photo.getWidth() * 384) / photo.getHeight(), 384,
//    						false);
//    			}
//    			  matrix.postRotate(getImageOrientation(ConstantPATH.PATH1+update_after_visit));
//    			  Bitmap rotatedBitmap = Bitmap.createBitmap(photoUpload, 0, 0, photoUpload.getWidth(),
//    					  photoUpload.getHeight(), matrix, true);
//    			  iv_after_visit.setImageBitmap(rotatedBitmap);
//        	}
//        	
//        	File file3 = new File(ConstantPATH.PATH1+update_before_dummy_demo);
//        	if (file3.exists()) {       	
//    			Bitmap photo = BitmapFactory
//    					.decodeFile(ConstantPATH.PATH1+update_before_dummy_demo);
//    			if (photo.getWidth() >= photo.getHeight()) {
//    				photoUpload = photo.createScaledBitmap(photo, 384,
//    						(photo.getHeight() * 384) / photo.getWidth(), false);
//    			} else {
//    				photoUpload = photo.createScaledBitmap(photo,
//    						(photo.getWidth() * 384) / photo.getHeight(), 384,
//    						false);
//    			}
//    			  matrix.postRotate(getImageOrientation(ConstantPATH.PATH1+update_before_dummy_demo));
//    			  Bitmap rotatedBitmap = Bitmap.createBitmap(photoUpload, 0, 0, photoUpload.getWidth(),
//    					  photoUpload.getHeight(), matrix, true);
//    			  iv_before_Live_demo.setImageBitmap(rotatedBitmap);
//        	}
//
//
//        	File file4 = new File(ConstantPATH.PATH1+update_after_dummy_demo);
//        	if (file4.exists()) {       	
//    			Bitmap photo = BitmapFactory
//    					.decodeFile(ConstantPATH.PATH1+update_after_dummy_demo);
//    			if (photo.getWidth() >= photo.getHeight()) {
//    				photoUpload = photo.createScaledBitmap(photo, 384,
//    						(photo.getHeight() * 384) / photo.getWidth(), false);
//    			} else {
//    				photoUpload = photo.createScaledBitmap(photo,
//    						(photo.getWidth() * 384) / photo.getHeight(), 384,
//    						false);
//    			}
//    			  matrix.postRotate(getImageOrientation(ConstantPATH.PATH1+update_after_dummy_demo));
//    			  Bitmap rotatedBitmap = Bitmap.createBitmap(photoUpload, 0, 0, photoUpload.getWidth(),
//    					  photoUpload.getHeight(), matrix, true);
//    			  iv_after_Live_demo.setImageBitmap(rotatedBitmap);
//        	}
//
//
//        	File file5 = new File(ConstantPATH.PATH1+update_before_posm);
//        	if (file5.exists()) {       	
//    			Bitmap photo = BitmapFactory
//    					.decodeFile(ConstantPATH.PATH1+update_before_posm);
//    			if (photo.getWidth() >= photo.getHeight()) {
//    				photoUpload = photo.createScaledBitmap(photo, 384,
//    						(photo.getHeight() * 384) / photo.getWidth(), false);
//    			} else {
//    				photoUpload = photo.createScaledBitmap(photo,
//    						(photo.getWidth() * 384) / photo.getHeight(), 384,
//    						false);
//    			}
//    			  matrix.postRotate(getImageOrientation(ConstantPATH.PATH1+update_before_posm));
//    			  Bitmap rotatedBitmap = Bitmap.createBitmap(photoUpload, 0, 0, photoUpload.getWidth(),
//    					  photoUpload.getHeight(), matrix, true);
//    			  iv_before_POSM.setImageBitmap(rotatedBitmap);
//        	}
//
//
//        	File file6 = new File(ConstantPATH.PATH1+update_after_posm);
//        	if (file6.exists()) {       	
//    			Bitmap photo = BitmapFactory
//    					.decodeFile(ConstantPATH.PATH1+update_after_posm);
//    			if (photo.getWidth() >= photo.getHeight()) {
//    				photoUpload = photo.createScaledBitmap(photo, 384,
//    						(photo.getHeight() * 384) / photo.getWidth(), false);
//    			} else {
//    				photoUpload = photo.createScaledBitmap(photo,
//    						(photo.getWidth() * 384) / photo.getHeight(), 384,
//    						false);
//    			}
//    			  matrix.postRotate(getImageOrientation(ConstantPATH.PATH1+update_after_posm));
//    			  Bitmap rotatedBitmap = Bitmap.createBitmap(photoUpload, 0, 0, photoUpload.getWidth(),
//    					  photoUpload.getHeight(), matrix, true);
//    			  iv_after_POSM.setImageBitmap(rotatedBitmap);
//        	}
//			
//			List<DetailActivitiesModel> detailactivity = db.getDataDetailActivities(String.valueOf(cn.get_activities_id()));
//			for (DetailActivitiesModel cx : detailactivity) {
//				type_product.put(cx.get_detail_activities_id(),cx.get_type_product_id());
//				type_live_demo.put(cx.get_detail_activities_id(),cx.get_type_dummy_demo_id());
//			}
//		}
//		final SessionManagement session = new SessionManagement(activity);
//		final HashMap<String, String> user = session.getUserDetails();
//		UserId = user.get(session.USER_ID);	
//		
//		String last_id = new SimpleDateFormat("yyyyMMddHHmmss",
//				Locale.getDefault()).format(new Date());
//		last_id += user.get(session.USER_ID);
//
//		
//		if(update_activities_id.equals("")){
//			finalLastId = Utils.md5(last_id).toString();
//		} else {
//			finalLastId = String.valueOf(update_activities_id);			
//		}
//		
//		
//
//		initTypeProgram(rootView,update_activities_id);
//	}
//
//	
//	public void initTypeProgram(View rootView, String getFinalId) {
//		List<DummyDemoModel> dummydemo = db.getAllDummyDemoModel();
//		List<TypeDummyDemoModel> typedummydemo = db.getAllTypeDummyDemoModel();
//		List<DetailActivitiesModel> typedummydemodetail = db.getAllTypeDummyDemoDetailModel(getFinalId);
//		LinearLayout ll_loop = (LinearLayout) rootView.findViewById(R.id.ll_loop);
//		TextView tv ;
//		EditText et ;
//		CheckBox cb;
//		allResultText = new ArrayList<TextView>();
//		allResultCheckType1 = new ArrayList<CheckBox>();
//		allResultCheckType2 = new ArrayList<CheckBox>();
//		
//		int z = 0;
//		for (DummyDemoModel cn : dummydemo) {					
//			LinearLayout ll_loop_checkbox = new LinearLayout(activity);
//			ll_loop_checkbox.setOrientation(LinearLayout.VERTICAL);
//			ll_loop_checkbox.setLayoutParams(new LinearLayout.LayoutParams(
//					LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
//			ll_loop_checkbox.setGravity(Gravity.LEFT);
//			
//			LinearLayout ll_loop_checkbox2 = new LinearLayout(activity);
//			ll_loop_checkbox2.setOrientation(LinearLayout.HORIZONTAL);
//			ll_loop_checkbox2.setLayoutParams(new LinearLayout.LayoutParams(
//					LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
//			ll_loop_checkbox2.setGravity(Gravity.LEFT);
//
//			tv = new TextView(activity);
//			tv.setLayoutParams(new TableLayout.LayoutParams(
//					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1f));
//			tv.setTextColor(Color.parseColor("#000000"));
////			tv.setPadding(50, 0, 20, 0);
////			tv.setWidth(300);
//			tv.setText(cn.get_type_product_name());
//			tv.setId(cn.get_type_product_id());
//			allResultText.add(tv);
//			ll_loop_checkbox.addView(tv, 0);
//			ll_loop_checkbox.addView(ll_loop_checkbox2, 1);
//			int x = 1;
//			for (TypeDummyDemoModel ch : typedummydemo) {				
//				cb = new CheckBox(activity);
//				if(x%2 == 0){
//					allResultCheckType2.add(cb);
//				} else {
//					allResultCheckType1.add(cb);					
//				}
//				cb.setTextColor(Color.parseColor("#000000"));
//				cb.setLayoutParams(new TableLayout.LayoutParams(
//						LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1f));
//				
//				cb.setText(ch.get_type_dummy_demo_name());
//				cb.setId(ch.get_type_dummy_demo_id());
//				cb.setEnabled(false);
//				
//				
//
//
//				for (DetailActivitiesModel cx : typedummydemodetail) {	
//					if(String.valueOf(cn.get_type_product_id()).equals(String.valueOf(cx.get_type_product_id()))) {
//						if(ch.get_type_dummy_demo_id() == cx.get_type_dummy_demo_id()) {
//							cb.setChecked(true);		
//						} 			
//					}
//				}
//				ll_loop_checkbox2.addView(cb, x-1);
//				x++;
//			
//			}
//			
//			ll_loop.addView(ll_loop_checkbox);
//		}
//		
//	}
//
//	public static int getImageOrientation(String imagePath) {
//		int rotate = 0;
//		try {
//
//			File imageFile = new File(imagePath);
//			ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());
//			int orientation = exif.getAttributeInt(
//					ExifInterface.TAG_ORIENTATION,
//					ExifInterface.ORIENTATION_NORMAL);
//
//			switch (orientation) {
//			case ExifInterface.ORIENTATION_ROTATE_270:
//				rotate = 270;
//				break;
//			case ExifInterface.ORIENTATION_ROTATE_180:
//				rotate = 180;
//				break;
//			case ExifInterface.ORIENTATION_ROTATE_90:
//				rotate = 90;
//				break;
//			}
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return rotate;
//	}
}
