package com.andtechnology.indahjaya.view.journey.activejobs;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.andtechnology.indahjaya.R;
import com.andtechnology.indahjaya.database.DatabaseHandler;
import com.andtechnology.indahjaya.lib.AppLocationService;
import com.andtechnology.indahjaya.lib.util.GPSTracker;
import com.andtechnology.indahjaya.lib.util.SessionManagement;
import com.andtechnology.indahjaya.lib.util.Utils;
import com.andtechnology.indahjaya.model.ActivitiesModel;
import com.andtechnology.indahjaya.ui.service.APIParam;
import com.andtechnology.indahjaya.utils.Commons;
import com.andtechnology.indahjaya.view.ActivityLogin;
import com.andtechnology.indahjaya.view.framework.FragmentActivityFramework;

public class ActivityActiveJobs extends FragmentActivityFramework implements
		OnClickListener {
	android.support.v4.app.Fragment fragment = null;
	Intent intent;
	DatabaseHandler db;
	// initGPS
	GPSTracker gpsTracker;
	private String latitude, longitude;
	AppLocationService appLocationService;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_journey_activities);
		//Still don't know what API it's
		//initializeBindService(0);
		intent = getIntent();
		startFragment();

		TextView tv_today = (TextView) findViewById(R.id.tv_today);
		tv_today.setText(intent.getStringExtra("getDate"));

		db = new DatabaseHandler(activity);

		ImageView iv_back = (ImageView) findViewById(R.id.iv_back);
		iv_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});

		GPSTracker gpsTracker=new GPSTracker(activity);
		if(!gpsTracker.canGetLocation())
			gpsTracker.turnOnGps();

	}
    
//	public void initialGPS() {
//		appLocationService = new AppLocationService(activity);
//
//		gpsTracker = new GPSTracker(activity);
//
//		if (gpsTracker.canGetLocation()) {
//			latitude = String.valueOf(gpsTracker.latitude);
//			longitude = String.valueOf(gpsTracker.longitude);
//			Toast.makeText(activity,
//					"Latitude : " + latitude + "\nlongitude : " + longitude,
//					Toast.LENGTH_SHORT).show();
//
//		} else {
//			// gpsTracker.showSettingsAlert();
//
//			AlertDialog.Builder builder = new AlertDialog.Builder(activity);
//			builder.setTitle(R.string.confirmation);
//			builder.setMessage(R.string.check_in_confirmation);
//			builder.setPositiveButton(R.string.gps,
//					new DialogInterface.OnClickListener() {
//						public void onClick(DialogInterface dialog, int id) {
//							checkProvider(true, true);
//						}
//					});
//			builder.setNegativeButton(R.string.network,
//					new DialogInterface.OnClickListener() {
//						public void onClick(DialogInterface dialog, int id) {
//
//							checkProvider(false, false);
//						}
//					});
//			AlertDialog alert = builder.create();
//			alert.show();
//			Toast.makeText(activity, "please turn on your gps.",
//					Toast.LENGTH_SHORT).show();
//
//		}
//	}

//	private void checkProvider(boolean isCheckOut, boolean isGPS) {
//		if (isGPS) {
//			if (Utils.isGPSEnable(activity)) {
//				gpsTracker = new GPSTracker(activity);
//				if (gpsTracker.canGetLocation()) {
//					latitude = String.valueOf(gpsTracker.latitude);
//					longitude = String.valueOf(gpsTracker.longitude);
//					Toast.makeText(
//							activity,
//							"Latitude : " + latitude + "\nlongitude : "
//									+ longitude, Toast.LENGTH_SHORT).show();
//
//				} else {
//					gpsTracker.turnOnGps();
//
//				}
//
//				return;
//			}
//		} else {
//			if (Utils.isNetworkEnable(activity)) {
//				Location nwLocation = appLocationService
//						.getLocation(LocationManager.NETWORK_PROVIDER);
//
//				if (nwLocation != null) {
//					double latitude = nwLocation.getLatitude();
//					double longitude = nwLocation.getLongitude();
//					Toast.makeText(
//							activity,
//							"Mobile Location (NW): \nLatitude: " + latitude
//									+ "\nLongitude: " + longitude,
//							Toast.LENGTH_LONG).show();
//				} else {
//				}
//				return;
//			}
//		}
//
//		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
//		builder.setTitle(R.string.confirmation);
//		builder.setMessage(isGPS ? R.string.gps_not_active_alert
//				: R.string.network_not_active_alert);
//		builder.setPositiveButton(R.string.gps,
//				new DialogInterface.OnClickListener() {
//					public void onClick(DialogInterface dialog, int id) {
//						Intent callGPSSettingIntent = new Intent(
//								android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//						activity.startActivity(callGPSSettingIntent);
//					}
//				});
//		builder.setNegativeButton(R.string.cancel, null);
//		AlertDialog alert = builder.create();
//		alert.show();
//	}

	public void startFragment() {
		fragment = new FragmentActivities();
		Bundle data = new Bundle();
		data.putString("getTitle", intent.getStringExtra("getTitle"));
		data.putString("getId", intent.getStringExtra("getId"));
		data.putInt("getFinalId", intent.getIntExtra("getFinalId", 0));
		data.putString("getDate", intent.getStringExtra("getDate"));
		data.putString("mapping_store_id", intent.getStringExtra("mapping_store_id"));
		data.putString("longitude_store", intent.getStringExtra("longitude_store"));
		data.putString("latitude_store", intent.getStringExtra("latitude_store"));
		data.putString("progress_status", intent.getStringExtra("progress_status"));
		data.putString("date_checkin", intent.getStringExtra("date_checkin"));
		data.putString("radius", intent.getStringExtra("radius"));


		fragment.setArguments(data);
		FragmentManager fragmentManager = this.getSupportFragmentManager();
		fragmentManager.beginTransaction()
				.replace(R.id.content_frame, fragment).commit();

		ImageView iv_activities = (ImageView) findViewById(R.id.iv_activities);
		ImageView iv_sales = (ImageView) findViewById(R.id.iv_sales);
		ImageView iv_stock_report = (ImageView) findViewById(R.id.iv_stock_report);

		iv_activities.setVisibility(View.VISIBLE);
		iv_sales.setVisibility(View.INVISIBLE);
		iv_stock_report.setVisibility(View.INVISIBLE);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {
		case R.id.tv_activities:

			if (gpsTracker.canGetLocation()) {
				fragment = new FragmentActivities();
				FragmentManager fragmentManager = this.getSupportFragmentManager();
				Bundle data = new Bundle();
				data.putString("getTitle", intent.getStringExtra("getTitle"));
				data.putString("getId", intent.getStringExtra("getId"));
				data.putInt("getFinalId", intent.getIntExtra("getFinalId", 0));
				data.putString("getDate", intent.getStringExtra("getDate"));
				data.putString("latitude", intent.getStringExtra("latitude_store"));
				data.putString("longitude", intent.getStringExtra("longitude_store"));
				data.putString("mapping_store_id", intent.getStringExtra("mapping_store_id"));
                fragment.setArguments(data);
				fragmentManager.beginTransaction()
						.replace(R.id.content_frame, fragment).commit();
			} else {
				Toast.makeText(activity, "Open your gps ", Toast.LENGTH_SHORT).show();
			}

			break;

		default:
			break;
		}	

	}
	
	  
}
