package com.andtechnology.indahjaya.view.journey.activejobs;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.andtechnology.indahjaya.database.DatabaseHandler;
import com.andtechnology.indahjaya.view.framework.FragmentActivityFramework;
import com.andtechnology.indahjaya.R;

public class ActivityActiveJobsView extends FragmentActivityFramework implements
		OnClickListener {
	android.support.v4.app.Fragment fragment = null;
	Intent intent;
	DatabaseHandler db;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_journey_activities_view);
		
		intent = getIntent();
		startFragment();

		TextView tv_today = (TextView) findViewById(R.id.tv_today);
		tv_today.setText(intent.getStringExtra("getDate"));
		
		db = new DatabaseHandler(activity);

		TextView tv_activities = (TextView) findViewById(R.id.tv_activities);
		TextView tv_sales = (TextView) findViewById(R.id.tv_sales);
		TextView tv_stock_report = (TextView) findViewById(R.id.tv_stock_report);

		tv_activities.setOnClickListener(this);
		tv_sales.setOnClickListener(this);
		tv_stock_report.setOnClickListener(this);

		ImageView iv_back = (ImageView) findViewById(R.id.iv_back);
		iv_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	public void startFragment() {
		fragment = new FragmentActivitiesView();
		Bundle data = new Bundle();
		data.putString("getTitle", intent.getStringExtra("getTitle"));
		data.putString("getId", intent.getStringExtra("getId"));
		data.putInt("getFinalId", intent.getIntExtra("getFinalId", 0));
		data.putString("getDate", intent.getStringExtra("getDate"));
		
		fragment.setArguments(data);
		FragmentManager fragmentManager = this.getSupportFragmentManager();
		fragmentManager.beginTransaction()
				.replace(R.id.content_frame, fragment).commit();

		ImageView iv_activities = (ImageView) findViewById(R.id.iv_activities);
		ImageView iv_sales = (ImageView) findViewById(R.id.iv_sales);
		ImageView iv_stock_report = (ImageView) findViewById(R.id.iv_stock_report);
		
		iv_activities.setVisibility(View.VISIBLE);
		iv_sales.setVisibility(View.INVISIBLE);
		iv_stock_report.setVisibility(View.INVISIBLE);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		ImageView iv_activities = (ImageView) findViewById(R.id.iv_activities);
		ImageView iv_sales = (ImageView) findViewById(R.id.iv_sales);
		ImageView iv_stock_report = (ImageView) findViewById(R.id.iv_stock_report);
		switch (v.getId()) {
		case R.id.tv_activities:
			fragment = new FragmentActivitiesView();
			iv_activities.setVisibility(View.VISIBLE);
			iv_sales.setVisibility(View.INVISIBLE);
			iv_stock_report.setVisibility(View.INVISIBLE);
			break;
		

		default:
			break;
		}
		FragmentManager fragmentManager = this.getSupportFragmentManager();
		Bundle data = new Bundle();
		data.putString("getTitle", intent.getStringExtra("getTitle"));
		data.putString("getId", intent.getStringExtra("getId"));
		data.putInt("getFinalId", intent.getIntExtra("getFinalId", 0));
		data.putString("getDate", intent.getStringExtra("getDate"));
		
		fragment.setArguments(data);
		fragmentManager.beginTransaction()
				.replace(R.id.content_frame, fragment).commit();

	}
}
