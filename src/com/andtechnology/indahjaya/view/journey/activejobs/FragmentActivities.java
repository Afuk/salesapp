package com.andtechnology.indahjaya.view.journey.activejobs;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.andtechnology.indahjaya.AddQuestion;
import com.andtechnology.indahjaya.R;
import com.andtechnology.indahjaya.adapter.MultiSelectionSpinner;
import com.andtechnology.indahjaya.adapter.QuestionListAdapter;
import com.andtechnology.indahjaya.database.DatabaseHandler;
import com.andtechnology.indahjaya.lib.AppLocationService;
import com.andtechnology.indahjaya.lib.util.GPSTracker;
import com.andtechnology.indahjaya.lib.util.SphericalUtils;
import com.andtechnology.indahjaya.lib.util.Utils;
import com.andtechnology.indahjaya.lib.util.UtilsCamera;
import com.andtechnology.indahjaya.model.ActivitiesModel;
import com.andtechnology.indahjaya.model.QuestionModel;
import com.andtechnology.indahjaya.utils.Commons;
import com.andtechnology.indahjaya.view.framework.FragmentFramework;

public class FragmentActivities extends FragmentFramework implements
		View.OnClickListener {
	DatabaseHandler db;

	int finalFlag;
	String getId, getTitle;
	String UserId;

	String update_activities_id = "";
	int update_mapping_store_id = 0;
	String formattedDate;
	int radius = 0;

	String update_detail_activities_id;
	String update_type_product_id;

	private Uri fileUri = null;

	private String before_image_url, after_image_url;
	private ImageView iv_before_visit, iv_after_visit;
	private MultiSelectionSpinner spinner;
	private ArrayList<QuestionModel> question_list = new ArrayList<QuestionModel>();
	private ListView lvQuestionList;
	private QuestionListAdapter adapter;
	private String latitude_store, longitude_store;
	private String mapping_store_id;
	// initGPS
	// GPSTracker gpsTracker;
	private String latitude, longitude, progress_status;
	private String date_checkin;
	private String activity_id;

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_activities,
				container, false);
		db = new DatabaseHandler(rootView.getContext());
		activity_id = Utils.autoCreateIdModel(UserId);
		PackageManager packageManager = activity.getPackageManager();
		// gpsTracker = new GPSTracker(getActivity());
		initListView(rootView);

		LoadView(rootView);
		return rootView;
	}

	public void LoadView(View rootView) {
		final Bundle data = getArguments();
		TextView tv_Oulite_Id = (TextView) rootView
				.findViewById(R.id.tv_Oulite_Id);
		TextView tv_Store_Name = (TextView) rootView
				.findViewById(R.id.tv_Store_Name);
		tv_Oulite_Id.setText("Store ID : " + data.getString("getId"));
		tv_Store_Name.setText("Store Name : " + data.getString("getTitle"));
		radius = Integer.parseInt(data.getString("radius"));
		getId = data.getString("getId");
		getTitle = data.getString("getTitle");
		mapping_store_id = data.getString("mapping_store_id");
		longitude_store = data.getString("longitude_store");
		latitude_store = data.getString("latitude_store");
		date_checkin = data.getString("date_checkin");

		progress_status = data.getString("progress_status");
		iv_before_visit = (ImageView) rootView
				.findViewById(R.id.iv_before_visit);
		iv_after_visit = (ImageView) rootView.findViewById(R.id.iv_after_visit);

		if (Integer.parseInt(mapping_store_id) > 0
				&& Integer.parseInt(progress_status) == 1
				|| Integer.parseInt(progress_status) == 2) {
			((ImageView) rootView.findViewById(R.id.addQuestion))
					.setVisibility(View.INVISIBLE);
			((Button) rootView.findViewById(R.id.btn_next))
					.setVisibility(View.INVISIBLE);

			onLoadReview(mapping_store_id);
			return;
		}

		iv_before_visit.setOnClickListener(this);
		iv_after_visit.setOnClickListener(this);

		Button next = (Button) rootView.findViewById(R.id.btn_next);
		next.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				AlertDialog.Builder builder = new AlertDialog.Builder(v
						.getContext());
				builder.setTitle(R.string.confirmation);
				builder.setMessage(R.string.check_out_confirmation);
				builder.setPositiveButton(R.string.yes,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {

								GPSTracker gpsTracker = new GPSTracker(
										getActivity());
								if (!gpsTracker.canGetLocation()) {
									dialog.dismiss();
									gpsTracker.turnOnGps();
								} else {
									latitude = String
											.valueOf(gpsTracker.latitude);
									longitude = String
											.valueOf(gpsTracker.longitude);
									double from[] = new double[2];
									from[0] = gpsTracker.latitude;
									from[1] = gpsTracker.longitude;
									int maxDistance = radius * 100;
									if (isValidDistance(from, maxDistance)) {
										int size = question_list.size();
										StringBuilder type_activity_id = new StringBuilder();
										StringBuilder question_id = new StringBuilder();
										StringBuilder answer_id = new StringBuilder();
										StringBuilder type_activity_value = new StringBuilder();
										StringBuilder question_value = new StringBuilder();
										StringBuilder answer_value = new StringBuilder();

										for (int i = 0; i < size; i++) {
											if (i < size - 1) {
												type_activity_id
														.append(question_list
																.get(i)
																.getType_activity_id()
																+ "#");

												question_id
														.append(question_list
																.get(i)
																.getQuestion_id()
																+ "#");

												answer_id.append(question_list
														.get(i).getAnswer_id()
														+ "#");

												type_activity_value
														.append(question_list
																.get(i)
																.getType_activity_value()
																+ "#");

												question_value
														.append(question_list
																.get(i)
																.getQuestion_value()
																+ "#");

												answer_value
														.append(question_list
																.get(i)
																.getAnswer_value()
																+ "#");
											} else {
												type_activity_id
														.append(question_list
																.get(i)
																.getType_activity_id());

												question_id
														.append(question_list
																.get(i)
																.getQuestion_id());

												answer_id.append(question_list
														.get(i).getAnswer_id());

												type_activity_value
														.append(question_list
																.get(i)
																.getType_activity_value());

												question_value
														.append(question_list
																.get(i)
																.getQuestion_value());

												answer_value
														.append(question_list
																.get(i)
																.getAnswer_value());
											}

										}
										DatabaseHandler db = new DatabaseHandler(
												activity);
										ActivitiesModel activity_model = new ActivitiesModel();

										activity_model
												.set_activities_id(activity_id/*
																			 * Utils
																			 * .
																			 * autoCreateIdModel
																			 * (
																			 * user_id
																			 * )
																			 */);
										activity_model.set_mapping_store_id(Integer
												.parseInt(mapping_store_id));
										activity_model
												.set_before_photo_store(before_image_url);
										activity_model
												.set_after_photo_store(after_image_url);
										activity_model
												.set_longtitude_sales(longitude);
										activity_model
												.set_latitude_sales(latitude);
										activity_model.set_status(1);

										String date = Utils.getTimeNow();
										activity_model.set_user_change(user_id);
										activity_model.set_date_change(date);
										activity_model.set_stsrc("A");
										activity_model
												.setType_activity_id(type_activity_id
														.toString());
										activity_model
												.setType_activity_value(type_activity_value
														.toString());
										activity_model
												.setQuestion_id(question_id
														.toString());
										activity_model
												.setQuestion_value(question_value
														.toString());
										activity_model.setAnswer_id(answer_id
												.toString());
										activity_model
												.setAnswer_value(answer_value
														.toString());
										activity_model
												.setDate_checkin(date_checkin);
										activity_model.setDate_checkout(Utils
												.getTimeNow());

										String error = validationForm(
												activity_model).toString();
										if (error.length() > 0) {
											Toast.makeText(activity,
													"" + error,
													Toast.LENGTH_SHORT).show();
											return;
										}
										// validationForm(activity_model);
										db.addActivities(activity_model);
										db.updateProggressMappingStore(Integer
												.parseInt(mapping_store_id), 1);

										getActivity().finish();
									} else {
										Toast.makeText(
												activity,
												"cannot checkout,\nyou too far from store location",
												Toast.LENGTH_SHORT).show();
									}
									dialog.dismiss();
								}

							}
						});
				builder.setNegativeButton(R.string.no, null);
				final AlertDialog alert = builder.create();
				alert.show();

			}
		});

		((ImageView) rootView.findViewById(R.id.addQuestion))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent i = new Intent(activity, AddQuestion.class);
						startActivityForResult(i, Commons.ADD_QUESTION);
					}
				});
	}

	public void onLoadReview(String mapping_store_id) {
		DatabaseHandler db = new DatabaseHandler(getActivity());
		ActivitiesModel activitiesModel = db.getActivityByID(Integer
				.parseInt(mapping_store_id));
		if (activitiesModel.get_activities_id() == null)
			return;
		String type_activity_value[] = activitiesModel.getType_activity_value()
				.split("#");
		String question_value[] = activitiesModel.getQuestion_value()
				.split("#");
		String answer_value[] = activitiesModel.getAnswer_value().split("#");
		for (int i = 0; i < type_activity_value.length; i++) {
			QuestionModel model = new QuestionModel();
			model.setType_activity_value(type_activity_value[i]);
			model.setQuestion_value(question_value[i]);
			model.setAnswer_value(answer_value[i]);
			question_list.add(model);

		}
		adapter.notifyDataSetChanged();
		setListViewHeightBasedOnChildren(lvQuestionList);
		Utils.loadImageAqueryAutoRotate(getActivity(), iv_before_visit,
				activitiesModel.get_before_photo_store(),
				Utils.getDisplayWidth(getActivity()));
		Utils.loadImageAqueryAutoRotate(getActivity(), iv_after_visit,
				activitiesModel.get_after_photo_store(),
				Utils.getDisplayWidth(getActivity()));

		// ----------end Listview

	}

	public void initListView(View view) {

		lvQuestionList = (ListView) view.findViewById(R.id.lvQuestionList);
		adapter = new QuestionListAdapter(getActivity(), 0, question_list);
		lvQuestionList.setAdapter(adapter);
		lvQuestionList.setOnTouchListener(new OnTouchListener() {
			// Setting on Touch Listener for handling the touch inside
			// ScrollView
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// Disallow the touch request for parent scroll on touch of
				// child view
				v.getParent().requestDisallowInterceptTouchEvent(false);
				return false;
			}
		});
		setListViewHeightBasedOnChildren(lvQuestionList);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Commons.RESULT_OK_FRAGMENT) {
			if (requestCode == Commons.FROM_CAMERA_ONE) {
				Utils.loadImageAqueryAutoRotate(activity, iv_before_visit,
						fileUri.getPath(), Utils.getDisplayWidth(activity));
				before_image_url = fileUri.getPath();
			} else if (requestCode == Commons.FROM_CAMERA_TWO) {
				Utils.loadImageAqueryAutoRotate(activity, iv_after_visit,
						fileUri.getPath(), Utils.getDisplayWidth(activity));
				after_image_url = fileUri.getPath();

			} else if (requestCode == Commons.ADD_QUESTION) {
				QuestionModel model = data.getParcelableExtra("question_model");
				question_list.add(model);
				adapter.notifyDataSetChanged();
				setListViewHeightBasedOnChildren(lvQuestionList);
			}
		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		// capture picture
		// if (UtilsCamera.isDeviceSupportCamera(activity)) {
		switch (v.getId()) {
		case R.id.iv_before_visit:
			if (UtilsCamera.isDeviceSupportCamera(activity)) {
				captureImageNew(Commons.FROM_CAMERA_ONE);
			} else {
				Utils.showToast(activity, "This device not support camera");
			}
			break;
		case R.id.iv_after_visit:
			if (UtilsCamera.isDeviceSupportCamera(activity)) {
				captureImageNew(Commons.FROM_CAMERA_TWO);
			} else {
				Utils.showToast(activity, "This device not support camera");
			}

		default:
			break;
		}

	}

	private void captureImageNew(int from_camera) {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String formattedDate = df.format(new Date());

		String date = new SimpleDateFormat("yyyyMMddHHmmss",
				Locale.getDefault()).format(new Date());
		String filename = user_id + '_' + date + "_" + from_camera;

		fileUri = UtilsCamera.getOutputMediaFileUri(
				UtilsCamera.MEDIA_TYPE_IMAGE, activity, filename);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
		startActivityForResult(intent, from_camera);

	}

	public void setListViewHeightBasedOnChildren(ListView listView) {
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null)
			return;

		int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(),
				MeasureSpec.UNSPECIFIED);
		int totalHeight = 0;
		View view = null;
		for (int i = 0; i < listAdapter.getCount(); i++) {
			view = listAdapter.getView(i, view, listView);
			if (i == 0)
				view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth,
						LayoutParams.WRAP_CONTENT));

			view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
			totalHeight += view.getMeasuredHeight();
		}
		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight
				+ (listView.getDividerHeight() * (listAdapter.getCount() - 1));

		listView.setLayoutParams(params);
		listView.requestLayout();
		lvQuestionList.setOnTouchListener(new OnTouchListener() {
			// Setting on Touch Listener for handling the touch inside
			// ScrollView
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// Disallow the touch request for parent scroll on touch of
				// child view
				v.getParent().requestDisallowInterceptTouchEvent(false);
				return false;
			}
		});

	}

	public boolean isValidDistance(double from[], int max_distance) {
		if (from[0] == 0 && from[1] == 0) {
			if (latitude == null || longitude == null) {
				Toast.makeText(
						activity,
						"Failed,your position latlong 0, Please check your gps",
						Toast.LENGTH_SHORT).show();
				return false;
			}
			Toast.makeText(
					activity,
					"Latlong store awal masih kosong jadi diperbolehkan upload.",
					Toast.LENGTH_SHORT).show();
			return true;
		} else {

			if (latitude_store == null || latitude_store == null) {
				Toast.makeText(
						activity,
						"Failed,your position latlong 0, Please check your gps",
						Toast.LENGTH_SHORT).show();
				return true;
			} else {

				double to[] = new double[2];
				to[0] = Double.parseDouble(latitude_store);
				to[1] = Double.parseDouble(longitude_store);
				int distance = (int) SphericalUtils.computeDistanceBetween(
						from, to);
				return distance <= max_distance;
			}
		}
	}

	public String validationForm(ActivitiesModel activity_model) {
		if (activity_model.get_after_photo_store() == null)
			return "after photo not null";
		if (activity_model.get_before_photo_store() == null)
			return "before photo not null";
		if (activity_model.get_latitude_sales() == null)
			return "latitude not null";
		if (activity_model.get_longtitude_sales() == null)
			return "longitude not null";
		if (activity_model.getAnswer_id() == null)
			return "Answer not null";
		if (activity_model.getQuestion_id() == null)
			return "Question not null";
		if (activity_model.getType_activity_id() == null
				|| activity_model.getType_activity_id().length() == 0)
			return "please input your question..";
		else
			return "";

	}

}
