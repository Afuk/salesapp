package com.andtechnology.indahjaya.view.journey;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.andtechnology.indahjaya.adapter.ListFragmentActiveJobsAdapter;
import com.andtechnology.indahjaya.database.DatabaseHandler;
import com.andtechnology.indahjaya.lib.util.SessionManagement;
import com.andtechnology.indahjaya.lib.util.UtilsPointer;
import com.andtechnology.indahjaya.model.MappingStoreModel;
import com.andtechnology.indahjaya.model.NavDrawerMenuModel;
import com.andtechnology.indahjaya.service.MyService;
import com.andtechnology.indahjaya.view.framework.FragmentFramework;
import com.andtechnology.indahjaya.view.journey.activejobs.ActivityActiveJobs;
import com.andtechnology.indahjaya.R;

public class FragmentActiveJobs extends FragmentFramework {

	MappingStoreModel model;
	DatabaseHandler db;
	Calendar c;
	ArrayList<MappingStoreModel> navDrawerMenu;
	ListView mDrawerList;

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_active_jobs,
				container, false);

		c = Calendar.getInstance();

		SimpleDateFormat df = new SimpleDateFormat("EEEE, dd MMMM yyyy");
		final String formattedDate = df.format(new Date());

		TextView tv_today = (TextView) rootView.findViewById(R.id.tv_today);
		tv_today.setText(formattedDate);


		navDrawerMenu = new ArrayList<MappingStoreModel>();
		mDrawerList = (ListView) rootView.findViewById(R.id.lv_store);

		db = new DatabaseHandler(rootView.getContext());
		SessionManagement session = new SessionManagement(activity);
		HashMap<String, String> user = session.getUserDetails();

//		SimpleDateFormat dfg = new SimpleDateFormat("yyyy-MM-dd");
//		List<MappingStoreModel> mappingstoremodel = db.getAllMappingStore(0, user.get(session.USER_ID),dfg.format(new Date()));
//
//		for (MappingStoreModel cn : mappingstoremodel) {
//			model = new NavDrawerMenuModel();
//			model.setTitle(cn.get_store_name());
//			model.setIds(cn.get_store_code());
//			model.setId(cn.get_mapping_store_id());
//			model.setAddress(cn.get_address());
//			navDrawerMenu.add(model);
//		}
//
//		// setting the nav drawer list adapter
//		ListFragmentActiveJobsAdapter adapter = new ListFragmentActiveJobsAdapter(
//				a, navDrawerMenu);
//		mDrawerList.setAdapter(adapter);

		mDrawerList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				NavDrawerMenuModel model = (NavDrawerMenuModel) parent
						.getAdapter().getItem(position);
				
				final Intent i = new Intent(v.getContext(), ActivityActiveJobs.class);
				i.putExtra("getTitle", model.getTitle());
				i.putExtra("getId", model.getIds());
				i.putExtra("get_mapping_id", model.getId());
				i.putExtra("getDate", formattedDate);

				
				AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
				builder.setTitle(R.string.confirmation);
				builder.setMessage(R.string.check_in_confirmation);
				builder.setPositiveButton(R.string.yes,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {			
								startActivityForResult(i, 901);
							}
						});
				builder.setNegativeButton(R.string.no, null);
				AlertDialog alert = builder.create();
				alert.show();
			}
		});
		
		
		return rootView;
	}
	
	public void LoadList() {
		SimpleDateFormat dfg = new SimpleDateFormat("yyyy-MM-dd");
		SessionManagement session = new SessionManagement(activity);
		HashMap<String, String> user = session.getUserDetails();
		List<MappingStoreModel> mappingstoremodel = db.getAllMappingStore(0, user.get(session.USER_ID),dfg.format(new Date()));
        if(mappingstoremodel.size() == 0) {
        	activity.findViewById(R.id.EmptyIndicator).setVisibility(View.VISIBLE);
        } else {
        	activity.findViewById(R.id.EmptyIndicator).setVisibility(View.INVISIBLE);            
        }

		// setting the nav drawer list adapter
		ListFragmentActiveJobsAdapter adapter = new ListFragmentActiveJobsAdapter(
				activity,(ArrayList<MappingStoreModel>)mappingstoremodel);
		mDrawerList.setAdapter(adapter);
		
	}

	
	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		navDrawerMenu.removeAll(navDrawerMenu);
		LoadList();
	}
	
	
}
