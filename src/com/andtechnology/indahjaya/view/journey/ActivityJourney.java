package com.andtechnology.indahjaya.view.journey;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.andtechnology.indahjaya.view.framework.FragmentActivityFramework;
import com.andtechnology.indahjaya.R;

public class ActivityJourney extends FragmentActivityFramework implements
		OnClickListener {
	android.support.v4.app.Fragment fragment = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_journey);
		startFragment();

		TextView tv_active_jobs = (TextView) findViewById(R.id.tv_active_jobs);
		TextView tv_draft_jobs = (TextView) findViewById(R.id.tv_draft_jobs);
		TextView tv_jobs_done = (TextView) findViewById(R.id.tv_jobs_done);

		tv_active_jobs.setOnClickListener(this);
		tv_draft_jobs.setOnClickListener(this);
		tv_jobs_done.setOnClickListener(this);

		ImageView iv_back = (ImageView) findViewById(R.id.iv_back);
		iv_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	public void startFragment() {
		fragment = new FragmentActiveJobs();
		FragmentManager fragmentManager = this.getSupportFragmentManager();
		fragmentManager.beginTransaction()
				.replace(R.id.content_frame, fragment).commit();

		ImageView iv_active_jobs = (ImageView) findViewById(R.id.iv_active_jobs);
		ImageView iv_draft_jobs = (ImageView) findViewById(R.id.iv_draft_jobs);
		ImageView iv_jobs_done = (ImageView) findViewById(R.id.iv_jobs_done);
		iv_active_jobs.setVisibility(View.VISIBLE);
		iv_draft_jobs.setVisibility(View.INVISIBLE);
		iv_jobs_done.setVisibility(View.INVISIBLE);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		ImageView iv_active_jobs = (ImageView) findViewById(R.id.iv_active_jobs);
		ImageView iv_draft_jobs = (ImageView) findViewById(R.id.iv_draft_jobs);
		ImageView iv_jobs_done = (ImageView) findViewById(R.id.iv_jobs_done);
		switch (v.getId()) {
		case R.id.tv_active_jobs:
			fragment = new FragmentActiveJobs();
			iv_active_jobs.setVisibility(View.VISIBLE);
			iv_draft_jobs.setVisibility(View.INVISIBLE);
			iv_jobs_done.setVisibility(View.INVISIBLE);
			break;
		case R.id.tv_draft_jobs:
			fragment = new FragmentDraftJobs();
			iv_active_jobs.setVisibility(View.INVISIBLE);
			iv_draft_jobs.setVisibility(View.VISIBLE);
			iv_jobs_done.setVisibility(View.INVISIBLE);
			break;
		case R.id.tv_jobs_done:
			fragment = new FragmentJobsDone();
			iv_active_jobs.setVisibility(View.INVISIBLE);
			iv_draft_jobs.setVisibility(View.INVISIBLE);
			iv_jobs_done.setVisibility(View.VISIBLE);
			break;

		default:
			break;
		}
		FragmentManager fragmentManager = this.getSupportFragmentManager();
		fragmentManager.beginTransaction()
				.replace(R.id.content_frame, fragment).commit();

	}
}
