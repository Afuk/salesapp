package com.andtechnology.indahjaya.view;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.andtechnology.indahjaya.R;
import com.andtechnology.indahjaya.R.layout;
import com.andtechnology.indahjaya.database.DatabaseHandler;
import com.andtechnology.indahjaya.lib.util.Utils;
import com.andtechnology.indahjaya.lib.util.UtilsCamera;
import com.andtechnology.indahjaya.model.MarketSurveyModel;
import com.andtechnology.indahjaya.model.StoreRecommendationModel;
import com.andtechnology.indahjaya.utils.Commons;
import com.andtechnology.indahjaya.view.framework.ActivityFramework;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class MarketSurveyDetail extends ActivityFramework implements
		OnClickListener {
	ImageView ivCameraOne, ivCameraTwo, ivCameraThree, ivCameraFour,
			ivCameraFive;

	private Uri fileUri = null;

	private String image_url_one, image_url_two, image_url_three,
			image_url_four, image_url_five;
	private boolean isView = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_market_survey_detail);
		initButton();
		if (getIntent().getExtras().containsKey("isView")) {
			if (getIntent().getExtras().getBoolean("isView"))
				loadViewData();
		}

		ImageView iv_back = (ImageView) findViewById(R.id.iv_back);
		iv_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	public void initButton() {
		ivCameraOne = (ImageView) findViewById(R.id.ivCameraOne);
		ivCameraOne.setOnClickListener(this);
		ivCameraTwo = (ImageView) findViewById(R.id.ivCameraTwo);
		ivCameraTwo.setOnClickListener(this);
		ivCameraThree = (ImageView) findViewById(R.id.ivCameraThree);
		ivCameraThree.setOnClickListener(this);
		ivCameraFour = (ImageView) findViewById(R.id.ivCameraFour);
		ivCameraFour.setOnClickListener(this);
		ivCameraFive = (ImageView) findViewById(R.id.ivCameraFive);
		ivCameraFive.setOnClickListener(this);
		((Button) findViewById(R.id.btnSubmit)).setOnClickListener(this);

	}

	public void loadViewData() {
		disableView();
		DatabaseHandler db = new DatabaseHandler(activity);
		MarketSurveyModel model = db.getMarketSurveyByID(getIntent()
				.getStringExtra("id"));
		Utils.loadImageAqueryAutoRotate(activity, ivCameraOne,
				model.getPhoto_one(),
				Utils.getDisplayWidth(getApplicationContext()));
		Utils.loadImageAqueryAutoRotate(activity, ivCameraTwo,
				model.getPhoto_two(),
				Utils.getDisplayWidth(getApplicationContext()));
		Utils.loadImageAqueryAutoRotate(activity, ivCameraThree,
				model.getPhoto_three(),
				Utils.getDisplayWidth(getApplicationContext()));
		Utils.loadImageAqueryAutoRotate(activity, ivCameraFour,
				model.getPhoto_four(),
				Utils.getDisplayWidth(getApplicationContext()));
		Utils.loadImageAqueryAutoRotate(activity, ivCameraFive,
				model.getPhoto_five(),
				Utils.getDisplayWidth(getApplicationContext()));
		((EditText) findViewById(R.id.edtImportir))
				.setText(model.getImportir());
		((EditText) findViewById(R.id.edtBrandName)).setText(model
				.getBrand_name());
		((EditText) findViewById(R.id.edtProductName)).setText(model
				.getProduct_name());
		((EditText) findViewById(R.id.edtPrice)).setText(model.getPrice());
		((EditText) findViewById(R.id.edtNotes)).setText(model.getComment());

	}

	public void disableView() {
		((ImageView) findViewById(R.id.ivCameraOne)).setEnabled(false);
		((ImageView) findViewById(R.id.ivCameraTwo)).setEnabled(false);
		((ImageView) findViewById(R.id.ivCameraThree)).setEnabled(false);
		((ImageView) findViewById(R.id.ivCameraFour)).setEnabled(false);
		((ImageView) findViewById(R.id.ivCameraFive)).setEnabled(false);
		((EditText) findViewById(R.id.edtImportir)).setFocusable(false);
		((EditText) findViewById(R.id.edtBrandName)).setFocusable(false);
		((EditText) findViewById(R.id.edtProductName)).setFocusable(false);
		((EditText) findViewById(R.id.edtPrice)).setFocusable(false);
		((EditText) findViewById(R.id.edtNotes)).setFocusable(false);
		((Button) findViewById(R.id.btnSubmit)).setVisibility(View.INVISIBLE);

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.ivCameraOne:
			if (UtilsCamera.isDeviceSupportCamera(MarketSurveyDetail.this)) {
				captureImage(Commons.FROM_CAMERA_ONE);
			} else {
				Utils.showToast(MarketSurveyDetail.this,
						"This device not support camera");
			}
			break;
		case R.id.ivCameraTwo:
			if (UtilsCamera.isDeviceSupportCamera(MarketSurveyDetail.this)) {
				captureImage(Commons.FROM_CAMERA_TWO);
			} else {
				Utils.showToast(MarketSurveyDetail.this,
						"This device not support camera");
			}
			break;
		case R.id.ivCameraThree:
			if (UtilsCamera.isDeviceSupportCamera(MarketSurveyDetail.this)) {
				captureImage(Commons.FROM_CAMERA_THREE);
			} else {
				Utils.showToast(MarketSurveyDetail.this,
						"This device not support camera");
			}
			break;
		case R.id.ivCameraFour:
			if (UtilsCamera.isDeviceSupportCamera(MarketSurveyDetail.this)) {
				captureImage(Commons.FROM_CAMERA_FOUR);
			} else {
				Utils.showToast(MarketSurveyDetail.this,
						"This device not support camera");
			}
			break;
		case R.id.ivCameraFive:
			if (UtilsCamera.isDeviceSupportCamera(MarketSurveyDetail.this)) {
				captureImage(Commons.FROM_CAMERA_FIVE);
			} else {
				Utils.showToast(MarketSurveyDetail.this,
						"This device not support camera");
			}
			break;

		case R.id.btnSubmit:
			String date = Utils.getTimeNow();

			MarketSurveyModel model = new MarketSurveyModel();
			model.setMarket_intelligence_id(Utils.autoCreateIdModel(user_id));
			model.setUser_sales_id(user_id);
			model.setImportir(((EditText) findViewById(R.id.edtImportir))
					.getText().toString());
			model.setBrand_name(((EditText) findViewById(R.id.edtBrandName))
					.getText().toString());
			model.setProduct_name(((EditText) findViewById(R.id.edtProductName))
					.getText().toString());
			model.setPhoto_one(image_url_one);
			model.setPhoto_two(image_url_two);
			model.setPhoto_three(image_url_three);
			model.setPhoto_four(image_url_four);
			model.setPhoto_five(image_url_five);
			model.setPrice(((EditText) findViewById(R.id.edtPrice)).getText()
					.toString());
			model.setComment(((EditText) findViewById(R.id.edtNotes)).getText()
					.toString());
			model.setDate_market_intelligence(date);
			model.setUser_change(user_id);
			model.setDate_change(date);
			model.setStsrc("A");
			model.setStatus("1");


			String error = validationForm(model).toString();
			if (error.length() > 0) {
				Toast.makeText(activity, "" + error, Toast.LENGTH_SHORT).show();
				return;
			}
			DatabaseHandler db = new DatabaseHandler(getApplicationContext());
			db.addMarketSurvey(model);
			setResult(RESULT_OK);
			finish();

			break;

		default:
			break;
		}
	}

	private void captureImage(int from_camera) {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String formattedDate = df.format(new Date());

		String date = new SimpleDateFormat("yyyyMMddHHmmss",
				Locale.getDefault()).format(new Date());
		String filename = user_id + '_' + date + "_" + from_camera;

		fileUri = UtilsCamera
				.getOutputMediaFileUri(UtilsCamera.MEDIA_TYPE_IMAGE,
						MarketSurveyDetail.this, filename);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
		startActivityForResult(intent, from_camera);

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Commons.RESULT_OK_FRAGMENT) {
			if (requestCode == Commons.FROM_CAMERA_ONE) {
				Utils.loadImageAqueryAutoRotate(activity, ivCameraOne,
						fileUri.getPath(), Utils.getDisplayWidth(activity));
				image_url_one = fileUri.getPath();
			} else if (requestCode == Commons.FROM_CAMERA_TWO) {
				Utils.loadImageAqueryAutoRotate(activity, ivCameraTwo,
						fileUri.getPath(),
						Utils.getDisplayWidth(getApplicationContext()));
				image_url_two = fileUri.getPath();

			} else if (requestCode == Commons.FROM_CAMERA_THREE) {
				Utils.loadImageAqueryAutoRotate(activity, ivCameraThree,
						fileUri.getPath(),
						Utils.getDisplayWidth(getApplicationContext()));
				image_url_three = fileUri.getPath();

			} else if (requestCode == Commons.FROM_CAMERA_FOUR) {
				Utils.loadImageAqueryAutoRotate(activity, ivCameraFour,
						fileUri.getPath(),
						Utils.getDisplayWidth(getApplicationContext()));
				image_url_four = fileUri.getPath();

			} else if (requestCode == Commons.FROM_CAMERA_FIVE) {
				Utils.loadImageAqueryAutoRotate(activity, ivCameraFive,
						fileUri.getPath(),
						Utils.getDisplayWidth(getApplicationContext()));
				image_url_five = fileUri.getPath();

			}

		}
	}

	public String validationForm(MarketSurveyModel model) {
		if (model.getImportir() == null || model.getImportir().length() == 0)
			return "Importir not null";
		else if (model.getBrand_name() == null
				|| model.getBrand_name().length() == 0)
			return "Brand name not null";
		else if (model.getProduct_name() == null
				|| model.getProduct_name().length() == 0)
			return "Product Name not null";
		else if (model.getPrice() == null || model.getPrice().length() == 0)
			return "Price not null";
		else if (model.getComment() == null || model.getComment().length() == 0)
			return "Notes not null";
		else {
			return "";
		}

	}
}
