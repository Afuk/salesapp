package com.andtechnology.indahjaya.view.store;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.location.Location;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.andtechnology.indahjaya.R;
import com.andtechnology.indahjaya.adapter.MultiSelectionSpinner;
import com.andtechnology.indahjaya.database.DatabaseHandler;
import com.andtechnology.indahjaya.lib.AppLocationService;
import com.andtechnology.indahjaya.lib.util.GPSTracker;
import com.andtechnology.indahjaya.lib.util.SessionManagement;
import com.andtechnology.indahjaya.lib.util.Utils;
import com.andtechnology.indahjaya.lib.util.UtilsCamera;
import com.andtechnology.indahjaya.model.ActivitiesModel;
import com.andtechnology.indahjaya.model.CategoryProductModel;
import com.andtechnology.indahjaya.model.StoreRecommendationModel;
import com.andtechnology.indahjaya.ui.service.APIParam;
import com.andtechnology.indahjaya.utils.Commons;
import com.andtechnology.indahjaya.view.framework.ActivityFramework;

public class ActivityStoreRegistrationAction extends ActivityFramework
		implements OnClickListener {
	private Uri fileUri = null;
	private ImageView ivCamera2, ivCamera;
	GPSTracker gpsTracker;
	private String latitude, longitude;
	private String image_url_one, image_url_two;
	private int category_id;
	private int position = 0;

	MultiSelectionSpinner spinner;
	AppLocationService appLocationService;
	private String store_recommend_id;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_add_store_recommendation);

		appLocationService = new AppLocationService(activity);
		store_recommend_id = Utils.autoCreateIdModel(user_id);
		initView();
		initButton();
		ImageView iv_back = (ImageView) findViewById(R.id.iv_back);
		iv_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
		DatabaseHandler db = new DatabaseHandler(activity);
		ArrayList<CategoryProductModel> category_list = (ArrayList<CategoryProductModel>) db
				.getAllCategoryProduct();
		String[] array = new String[category_list.size()];
		//array [0]="-choose category-";
		for (int i = 0; i < category_list.size(); i++) {
			array[i] = category_list.get(i).get_category_product_name();
		}

		spinner = (MultiSelectionSpinner) findViewById(R.id.mySpinner1);
		spinner.setCategory_list(category_list);
		if(array.length > 0) {
			spinner.setItems(array);			
		}
//		spinner.setPrompt("choose status");
		// String s = spinner.getSelectedItemsAsID();

	}

	public void initView() {

	}

	public void initButton() {
		ivCamera = (ImageView) findViewById(R.id.ivCamera);
		ivCamera.setOnClickListener(this);
		ivCamera2 = (ImageView) findViewById(R.id.ivCamera2);
		ivCamera2.setOnClickListener(this);
		((ImageView) findViewById(R.id.ivMarker)).setOnClickListener(this);
		((Button) findViewById(R.id.btnSubmit)).setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.ivCamera:
			if (UtilsCamera.isDeviceSupportCamera(activity)) {
				captureImage(Commons.FROM_CAMERA_ONE);
			} else {
				Utils.showToast(activity, "This device not support camera");
			}
			break;
		case R.id.ivCamera2:
			if (UtilsCamera.isDeviceSupportCamera(activity)) {
				captureImage(Commons.FROM_CAMERA_TWO);
			} else {
				Utils.showToast(activity, "This device not support camera");
			}

			break;
		case R.id.ivMarker:

			AlertDialog.Builder builder = new AlertDialog.Builder(
					v.getContext());
			builder.setTitle(R.string.confirmation);
			builder.setMessage(R.string.check_in_confirmation);
			builder.setPositiveButton(R.string.gps,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							checkProvider(true, true);
						}
					});
			builder.setNegativeButton(R.string.network,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {

							checkProvider(false, false);
						}
					});
			AlertDialog alert = builder.create();
			alert.show();

			break;
		case R.id.btnSubmit:

			StoreRecommendationModel model = new StoreRecommendationModel();
			model.set_store_recommendation_id(store_recommend_id/*
																 * Utils.
																 * autoCreateIdModel
																 * (user_id)
																 */);
			model.set_store_name(((EditText) findViewById(R.id.edtStoreName))
					.getText().toString());
			model.set_store_owner(((EditText) findViewById(R.id.edtStoreOwner))
					.getText().toString());
			model.set_store_address(((EditText) findViewById(R.id.edtStoreAddress))
					.getText().toString());
			model.set_contact(((EditText) findViewById(R.id.edtContact))
					.getText().toString());
			model.set_image(image_url_one);
			model.set_image2(image_url_two);
			model.set_description(((EditText) findViewById(R.id.edtStoreNotes))
					.getText().toString());
			model.set_latitude(latitude);
			model.set_longtitude(longitude);
			model.set_category_id(spinner.getSelectedItemsAsID());
			model.set_user_id(user_id);
			model.set_status(1);
			String date = Utils.getTimeNow();
			model.set_date_recommendation(date);
			model.set_user_change(user_id);
			model.set_date_change(date);
			model.set_stsrc("A");
			String error = validationForm(model).toString();
			if (error.length() > 0) {
				Toast.makeText(activity, "" + error, Toast.LENGTH_SHORT).show();
				return;
			}
			DatabaseHandler db = new DatabaseHandler(activity);
			db.addStoreRecommendation(model);
			 setResult(RESULT_OK);
			 finish();
			break;
		default:
			break;
		}
	}

	private void checkProvider(boolean isCheckOut, boolean isGPS) {
		if (isGPS) {
			if (Utils.isGPSEnable(activity)) {
				gpsTracker = new GPSTracker(this);
				if (gpsTracker.canGetLocation()) {
					latitude = String.valueOf(gpsTracker.latitude);
					longitude = String.valueOf(gpsTracker.longitude);
					((ImageView) findViewById(R.id.ivMarker))
							.setImageDrawable(getResources().getDrawable(
									R.drawable.ic_on_marker));
				} else {
					gpsTracker.turnOnGps();
					Toast.makeText(getApplicationContext(),
							"please turn on your gps.", Toast.LENGTH_SHORT)
							.show();
					((ImageView) findViewById(R.id.ivMarker))
							.setImageDrawable(getResources().getDrawable(
									R.drawable.ic_off_marker));

				}

				return;
			}
		} else {
			if (Utils.isNetworkEnable(activity)) {
				Location nwLocation = appLocationService
						.getLocation(LocationManager.NETWORK_PROVIDER);

				if (nwLocation != null) {
					double latitude = nwLocation.getLatitude();
					double longitude = nwLocation.getLongitude();
				} else {
				}
				return;
			}
		}

		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setTitle(R.string.confirmation);
		builder.setMessage(isGPS ? R.string.gps_not_active_alert
				: R.string.network_not_active_alert);
		builder.setPositiveButton(R.string.gps,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						Intent callGPSSettingIntent = new Intent(
								android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
						activity.startActivity(callGPSSettingIntent);
					}
				});
		builder.setNegativeButton(R.string.cancel, null);
		AlertDialog alert = builder.create();
		alert.show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Commons.RESULT_OK_FRAGMENT) {
			if (requestCode == Commons.FROM_CAMERA_ONE) {
				Utils.loadImageAqueryAutoRotate(activity, ivCamera,
						fileUri.getPath(), Utils.getDisplayWidth(activity));
				image_url_one = fileUri.getPath();
			} else if (requestCode == Commons.FROM_CAMERA_TWO) {
				Utils.loadImageAqueryAutoRotate(activity, ivCamera2,
						fileUri.getPath(),
						Utils.getDisplayWidth(getApplicationContext()));
				image_url_two = fileUri.getPath();

			}

		}
	}

	private void captureImage(int from_camera) {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String formattedDate = df.format(new Date());

		String date = new SimpleDateFormat("yyyyMMddHHmmss",
				Locale.getDefault()).format(new Date());
		String filename = user_id + '_' + date + "_" + from_camera;

		fileUri = UtilsCamera.getOutputMediaFileUri(
				UtilsCamera.MEDIA_TYPE_IMAGE, activity, filename);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
		startActivityForResult(intent, from_camera);

	}

	private class InsertStoreRegistration extends
			AsyncTask<String, Void, HashMap<Object, Object>> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected HashMap<Object, Object> doInBackground(String... params) {
			// Creating service handler class instance
			DatabaseHandler db = new DatabaseHandler(getApplicationContext());
			SessionManagement session = new SessionManagement(
					getApplicationContext());
			HashMap<String, String> user = session.getUserDetails();
			user = session.getUserDetails();

			List<StoreRecommendationModel> sycnstoreregistration = db
					.getDataSycnStoreRegistration();
			String[] key_string = { "before_photo", "after_photo" };

			Bundle parse_bundle = new Bundle();
			parse_bundle.putStringArray("key_string", key_string);
			parse_bundle.putString("type_file", ".png");

			ArrayList<HashMap<String, String>> store_recommendation_list = new ArrayList<HashMap<String, String>>();
			HashMap<String, String> store_recommendation = new HashMap<String, String>();
			HashMap<Object, Object> result = null;
			for (StoreRecommendationModel cn : sycnstoreregistration) {

				store_recommendation.put("store_recommendation_id[]",
						cn.get_store_recommendation_id());
				store_recommendation.put("store_name[]", cn.get_store_name());
				store_recommendation.put("store_owner[]", cn.get_store_owner());
				store_recommendation.put("store_address[]",
						cn.get_store_address());
				store_recommendation.put("contact[]", cn.get_contact());
				store_recommendation.put("description[]", cn.get_description());
				store_recommendation.put("user_id[]", cn.get_user_id());
				store_recommendation.put("date_recommendation[]",
						cn.get_date_recommendation());
				store_recommendation.put("longtitude[]", cn.get_longtitude());
				store_recommendation.put("latitude[]", cn.get_latitude());
				store_recommendation.put("category_product_id[]",
						cn.get_category_id());
				store_recommendation.put("user_change[]", cn.get_user_change());
				store_recommendation.put("date_change[]", cn.get_date_change());
				store_recommendation.put("stsrc[]", cn.get_stsrc());
				store_recommendation_list.add(store_recommendation);
				setImageURL(cn, parse_bundle);
				result = doApiNew(true, parse_bundle, store_recommendation);
			}
			return result;
		}

		@Override
		protected void onPostExecute(HashMap<Object, Object> msg) {
			Log.d("Status :", "Success" + msg);

		}

		public HashMap<Object, Object> doApiNew(boolean hasMultiEntity,
				Bundle parse_bundle, HashMap<String, String> parameters) {

			HashMap<Object, Object> result = new HashMap<Object, Object>();
			try {
				HttpResponse response;
				HttpClient myClient = new DefaultHttpClient();				
				String url;
				HttpPost myConnection = new HttpPost(Commons.ProductionHttp
						+ APIParam.getAPIUrl(APIParam.API_005));

				MultipartEntity mpEntity = new MultipartEntity(
						HttpMultipartMode.BROWSER_COMPATIBLE);
				// List<NameValuePair> nameValuePairs = new
				// ArrayList<NameValuePair>(2);
				for (Entry<String, String> entry : parameters.entrySet()) {
					String key = entry.getKey();
					String value = entry.getValue();
					Log.i("AAA", "key : " + key);
					Log.i("AAA", "value : " + value);
					Charset chars = Charset.forName("UTF-8");
					mpEntity.addPart(key, new StringBody(value, chars));

				}

				// TODO BUAT MULTIPART ENTITY
				if (hasMultiEntity) {
					Log.i("TAG", "MASUK ME");
					String[] key_string = parse_bundle
							.getStringArray("key_string");
					String file_type = parse_bundle.getString("type_file");

					Log.i("TAG", "file_type : " + file_type);
					if (file_type.equalsIgnoreCase(".png")) {
						Log.i("TAG", "cuma gambar");
						for (int i = 0; i < key_string.length; i++) {
							Log.i("TAG", "arrkey : " + key_string[i]);
							if (key_string[i].toString().length() > 0) {
								byte[] bytearr = parse_bundle
										.getByteArray(key_string[i]);
								Log.i("AAA", "bytearr : " + bytearr);
								if (bytearr != null) {
									mpEntity.addPart(
											key_string[i],
											new ByteArrayBody(
													parse_bundle
															.getByteArray(key_string[i]),
													key_string[i] + file_type));
								}
							}
						}
					} else {

						Log.i("AAA", "key_string[0] : " + key_string[0]);
						Log.i("TAG",
								"key0 : "
										+ parse_bundle
												.getStringArray(key_string[0])[0]);
						Log.i("TAG",
								"path0 : "
										+ parse_bundle
												.getStringArray(key_string[0])[1]);
						Log.i("TAG",
								"mimetype0 : "
										+ parse_bundle
												.getStringArray(key_string[0])[2]);
						for (int i = 0; i < key_string.length; i++) {
							Log.i("TAG", "arrkey : " + key_string[i]);
							if (key_string[i].toString().length() > 0) {
								File file = new File(
										parse_bundle
												.getStringArray(key_string[i])[1]);
								FileBody fileBody = new FileBody(
										file,
										parse_bundle
												.getStringArray(key_string[i])[2]);

								mpEntity.addPart(parse_bundle
										.getStringArray(key_string[i])[0],
										fileBody);
							}
						}
					}
				}
				myConnection.setEntity(mpEntity);
				try {
					response = myClient.execute(myConnection);
					String JSONString = EntityUtils.toString(
							response.getEntity(), "UTF-8");
					Log.i("TAG", "jsonstring : " + JSONString);

				} catch (ClientProtocolException e) {
					result.put("result", Commons.ERROR);
					e.printStackTrace();
				} catch (IOException e) {
					result.put("result", Commons.ERROR);
					e.printStackTrace();
				}
			} catch (Exception e) {
				result.put("result", Commons.ERROR);
				e.printStackTrace();
			}
			Log.i("TAG", "result balikan : " + result);
			return result;

		}
	}

	public void setImageURL(StoreRecommendationModel model, Bundle parse_bundle) {
		Bitmap before_photo = null;
		Bitmap after_photo = null;

		String before_photo_url = model.get_image();
		String after_photo_url = model.get_image2();
		if (before_photo_url != null) {
			before_photo = processingPhotoFromCamera(before_photo_url);
			ByteArrayOutputStream baops = new ByteArrayOutputStream();
			before_photo.compress(Bitmap.CompressFormat.JPEG, 100, baops);
			parse_bundle.putByteArray("before_photo", baops.toByteArray());
			File f = new File(before_photo_url);
			parse_bundle.putString("before_photo[]"/* + "0" */, f.getName());
		}

		if (after_photo_url != null) {
			after_photo = processingPhotoFromCamera(after_photo_url);
			ByteArrayOutputStream baops = new ByteArrayOutputStream();
			after_photo.compress(Bitmap.CompressFormat.JPEG, 100, baops);
			parse_bundle.putByteArray("after_photo", baops.toByteArray());
			File f = new File(after_photo_url);
			parse_bundle.putString("after_photo[]"/* + "1" */, f.getName());
		}
	}

	public void setImageURL2(ActivitiesModel model, Bundle parse_bundle) {
		Bitmap before_photo = null;
		Bitmap after_photo = null;

		String before_photo_url = model.get_before_photo_store();
		String after_photo_url = model.get_after_photo_store();
		if (before_photo_url != null) {
			before_photo = processingPhotoFromCamera(before_photo_url);
			ByteArrayOutputStream baops = new ByteArrayOutputStream();
			before_photo.compress(Bitmap.CompressFormat.JPEG, 100, baops);
			parse_bundle.putByteArray("before_photo_store", baops.toByteArray());
			File f = new File(before_photo_url);
			parse_bundle.putString("before_photo_store[]"/* + "0" */, f.getName());
		}

		if (after_photo_url != null) {
			after_photo = processingPhotoFromCamera(after_photo_url);
			ByteArrayOutputStream baops = new ByteArrayOutputStream();
			after_photo.compress(Bitmap.CompressFormat.JPEG, 100, baops);
			parse_bundle.putByteArray("after_photo_store", baops.toByteArray());
			File f = new File(after_photo_url);
			parse_bundle.putString("after_photo_store[]"/* + "1" */, f.getName());
		}
	}

	private Bitmap processingPhotoFromCamera(String imagepath) {
		
		Bitmap bmp = Utils.compressImage(imagepath);
		ExifInterface ei;
		int orientation = 0;
		try {
			ei = new ExifInterface(imagepath);
			orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_NORMAL);
		} catch (IOException e) {
			e.printStackTrace();
		}

		switch (orientation) {
		case ExifInterface.ORIENTATION_ROTATE_90:
			bmp = RotateBitmap(bmp, 90);
			break;
		case ExifInterface.ORIENTATION_ROTATE_180:
			bmp = RotateBitmap(bmp, 180);
			break;
		}
		return bmp;
	}

	public Bitmap RotateBitmap(Bitmap source, float angle) {
		Matrix matrix = new Matrix();
		matrix.postRotate(angle);
		return Bitmap.createBitmap(source, 0, 0, source.getWidth(),
				source.getHeight(), matrix, true);
	}

	public Bitmap scaleDownBitmap(Bitmap photo) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		photo.compress(Bitmap.CompressFormat.JPEG, 100, stream);
		byte[] byteArray = stream.toByteArray();

		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 2;
		options.inPurgeable = true;
		options.inInputShareable = true;
		options.inJustDecodeBounds = false;
		options.inPreferredConfig = Bitmap.Config.ARGB_8888;
		photo = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length,
				options);

		return photo;
	}


	public String validationForm(StoreRecommendationModel model) {
		if (model.get_store_name() == null||model.get_store_name().length()==0)
			return "Store name not null";
		else if (model.get_store_owner() == null||model.get_store_owner().length()==0)
			return "Owner name not null";
		else if (model.get_store_address() == null||model.get_store_address().length()==0)
			return "Store address not null";
		else if (model.get_category_id() == null||model.get_category_id().length()==0)
			return "category id not null";
		else if (model.get_contact() == null||model.get_contact().length()==0)
			return "Contact not null";
		else if (model.get_image() == null)
			return "image 1 not null";
		else if (model.get_image2() == null)
			return "image 2 not null";
		else if (model.get_description() == null||model.get_description().length()==0) {
			return "Description not null";
		} else {
			return "";
		}

	}
}