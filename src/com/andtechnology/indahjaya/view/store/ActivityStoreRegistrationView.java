package com.andtechnology.indahjaya.view.store;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.andtechnology.indahjaya.R;
import com.andtechnology.indahjaya.database.DatabaseHandler;
import com.andtechnology.indahjaya.lib.util.Utils;
import com.andtechnology.indahjaya.model.StoreRecommendationModel;
import com.andtechnology.indahjaya.view.framework.FragmentActivityFramework;


public class ActivityStoreRegistrationView extends FragmentActivityFramework {
	private static final String IMAGE_DIRECTORY_NAME = "Indah_Jaya_Camera";
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_store_registration_view);
		Intent i = getIntent();
		
		StoreRecommendationModel model_store = getIntent().getExtras().getParcelable("model_store");
		TextView tv_StoreName = (TextView) findViewById(R.id.tv_StoreName);
		TextView tv_StoreOwner = (TextView) findViewById(R.id.tv_StoreOwner);
		TextView tv_StoreAddress = (TextView) findViewById(R.id.tv_StoreAddress);
		TextView tv_Contact = (TextView) findViewById(R.id.tv_Contact);
		TextView tv_StoreNotes = (TextView) findViewById(R.id.tv_StoreNotes);	
		ImageView ivCamera = (ImageView) findViewById(R.id.ivCamera);		
		ImageView ivCamera2 = (ImageView) findViewById(R.id.ivCamera2);
		DatabaseHandler db = new DatabaseHandler(getApplicationContext());

		List<StoreRecommendationModel> storeregistration = db.getDataStoreRecommendation(i.getStringExtra("getId"));	

		for (StoreRecommendationModel cn : storeregistration) {
			tv_StoreName.setText(cn.get_store_name());
			tv_StoreOwner.setText(cn.get_store_owner());
			tv_StoreAddress.setText(cn.get_store_address());
			tv_Contact.setText(cn.get_contact());
			tv_StoreNotes.setText(cn.get_description());
			Utils.loadImageAqueryAutoRotate(activity, ivCamera,
					cn.get_image(), Utils.getDisplayWidth(activity));
			Utils.loadImageAqueryAutoRotate(activity, ivCamera2,
					cn.get_image2(), Utils.getDisplayWidth(activity));

		}

		ImageView iv_back = (ImageView) findViewById(R.id.iv_back);
		iv_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}
}

