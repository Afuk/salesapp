package com.andtechnology.indahjaya.view.store;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.andtechnology.indahjaya.R;
import com.andtechnology.indahjaya.database.DatabaseHandler;
import com.andtechnology.indahjaya.lib.util.SessionManagement;
import com.andtechnology.indahjaya.lib.util.Utils;
import com.andtechnology.indahjaya.lib.util.UtilsCamera;
import com.andtechnology.indahjaya.utils.Commons;

public class AddStoreRecommendationActivity extends Activity implements
		OnClickListener {
	private Uri fileUri = null;
	private ImageView ivCamera2, ivCamera;
	String last_id = "";
	int proggress_status;
	String user;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_add_store_recommendation);
		initButton();
		initialLast_id();
	}

	public void initButton() {
		ivCamera = (ImageView) findViewById(R.id.ivCamera);
		ivCamera.setOnClickListener(this);
		ivCamera2 = (ImageView) findViewById(R.id.ivCamera2);
		ivCamera2.setOnClickListener(this);
		((ImageView) findViewById(R.id.iv_back)).setOnClickListener(this);
		((Button) findViewById(R.id.btnSubmit)).setOnClickListener(this);

	}

	public void initialLast_id() {
		final SessionManagement session = new SessionManagement(
				getApplicationContext());
		final HashMap<String, String> user = session.getUserDetails();
		this.user = user.get(session.USER_ID);

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String formattedDate = df.format(new Date());

		last_id = new SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
				.format(new Date());
		last_id += user.get(session.USER_ID);
	}

	private void captureImage(int from_camera) {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		SessionManagement session = new com.andtechnology.indahjaya.lib.util.SessionManagement(
				AddStoreRecommendationActivity.this);
		HashMap<String, String> user = session.getUserDetails();
		String UserId = user.get(session.USER_ID);

		SimpleDateFormat dx = new SimpleDateFormat("yyyyMMdd");
		String image_id = UserId + '_' + (dx.format(new Date()));

		// add calender for renaming file photo
		Calendar calendar = Calendar.getInstance();
		String hour = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
		String minute = String.valueOf(calendar.get(Calendar.MINUTE));
		String second = String.valueOf(calendar.get(Calendar.SECOND));

		fileUri = UtilsCamera.getOutputMediaFileUri(
				UtilsCamera.MEDIA_TYPE_IMAGE,
				AddStoreRecommendationActivity.this, hour + "-" + minute + "-"
						+ second + "_" + image_id + "_" + from_camera);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
		startActivityForResult(intent, from_camera);

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.ivCamera:
			if (UtilsCamera.isDeviceSupportCamera(AddStoreRecommendationActivity.this)) {
				captureImage(Commons.FROM_CAMERA_ONE);
			} else {
				Utils.showToast(AddStoreRecommendationActivity.this, "This device not support camera");
			}
			break;
		case R.id.ivCamera2:
			if (UtilsCamera.isDeviceSupportCamera(AddStoreRecommendationActivity.this)) {
				captureImage(Commons.FROM_CAMERA_TWO);
			} else {
				Utils.showToast(AddStoreRecommendationActivity.this, "This device not support camera");
			}

			break;
		case R.id.iv_back:
			finish();

			break;
	
			
		case R.id.btnSubmit:
			// TODO Auto-generated method stub
			
			if (((EditText) findViewById(R.id.et_store_name)).getText().equals("") 
					|| ((EditText) findViewById(R.id.et_store_owner)).getText().equals("")
					|| ((EditText) findViewById(R.id.et_store_address)).getText().equals("")
					|| ((EditText) findViewById(R.id.et_store_notes)).getText().equals(""))					
					 {
				Utils.showToast(AddStoreRecommendationActivity.this, "All Field Must be Filled");
				return;
			}
			DatabaseHandler db = new DatabaseHandler(
					getApplicationContext());

			 proggress_status = 2;
			Utils.showToast(AddStoreRecommendationActivity.this, "Insert Success");
			
			
			
			setResult(RESULT_OK);
			finish();
			break;
		default:
			break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Commons.RESULT_OK_FRAGMENT) {
			if (requestCode == Commons.FROM_CAMERA_ONE) {
				Utils.loadImageAqueryAutoRotate(
						AddStoreRecommendationActivity.this, ivCamera,
						fileUri.getPath(),
						Utils.getDisplayWidth(getApplicationContext()));
			} else if (requestCode == Commons.FROM_CAMERA_TWO) {
				Utils.loadImageAqueryAutoRotate(
						AddStoreRecommendationActivity.this, ivCamera2,
						fileUri.getPath(),
						Utils.getDisplayWidth(getApplicationContext()));
			}
		}
	}

}
