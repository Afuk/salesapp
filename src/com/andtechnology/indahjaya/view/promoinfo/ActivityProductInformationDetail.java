package com.andtechnology.indahjaya.view.promoinfo;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.andtechnology.indahjaya.R;
import com.andtechnology.indahjaya.customview.ResizeableImageView;
import com.andtechnology.indahjaya.lib.util.Utils;
import com.andtechnology.indahjaya.restful.ConstantPATH;
import com.andtechnology.indahjaya.restful.ConstantREST;
import com.andtechnology.indahjaya.view.framework.ActivityFramework;

public class ActivityProductInformationDetail extends ActivityFramework {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_product_information_detail);

		Intent i = getIntent();
		TextView title_text = (TextView) findViewById(R.id.title_text);
		TextView tv_product_name = (TextView) findViewById(R.id.tv_product_name);
		TextView tv_description = (TextView) findViewById(R.id.tv_description);
		ResizeableImageView iv_product = (ResizeableImageView) findViewById(R.id.iv_product);

		SimpleDateFormat dfg = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
		Calendar c = Calendar.getInstance();
		try {
			c.setTime(dfg.parse(i.getStringExtra("getStart_date")));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Calendar d = Calendar.getInstance();
		try {
			d.setTime(dfg.parse(i.getStringExtra("getEnd_date")));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		title_text.setText(i.getStringExtra("getTitle"));
		tv_product_name.setText(i.getStringExtra("getTitle")
				+ "\nValid date : " + df.format(c.getTime()) + " - "
				+ df.format(d.getTime()));
		tv_description
				.setText(Html.fromHtml(i.getStringExtra("getDescription")));
		tv_description.setMovementMethod(LinkMovementMethod.getInstance());
		String images = i.getStringExtra("getImage_name");
		//iv_product.setLayoutParams(new LinearLayout.LayoutParams(Utils.getDisplayWidth(activity)/2, LayoutParams.WRAP_CONTENT));
		Utils.loadImageUsingAquery(activity, iv_product, ConstantREST.ImagePath + images, Utils.getDisplayWidth(activity));
		/*AQuery aQuery = new AQuery(activity);
		aQuery.id(iv_product).image(ConstantREST.ImagePath + images, true,
				true, Utils.getDisplayWidth(activity), 0);*/

		ImageView iv_back = (ImageView) findViewById(R.id.iv_back);
		iv_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				setResult(RESULT_OK);
				finish();
			}
		});
	}
}
