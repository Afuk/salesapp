package com.andtechnology.indahjaya.view;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.andtechnology.indahjaya.database.DatabaseHandler;
import com.andtechnology.indahjaya.lib.util.SessionManagement;
import com.andtechnology.indahjaya.lib.util.UtilsPointer;
import com.andtechnology.indahjaya.model.LastUpdateModel;
import com.andtechnology.indahjaya.service.MyService;
import com.andtechnology.indahjaya.view.framework.ActivityFramework;

public class LogOut extends ActivityFramework {
	private TextView signup, forgot, login;
	SessionManagement session;
	Map<Integer, Integer> LanguageCode = new HashMap<Integer, Integer>();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);// CHECK

		session = new SessionManagement(getBaseContext());
		HashMap<String, String> user = session.getUserDetails();
		stopService(new Intent(getBaseContext(), MyService.class));
		session.clearLogin();			
		
		UtilsPointer.goToLogin(activity);
		finish();

	}
}
