package com.andtechnology.indahjaya.view.setting;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.andtechnology.indahjaya.adapter.NavDrawerMenuAdapter;
import com.andtechnology.indahjaya.lib.util.UtilsPointer;
import com.andtechnology.indahjaya.model.NavDrawerMenuModel;
import com.andtechnology.indahjaya.view.framework.ActivityFramework;
import com.andtechnology.indahjaya.R;

public class ActivitySetting extends ActivityFramework {
	android.support.v4.app.Fragment fragment = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting);

		Intent i = getIntent();
		TextView title_text = (TextView) findViewById(R.id.title_text);
		title_text.setText(i.getStringExtra("getTitle"));
		
		ArrayList<NavDrawerMenuModel> navDrawerMenu = new ArrayList<NavDrawerMenuModel>();

		navDrawerMenu = new ArrayList<NavDrawerMenuModel>();
		ListView mDrawerList = (ListView) findViewById(R.id.lv_setting);

		NavDrawerMenuModel model;

		model = new NavDrawerMenuModel();
		model.setTitle("Terms & Condition");
		model.setId(0001);
		model.setCount("");
		model.setIcon(R.drawable.ic_terms_condition);
		navDrawerMenu.add(model);


		model = new NavDrawerMenuModel();
		model.setTitle("About Indah Jaya");
		model.setId(0002);
		model.setCount("");
		model.setIcon(R.drawable.ic_info);
		navDrawerMenu.add(model);


		model = new NavDrawerMenuModel();
		model.setTitle("Contact Us");
		model.setId(0003);
		model.setCount("");
		model.setIcon(R.drawable.ic_contact_us);
		navDrawerMenu.add(model);

		model = new NavDrawerMenuModel();
		model.setTitle("Change Password");
		model.setIcon(R.drawable.ic_change_password);
		model.setId(4);
		model.setCount("");
		navDrawerMenu.add(model);

		// setting the nav drawer list adapter
		NavDrawerMenuAdapter adapter = new NavDrawerMenuAdapter(activity, navDrawerMenu);
		mDrawerList.setAdapter(adapter);

		mDrawerList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				NavDrawerMenuModel model = (NavDrawerMenuModel) parent
						.getAdapter().getItem(position);
				Intent i = null;
				switch (model.getId()) {
				case 4:
					UtilsPointer.goToChangePassword(activity);
					break;
				default:
					// i = new Intent(v.getContext(), ActivityMenu.class);
					break;
				}
				
				if (i == null) {
//					Utils.showToast(activity, "Comming Soon");
				} else {
					i.putExtra("getTitle", model.getTitle());
					startActivityForResult(i, 900);
				}
//				Intent i = new Intent(v.getContext(), ActivityProductInformationDetail.class);
//				i.putExtra("getTitle", model.getTitle());
//				startActivityForResult(i, 904);

			}
		});
		


		ImageView iv_back = (ImageView) findViewById(R.id.iv_back);
		iv_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}
}
