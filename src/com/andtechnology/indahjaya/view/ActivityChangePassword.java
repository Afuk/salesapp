package com.andtechnology.indahjaya.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.andtechnology.indahjaya.R;
import com.andtechnology.indahjaya.lib.util.JSONParser;
import com.andtechnology.indahjaya.lib.util.SessionManagement;
import com.andtechnology.indahjaya.lib.util.Utils;
import com.andtechnology.indahjaya.restful.ConstantREST;
import com.andtechnology.indahjaya.view.framework.ActivityFramework;

public class ActivityChangePassword extends ActivityFramework{
	private static String url = ConstantREST.API16;
	ProgressDialog pDialog;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_change_password);


		ImageView iv_back = (ImageView) findViewById(R.id.iv_back);
		iv_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});

		TextView btn_submit = (TextView) findViewById(R.id.btn_submit);
		btn_submit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (Utils.isOnline(activity)) {
					EditText new_password = (EditText) findViewById(R.id.et_new_password);
					EditText btn_password = (EditText) findViewById(R.id.et_old_password);

					if (new_password.getText().toString().equals("") || btn_password.getText().toString().equals("")) {
						Utils.showToast(activity,"All field must be required!");
					} else {
						if (Utils.isOnline(activity)) {
							new ChangePassword().execute();
						}
					}
				} else {
					Utils.showToast(activity, getString(R.string.internet_failed_title));
				}
				
				
				// TODO Auto-generated method stub
				
			}
		});
	}


	/**
	 * Async task class to get json by making HTTP call
	 * */
	private class ChangePassword extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog
			pDialog = new ProgressDialog(activity);
			pDialog.setMessage("Please wait...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			// Creating service handler class instance
			EditText new_password = (EditText) findViewById(R.id.et_new_password);
			EditText old_password = (EditText) findViewById(R.id.et_old_password);
			HashMap<String, String> user = session.getUserDetails();

			List<NameValuePair> pairs = new ArrayList<NameValuePair>();
			pairs.add(new BasicNameValuePair("user_id", user.get(session.USER_ID)));
			pairs.add(new BasicNameValuePair("old_password", old_password.getText().toString()));
			pairs.add(new BasicNameValuePair("new_password", new_password.getText().toString()));

			JSONParser jp = new JSONParser();
			return jp.getJSONFromUrl(url, pairs);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			try {
				JSONObject obj = new JSONObject(result);
				String status = obj.getString("status");
				String message = obj.getString("message");
				
				if(status.equals("success")) {
					Utils.showToast(activity, message);
					finish();
				} else {
					Utils.showToast(activity, message);					
				}

				pDialog.dismiss();

			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}
	}

}
