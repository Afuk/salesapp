package com.andtechnology.indahjaya.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.andtechnology.indahjaya.R;
import com.andtechnology.indahjaya.database.DatabaseHandler;
import com.andtechnology.indahjaya.lib.util.JSONParser;
import com.andtechnology.indahjaya.lib.util.SessionManagement;
import com.andtechnology.indahjaya.lib.util.Utils;
import com.andtechnology.indahjaya.model.ActivitiesModel;
import com.andtechnology.indahjaya.model.MarketSurveyModel;
import com.andtechnology.indahjaya.model.StoreRecommendationModel;
import com.andtechnology.indahjaya.restful.ConstantREST;
import com.andtechnology.indahjaya.ui.service.APIParam;
import com.andtechnology.indahjaya.utils.Commons;
import com.andtechnology.indahjaya.view.SplashScreen;

@SuppressLint("NewApi")
public class MyService extends Service {
	private static int SPLASH_TIME_OUT = 20000;
	private static Timer timer = new Timer();
	private Context ctx;
	int CheckMessage = 0;
	SessionManagement session;
	HashMap<String, String> user;

	public IBinder onBind(Intent arg0) {
		return null;
	}

	public void onCreate() {
		super.onCreate();
		ctx = this;
		startService();
	}

	private void startService() {
		// timer.cancel();
		session = new SessionManagement(ctx);
		user = session.getUserDetails();
		DatabaseHandler db = new DatabaseHandler(getApplicationContext());
		try {
			storeregistrationprogress();
			activitiesprogress();
			marketintelligenceprogress();
			messsage();
		} catch (Exception e) {
			// TODO: handle exception
			Log.d("error : ", e.toString());
		}
	}

	private class mainTask extends TimerTask {
		public void run() {
			toastHandler.sendEmptyMessage(0);
		}
	}

	public void onDestroy() {
		super.onDestroy();
		// Toast.makeText(this, "Service Stopped ...",
		// Toast.LENGTH_SHORT).show();
	}

	private final Handler toastHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if (Utils.isOnline(ctx)) {
				DatabaseHandler db = new DatabaseHandler(
						getApplicationContext());
				if (db.getMessageCount() == 0) {
					new CountMessage().execute();
				} else {
					// timer.cancel();
				}

			}
		}
	};

	public void storeregistrationprogress() {

		if (Utils.isOnline(getApplicationContext())) {
			Log.d("log : ", "InsertStoreRegistration reload");
			DatabaseHandler db = new DatabaseHandler(getApplicationContext());
			List<StoreRecommendationModel> store = db
					.getDataSycnStoreRegistration();
			if (store.size() != 0) {
				new InsertStoreRegistration().execute();
			} else {
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						Log.d("log : ", "InsertStoreRegistration no have work");
						storeregistrationprogress();
					}
				}, SPLASH_TIME_OUT);
			}
		} else {
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					Log.d("log : ", "InsertStoreRegistration no have work");
					storeregistrationprogress();
				}
			}, SPLASH_TIME_OUT);

		}
	}

	public void activitiesprogress() {
		if (Utils.isOnline(getApplicationContext())) {
			Log.d("log : ", "InsertActivities reload");
			DatabaseHandler db = new DatabaseHandler(getApplicationContext());
			List<ActivitiesModel> activities = db.getDataSycnActivities();
			if (activities.size() != 0) {
				new InsertActivities().execute();
			} else {
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						Log.d("log : ", "InsertActivities no have connection");
						activitiesprogress();
					}
				}, SPLASH_TIME_OUT);
			}
		} else {
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					Log.d("log : ", "InsertActivities no have connection");
					activitiesprogress();
				}
			}, SPLASH_TIME_OUT);

		}
	}

	public void marketintelligenceprogress() {
		if (Utils.isOnline(getApplicationContext())) {
			Log.d("log : ", "InsertMarketIntelligence reload");
			DatabaseHandler db = new DatabaseHandler(getApplicationContext());
			List<MarketSurveyModel> activities = db.getDataSycnMarketSurvey();
			if (activities.size() != 0) {
				new InsertMarketIntelligence().execute();
			} else {
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						Log.d("log : ",
								"InsertMarketIntelligence no have connection");
						marketintelligenceprogress();
					}
				}, SPLASH_TIME_OUT);
			}
		} else {
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					Log.d("log : ",
							"InsertMarketIntelligence no have connection");
					marketintelligenceprogress();
				}
			}, SPLASH_TIME_OUT);

		}
	}

	public void messsage() {
		Log.d("log : ", "Message reload");
		DatabaseHandler db = new DatabaseHandler(getApplicationContext());

		if (db.getMessageCount() == 0) {
			new CountMessage().execute();
		} else {
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					Log.d("log : ", " Message Have Message Please read");
					messsage();
				}
			}, SPLASH_TIME_OUT);
		}
	}

	private class CountMessage extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) {
			// Creating service handler class instance
			List<NameValuePair> pairs = new ArrayList<NameValuePair>();
			pairs.add(new BasicNameValuePair("user_id", user
					.get(session.USER_ID)));

			JSONParser jp = new JSONParser();
			return jp.getJSONFromUrl(ConstantREST.API2, pairs);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			if (Utils.isOnline(getApplicationContext())) {
				JSONObject obj = null;
				try {
					obj = new JSONObject(result);
					Log.d("log : ",
							"Message Check count " + obj.getString("count"));
					if (obj.getString("count").equals("0")) {
						new Handler().postDelayed(new Runnable() {
							@Override
							public void run() {
								messsage();
							}
						}, SPLASH_TIME_OUT);
					} else {

						if (CheckMessage < Integer.parseInt(obj
								.getString("count"))) {
							CheckMessage = Integer.parseInt(obj
									.getString("count"));
							notification(Integer.parseInt(obj
									.getString("count")));
						}
						new Handler().postDelayed(new Runnable() {
							@Override
							public void run() {
								messsage();
							}
						}, SPLASH_TIME_OUT);
					}

				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			} else {
				messsage();
			}

		}

		public void notification(int notifyID) {
			NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
			mNotificationManager.cancelAll();
			// Sets an ID for the notification, so it can be updated

			NotificationCompat.Builder mNotifyBuilder = new NotificationCompat.Builder(
					ctx).setContentTitle("Indah Jaya Message")
					.setContentText("You've received new messages.")
					.setSmallIcon(R.drawable.ic_notif);

			Uri alarmSound = RingtoneManager
					.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			mNotifyBuilder.setSound(alarmSound);

			Intent resultIntent = new Intent(ctx, SplashScreen.class);
			resultIntent.putExtra("NOTIFICATION_FLAG", 1);
			mNotifyBuilder.setContentText("New messages").setNumber(notifyID);
			// Because the ID remains unchanged, the existing notification is
			// updated.

			TaskStackBuilder stackBuilder = TaskStackBuilder.create(ctx);
			// Adds the back stack for the Intent (but not the Intent itself)
			stackBuilder.addParentStack(SplashScreen.class);
			// Adds the Intent that starts the Activity to the top of the stack
			stackBuilder.addNextIntent(resultIntent);
			PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
					0, PendingIntent.FLAG_UPDATE_CURRENT);
			mNotifyBuilder.setContentIntent(resultPendingIntent);
			// mId allows you to update the notification later on.
			mNotificationManager.notify(notifyID, mNotifyBuilder.build());

		}
	}

	private class InsertActivities extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) {
			// Creating service handler class instance
			DatabaseHandler db = new DatabaseHandler(getApplicationContext());
			SessionManagement session = new SessionManagement(
					getApplicationContext());
			HashMap<String, String> user = session.getUserDetails();
			user = session.getUserDetails();

			List<ActivitiesModel> sycnactivities = db.getDataSycnActivities();
			String[] key_string = { "before_photo_store", "after_photo_store" };

			Bundle parse_bundle = new Bundle();
			parse_bundle.putStringArray("key_string", key_string);
			parse_bundle.putString("type_file", ".png");

			ArrayList<HashMap<String, String>> activities_list = new ArrayList<HashMap<String, String>>();
			HashMap<String, String> activities = new HashMap<String, String>();
			String result = null;
			for (ActivitiesModel cn : sycnactivities) {
				activities.put("activities_id[]", cn.get_activities_id());
				activities.put("mapping_store_id[]",
						Integer.toString(cn.get_mapping_store_id()));
				activities.put("longtitude_sales[]", cn.get_longtitude_sales());
				activities.put("latitude_sales[]", cn.get_latitude_sales());
				activities.put("date_check_in[]", cn.getDate_checkin());
				activities.put("date_check_out[]", cn.getDate_checkout());
				activities.put("user_change[]", cn.get_user_change());
				activities.put("date_change[]", cn.get_date_change());
				activities.put("stsrc[]", cn.get_stsrc());
				activities
						.put("type_activities_id[]", cn.getType_activity_id());
				activities.put("type_activities_question_id[]",
						cn.getQuestion_id());
				activities
						.put("type_activities_answer_id[]", cn.getAnswer_id());

				Log.d("activities_id : ", cn.get_activities_id());
				Log.d("mapping_store_id : ", "" + cn.get_mapping_store_id());
				Log.d("longtitude_sales : ", cn.get_longtitude_sales());
				Log.d("latitude_sales : ", cn.get_latitude_sales());
				Log.d("date_check_in : ", "" + cn.getDate_checkin());
				Log.d("date_check_out : ", "" + cn.getDate_checkout());
				Log.d("user_change : ", cn.get_user_change());
				Log.d("date_change : ", cn.get_date_change());
				Log.d("stsrc : ", cn.get_stsrc());
				Log.d("type_activities_id : ", "" + cn.getType_activity_id());
				Log.d("type_activities_question_id : ",
						"" + cn.getQuestion_id());
				Log.d("type_activities_answer_id : ", "" + cn.getAnswer_id());
				activities_list.add(activities);
				setImageURL2(cn, parse_bundle);
				result = doApiNew(true, parse_bundle, activities);
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			if (Utils.isOnline(getApplicationContext())) {
				Log.d("Status :", "Success" + result);
				JSONObject obj = null;
				try {
					obj = new JSONObject(result);
					DatabaseHandler db = new DatabaseHandler(
							getApplicationContext());
					if (obj.getString("response").equals("success")) {
						JSONArray ary = (JSONArray) obj.get("mapping_store_id");
						for (int i = 0; i < ary.length(); i++) {
							db.updateProggressMappingStore(
									Integer.parseInt(ary.get(i).toString()), 2);
						}

						JSONArray ary2 = (JSONArray) obj.get("activities_id");
						for (int z = 0; z < ary2.length(); z++) {
							db.updateImageActivities(ary2.get(z).toString());
						}
						Log.d("Log : ", "Activities Success");

						new Handler().postDelayed(new Runnable() {
							@Override
							public void run() {
								activitiesprogress();
							}
						}, SPLASH_TIME_OUT);
					} else {
						new Handler().postDelayed(new Runnable() {
							@Override
							public void run() {
								activitiesprogress();
							}
						}, SPLASH_TIME_OUT);
					}

				} catch (final JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();

					new Handler().postDelayed(new Runnable() {
						@Override
						public void run() {
							Log.d("Log : ",
									"ERROR API Activities " + e1.getMessage());
							activitiesprogress();
						}
					}, SPLASH_TIME_OUT);
				}
			} else {

				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						Log.d("Log : ", "ERROR API Activities not have network");
						activitiesprogress();
					}
				}, SPLASH_TIME_OUT);
			}

		}

		public String doApiNew(boolean hasMultiEntity, Bundle parse_bundle,
				HashMap<String, String> parameters) {

			HashMap<Object, Object> result = new HashMap<Object, Object>();
			String JSONString = null;

			try {
				HttpResponse response;
				HttpClient myClient = new DefaultHttpClient();
				String url;
				HttpPost myConnection = new HttpPost(Commons.ProductionHttp
						+ APIParam.getAPIUrl(APIParam.API_007));

				MultipartEntity mpEntity = new MultipartEntity(
						HttpMultipartMode.BROWSER_COMPATIBLE);
				// List<NameValuePair> nameValuePairs = new
				// ArrayList<NameValuePair>(2);
				for (Entry<String, String> entry : parameters.entrySet()) {
					String key = entry.getKey();
					String value = entry.getValue();
					Log.i("AAA", "key : " + key);
					Log.i("AAA", "value : " + value);
					Charset chars = Charset.forName("UTF-8");
					mpEntity.addPart(key, new StringBody(value, chars));

				}

				// TODO BUAT MULTIPART ENTITY
				if (hasMultiEntity) {
					Log.i("TAG", "MASUK ME");
					String[] key_string = parse_bundle
							.getStringArray("key_string");
					String file_type = parse_bundle.getString("type_file");

					Log.i("TAG", "file_type : " + file_type);
					if (file_type.equalsIgnoreCase(".png")) {
						Log.i("TAG", "cuma gambar");
						for (int i = 0; i < key_string.length; i++) {
							Log.i("TAG", "arrkey : " + key_string[i]);
							if (key_string[i].toString().length() > 0) {
								byte[] bytearr = parse_bundle
										.getByteArray(key_string[i]);
								Log.i("AAA", "bytearr : " + bytearr);
								if (bytearr != null) {
									mpEntity.addPart(
											key_string[i],
											new ByteArrayBody(
													parse_bundle
															.getByteArray(key_string[i]),
													key_string[i] + file_type));
								}
							}
						}
					} else {

						Log.i("AAA", "key_string[0] : " + key_string[0]);
						Log.i("TAG",
								"key0 : "
										+ parse_bundle
												.getStringArray(key_string[0])[0]);
						Log.i("TAG",
								"path0 : "
										+ parse_bundle
												.getStringArray(key_string[0])[1]);
						Log.i("TAG",
								"mimetype0 : "
										+ parse_bundle
												.getStringArray(key_string[0])[2]);
						for (int i = 0; i < key_string.length; i++) {
							Log.i("TAG", "arrkey : " + key_string[i]);
							if (key_string[i].toString().length() > 0) {
								File file = new File(
										parse_bundle
												.getStringArray(key_string[i])[1]);
								FileBody fileBody = new FileBody(
										file,
										parse_bundle
												.getStringArray(key_string[i])[2]);

								mpEntity.addPart(parse_bundle
										.getStringArray(key_string[i])[0],
										fileBody);
							}
						}
					}
				}
				myConnection.setEntity(mpEntity);
				try {
					response = myClient.execute(myConnection);
					JSONString = EntityUtils.toString(response.getEntity(),
							"UTF-8");
					Log.i("TAG", "jsonstring : " + JSONString);

				} catch (ClientProtocolException e) {
					result.put("result", Commons.ERROR);
					e.printStackTrace();
				} catch (IOException e) {
					result.put("result", Commons.ERROR);
					e.printStackTrace();
				}
			} catch (Exception e) {
				result.put("result", Commons.ERROR);
				e.printStackTrace();
			}
			Log.i("TAG", "result balikan : " + result);
			return JSONString;

		}
	}

	private class InsertStoreRegistration extends
			AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) {
			// Creating service handler class instance
			DatabaseHandler db = new DatabaseHandler(getApplicationContext());
			SessionManagement session = new SessionManagement(
					getApplicationContext());
			HashMap<String, String> user = session.getUserDetails();
			user = session.getUserDetails();

			List<StoreRecommendationModel> sycnstoreregistration = db
					.getDataSycnStoreRegistration();
			String[] key_string = { "before_photo", "after_photo" };

			Bundle parse_bundle = new Bundle();
			parse_bundle.putStringArray("key_string", key_string);
			parse_bundle.putString("type_file", ".png");

			ArrayList<HashMap<String, String>> store_recommendation_list = new ArrayList<HashMap<String, String>>();
			HashMap<String, String> store_recommendation = new HashMap<String, String>();
			/* HashMap<Object, Object> result = null; */
			String result = null;
			for (StoreRecommendationModel cn : sycnstoreregistration) {

				store_recommendation.put("store_recommendation_id[]",
						cn.get_store_recommendation_id());
				store_recommendation.put("store_name[]", cn.get_store_name());
				store_recommendation.put("store_owner[]", cn.get_store_owner());
				store_recommendation.put("store_address[]",
						cn.get_store_address());
				store_recommendation.put("contact[]", cn.get_contact());
				store_recommendation.put("description[]", cn.get_description());
				store_recommendation.put("user_id[]", cn.get_user_id());
				store_recommendation.put("date_recommendation[]",
						cn.get_date_recommendation());
				store_recommendation.put("longtitude[]", cn.get_longtitude());
				store_recommendation.put("latitude[]", cn.get_latitude());
				store_recommendation.put("category_product_id[]",
						cn.get_category_id());
				store_recommendation.put("user_change[]", cn.get_user_change());
				store_recommendation.put("date_change[]", cn.get_date_change());
				store_recommendation.put("stsrc[]", cn.get_stsrc());
				store_recommendation_list.add(store_recommendation);
				setImageURL(cn, parse_bundle);
				result = doApiNew(true, parse_bundle, store_recommendation);
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			if (Utils.isOnline(getApplicationContext())) {
				Log.d("Status :", "Success" + result);
				JSONObject obj = null;
				try {
					obj = new JSONObject(result);
					DatabaseHandler db = new DatabaseHandler(
							getApplicationContext());
					if (obj.getString("response").equals("success")) {
						JSONArray ary = (JSONArray) obj
								.get("store_recommendation_id");
						for (int i = 0; i < ary.length(); i++) {
							db.updateProggressStoreRecommendation(ary.get(i)
									.toString(), 2);
						}

						Log.d("Log : ", "New Store Success");

						new Handler().postDelayed(new Runnable() {
							@Override
							public void run() {
								storeregistrationprogress();
							}
						}, SPLASH_TIME_OUT);
					} else {
						new Handler().postDelayed(new Runnable() {
							@Override
							public void run() {
								storeregistrationprogress();
							}
						}, SPLASH_TIME_OUT);
					}

				} catch (final JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();

					new Handler().postDelayed(new Runnable() {
						@Override
						public void run() {
							Log.d("Log : ",
									"ERROR API New Store " + e1.getMessage());
							storeregistrationprogress();
						}
					}, SPLASH_TIME_OUT);
				}
			} else {
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						Log.d("Log : ", "ERROR API New Store not have network");
						storeregistrationprogress();
					}
				}, SPLASH_TIME_OUT);
			}

		}

		public String doApiNew(boolean hasMultiEntity, Bundle parse_bundle,
				HashMap<String, String> parameters) {

			HashMap<Object, Object> result = new HashMap<Object, Object>();
			String JSONString = null;

			try {
				HttpResponse response;
				HttpClient myClient = new DefaultHttpClient();
				String url;
				HttpPost myConnection = new HttpPost(Commons.ProductionHttp
						+ APIParam.getAPIUrl(APIParam.API_005));

				MultipartEntity mpEntity = new MultipartEntity(
						HttpMultipartMode.BROWSER_COMPATIBLE);
				// List<NameValuePair> nameValuePairs = new
				// ArrayList<NameValuePair>(2);
				for (Entry<String, String> entry : parameters.entrySet()) {
					String key = entry.getKey();
					String value = entry.getValue();
					Log.i("AAA", "key : " + key);
					Log.i("AAA", "value : " + value);
					Charset chars = Charset.forName("UTF-8");
					mpEntity.addPart(key, new StringBody(value, chars));

				}

				// TODO BUAT MULTIPART ENTITY
				if (hasMultiEntity) {
					Log.i("TAG", "MASUK ME");
					String[] key_string = parse_bundle
							.getStringArray("key_string");
					String file_type = parse_bundle.getString("type_file");

					Log.i("TAG", "file_type : " + file_type);
					if (file_type.equalsIgnoreCase(".png")) {
						Log.i("TAG", "cuma gambar");
						for (int i = 0; i < key_string.length; i++) {
							Log.i("TAG", "arrkey : " + key_string[i]);
							if (key_string[i].toString().length() > 0) {
								byte[] bytearr = parse_bundle
										.getByteArray(key_string[i]);
								Log.i("AAA", "bytearr : " + bytearr);
								if (bytearr != null) {
									mpEntity.addPart(
											key_string[i],
											new ByteArrayBody(
													parse_bundle
															.getByteArray(key_string[i]),
													key_string[i] + file_type));
								}
							}
						}
					} else {

						Log.i("AAA", "key_string[0] : " + key_string[0]);
						Log.i("TAG",
								"key0 : "
										+ parse_bundle
												.getStringArray(key_string[0])[0]);
						Log.i("TAG",
								"path0 : "
										+ parse_bundle
												.getStringArray(key_string[0])[1]);
						Log.i("TAG",
								"mimetype0 : "
										+ parse_bundle
												.getStringArray(key_string[0])[2]);
						for (int i = 0; i < key_string.length; i++) {
							Log.i("TAG", "arrkey : " + key_string[i]);
							if (key_string[i].toString().length() > 0) {
								File file = new File(
										parse_bundle
												.getStringArray(key_string[i])[1]);
								FileBody fileBody = new FileBody(
										file,
										parse_bundle
												.getStringArray(key_string[i])[2]);

								mpEntity.addPart(parse_bundle
										.getStringArray(key_string[i])[0],
										fileBody);
							}
						}
					}
				}
				myConnection.setEntity(mpEntity);
				try {
					response = myClient.execute(myConnection);
					JSONString = EntityUtils.toString(response.getEntity(),
							"UTF-8");
					Log.i("TAG", "jsonstring : " + JSONString);

				} catch (ClientProtocolException e) {
					result.put("result", Commons.ERROR);
					e.printStackTrace();
				} catch (IOException e) {
					result.put("result", Commons.ERROR);
					e.printStackTrace();
				}
			} catch (Exception e) {
				result.put("result", Commons.ERROR);
				e.printStackTrace();
			}
			Log.i("TAG", "result balikan : " + result);
			return JSONString;

		}
	}

	public void setImageURL(StoreRecommendationModel model, Bundle parse_bundle) {
		Bitmap before_photo = null;
		Bitmap after_photo = null;

		String before_photo_url = model.get_image();
		String after_photo_url = model.get_image2();
		if (before_photo_url != null) {
			before_photo = processingPhotoFromCamera(before_photo_url);
			ByteArrayOutputStream baops = new ByteArrayOutputStream();
			before_photo.compress(Bitmap.CompressFormat.JPEG, 100, baops);
			parse_bundle.putByteArray("before_photo", baops.toByteArray());
			File f = new File(before_photo_url);
			parse_bundle.putString("before_photo[]"/* + "0" */, f.getName());
		}

		if (after_photo_url != null) {
			after_photo = processingPhotoFromCamera(after_photo_url);
			ByteArrayOutputStream baops = new ByteArrayOutputStream();
			after_photo.compress(Bitmap.CompressFormat.JPEG, 100, baops);
			parse_bundle.putByteArray("after_photo", baops.toByteArray());
			File f = new File(after_photo_url);
			parse_bundle.putString("after_photo[]"/* + "1" */, f.getName());
		}
	}

	public void setImageURL2(ActivitiesModel model, Bundle parse_bundle) {
		Bitmap before_photo = null;
		Bitmap after_photo = null;

		String before_photo_url = model.get_before_photo_store();
		String after_photo_url = model.get_after_photo_store();
		if (before_photo_url != null) {
			before_photo = processingPhotoFromCamera(before_photo_url);
			ByteArrayOutputStream baops = new ByteArrayOutputStream();
			before_photo.compress(Bitmap.CompressFormat.JPEG, 100, baops);
			parse_bundle
					.putByteArray("before_photo_store", baops.toByteArray());
			File f = new File(before_photo_url);
			parse_bundle.putString("before_photo_store[]"/* + "0" */,
					f.getName());
		}

		if (after_photo_url != null) {
			after_photo = processingPhotoFromCamera(after_photo_url);
			ByteArrayOutputStream baops = new ByteArrayOutputStream();
			after_photo.compress(Bitmap.CompressFormat.JPEG, 100, baops);
			parse_bundle.putByteArray("after_photo_store", baops.toByteArray());
			File f = new File(after_photo_url);
			parse_bundle.putString("after_photo_store[]"/* + "1" */,
					f.getName());
		}
	}

	public void setImageURLforMarketSurvey(MarketSurveyModel model,
			Bundle parse_bundle) {
		Bitmap photo1 = null;
		Bitmap photo2 = null;
		Bitmap photo3 = null;
		Bitmap photo4 = null;
		Bitmap photo5 = null;

		String photo1_url = model.getPhoto_one();
		String photo2_url = model.getPhoto_two();
		String photo3_url = model.getPhoto_three();
		String photo4_url = model.getPhoto_four();
		String photo5_url = model.getPhoto_five();

		if (photo1_url != null) {
			photo1 = processingPhotoFromCamera(photo1_url);
			ByteArrayOutputStream baops = new ByteArrayOutputStream();
			photo1.compress(Bitmap.CompressFormat.JPEG, 100, baops);
			parse_bundle.putByteArray("photo1", baops.toByteArray());
			File f = new File(photo1_url);
			parse_bundle.putString("photo1[]"/* + "0" */, f.getName());
		}
		if (photo2_url != null) {
			photo2 = processingPhotoFromCamera(photo2_url);
			ByteArrayOutputStream baops = new ByteArrayOutputStream();
			photo2.compress(Bitmap.CompressFormat.JPEG, 100, baops);
			parse_bundle.putByteArray("photo2", baops.toByteArray());
			File f = new File(photo2_url);
			parse_bundle.putString("photo2[]"/* + "0" */, f.getName());
		}
		if (photo3_url != null) {
			photo3 = processingPhotoFromCamera(photo3_url);
			ByteArrayOutputStream baops = new ByteArrayOutputStream();
			photo3.compress(Bitmap.CompressFormat.JPEG, 100, baops);
			parse_bundle.putByteArray("photo3", baops.toByteArray());
			File f = new File(photo3_url);
			parse_bundle.putString("photo3[]"/* + "0" */, f.getName());
		}
		if (photo4_url != null) {
			photo4 = processingPhotoFromCamera(photo4_url);
			ByteArrayOutputStream baops = new ByteArrayOutputStream();
			photo4.compress(Bitmap.CompressFormat.JPEG, 100, baops);
			parse_bundle.putByteArray("photo4", baops.toByteArray());
			File f = new File(photo4_url);
			parse_bundle.putString("photo4[]"/* + "0" */, f.getName());
		}
		if (photo5_url != null) {
			photo5 = processingPhotoFromCamera(photo5_url);
			ByteArrayOutputStream baops = new ByteArrayOutputStream();
			photo5.compress(Bitmap.CompressFormat.JPEG, 100, baops);
			parse_bundle.putByteArray("photo5", baops.toByteArray());
			File f = new File(photo5_url);
			parse_bundle.putString("photo5[]"/* + "0" */, f.getName());
		}
	}

	private Bitmap processingPhotoFromCamera(String imagepath) {

		Bitmap bmp = Utils.compressImage(imagepath);
		ExifInterface ei;
		int orientation = 0;
		try {
			ei = new ExifInterface(imagepath);
			orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_NORMAL);
		} catch (IOException e) {
			e.printStackTrace();
		}

		switch (orientation) {
		case ExifInterface.ORIENTATION_ROTATE_90:
			bmp = RotateBitmap(bmp, 90);
			break;
		case ExifInterface.ORIENTATION_ROTATE_180:
			bmp = RotateBitmap(bmp, 180);
			break;
		}
		return bmp;
	}

	public Bitmap RotateBitmap(Bitmap source, float angle) {
		Matrix matrix = new Matrix();
		matrix.postRotate(angle);
		return Bitmap.createBitmap(source, 0, 0, source.getWidth(),
				source.getHeight(), matrix, true);
	}

	public Bitmap scaleDownBitmap(Bitmap photo) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		photo.compress(Bitmap.CompressFormat.JPEG, 100, stream);
		byte[] byteArray = stream.toByteArray();

		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 2;
		options.inPurgeable = true;
		options.inInputShareable = true;
		options.inJustDecodeBounds = false;
		options.inPreferredConfig = Bitmap.Config.ARGB_8888;
		photo = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length,
				options);

		return photo;
	}

	private class InsertMarketIntelligence extends
			AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) {
			// Creating service handler class instance
			DatabaseHandler db = new DatabaseHandler(getApplicationContext());
			SessionManagement session = new SessionManagement(
					getApplicationContext());
			HashMap<String, String> user = session.getUserDetails();
			user = session.getUserDetails();

			List<MarketSurveyModel> sycnactivities = db
					.getDataSycnMarketSurvey();
			String[] key_string = { "photo1", "photo2", "photo3", "photo4",
					"photo5" };

			Bundle parse_bundle = new Bundle();
			parse_bundle.putStringArray("key_string", key_string);
			parse_bundle.putString("type_file", ".png");

			ArrayList<HashMap<String, String>> market_survey_list = new ArrayList<HashMap<String, String>>();
			HashMap<String, String> market_survey = new HashMap<String, String>();
			String result = null;
			for (MarketSurveyModel cn : sycnactivities) {
				market_survey.put("market_intelligence_id[]",
						cn.getMarket_intelligence_id());
				market_survey.put("user_sales_id[]", cn.getUser_sales_id());
				market_survey.put("importir[]", cn.getImportir());
				market_survey.put("brand_name[]", cn.getBrand_name());
				market_survey.put("price[]", cn.getPrice());
				market_survey.put("product_name[]", cn.getProduct_name());
				market_survey.put("comment[]", cn.getComment());
				market_survey.put("date_market_intelligence[]",
						cn.getDate_market_intelligence());
				market_survey.put("date_change[]", cn.getDate_change());
				market_survey.put("user_change[]", cn.getUser_change());
				market_survey.put("stsrc[]", cn.getStsrc());

				market_survey_list.add(market_survey);
				setImageURLforMarketSurvey(cn, parse_bundle);
				result = doApiNew(true, parse_bundle, market_survey);
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			if (Utils.isOnline(getApplicationContext())) {
				Log.d("Status :", "Success" + result);
				JSONObject obj = null;
				try {
					obj = new JSONObject(result);
					DatabaseHandler db = new DatabaseHandler(
							getApplicationContext());
					if (obj.getString("response").equals("success")) {
						JSONArray ary = (JSONArray) obj
								.get("market_intelligence_id");
						for (int i = 0; i < ary.length(); i++) {
							db.updateProggressMarketSurvey(ary.get(i)
									.toString(), 2);
						}

						Log.d("Log : ", "Market Intelligence Success");

						new Handler().postDelayed(new Runnable() {
							@Override
							public void run() {
								marketintelligenceprogress();
							}
						}, SPLASH_TIME_OUT);
					} else {
						new Handler().postDelayed(new Runnable() {
							@Override
							public void run() {
								marketintelligenceprogress();
							}
						}, SPLASH_TIME_OUT);
					}

				} catch (final JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();

					new Handler().postDelayed(new Runnable() {
						@Override
						public void run() {
							Log.d("Log : ", "ERROR API MARKET INTELLIGENCE "
									+ e1.getMessage());
							marketintelligenceprogress();
						}
					}, SPLASH_TIME_OUT);
				}
			} else {

				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						Log.d("Log : ", "ERROR API Activities not have network");
						marketintelligenceprogress();
					}
				}, SPLASH_TIME_OUT);
			}

		}

		public String doApiNew(boolean hasMultiEntity, Bundle parse_bundle,
				HashMap<String, String> parameters) {

			HashMap<Object, Object> result = new HashMap<Object, Object>();
			String JSONString = null;

			try {
				HttpResponse response;
				HttpClient myClient = new DefaultHttpClient();
				String url;
				HttpPost myConnection = new HttpPost(Commons.ProductionHttp
						+ APIParam.getAPIUrl(APIParam.API_012));

				MultipartEntity mpEntity = new MultipartEntity(
						HttpMultipartMode.BROWSER_COMPATIBLE);
				// List<NameValuePair> nameValuePairs = new
				// ArrayList<NameValuePair>(2);
				for (Entry<String, String> entry : parameters.entrySet()) {
					String key = entry.getKey();
					String value = entry.getValue();
					Log.i("AAA", "key : " + key);
					Log.i("AAA", "value : " + value);
					Charset chars = Charset.forName("UTF-8");
					mpEntity.addPart(key, new StringBody(value, chars));

				}

				// TODO BUAT MULTIPART ENTITY
				if (hasMultiEntity) {
					Log.i("TAG", "MASUK ME");
					String[] key_string = parse_bundle
							.getStringArray("key_string");
					String file_type = parse_bundle.getString("type_file");

					Log.i("TAG", "file_type : " + file_type);
					if (file_type.equalsIgnoreCase(".png")) {
						Log.i("TAG", "cuma gambar");
						for (int i = 0; i < key_string.length; i++) {
							Log.i("TAG", "arrkey : " + key_string[i]);
							if (key_string[i].toString().length() > 0) {
								byte[] bytearr = parse_bundle
										.getByteArray(key_string[i]);
								Log.i("AAA", "bytearr : " + bytearr);
								if (bytearr != null) {
									mpEntity.addPart(
											key_string[i],
											new ByteArrayBody(
													parse_bundle
															.getByteArray(key_string[i]),
													key_string[i] + file_type));
								}
							}
						}
					} else {

						Log.i("AAA", "key_string[0] : " + key_string[0]);
						Log.i("TAG",
								"key0 : "
										+ parse_bundle
												.getStringArray(key_string[0])[0]);
						Log.i("TAG",
								"path0 : "
										+ parse_bundle
												.getStringArray(key_string[0])[1]);
						Log.i("TAG",
								"mimetype0 : "
										+ parse_bundle
												.getStringArray(key_string[0])[2]);
						for (int i = 0; i < key_string.length; i++) {
							Log.i("TAG", "arrkey : " + key_string[i]);
							if (key_string[i].toString().length() > 0) {
								File file = new File(
										parse_bundle
												.getStringArray(key_string[i])[1]);
								FileBody fileBody = new FileBody(
										file,
										parse_bundle
												.getStringArray(key_string[i])[2]);

								mpEntity.addPart(parse_bundle
										.getStringArray(key_string[i])[0],
										fileBody);
							}
						}
					}
				}
				myConnection.setEntity(mpEntity);
				try {
					response = myClient.execute(myConnection);
					JSONString = EntityUtils.toString(response.getEntity(),
							"UTF-8");
					Log.i("TAG", "jsonstring : " + JSONString);

				} catch (ClientProtocolException e) {
					result.put("result", Commons.ERROR);
					e.printStackTrace();
				} catch (IOException e) {
					result.put("result", Commons.ERROR);
					e.printStackTrace();
				}
			} catch (Exception e) {
				result.put("result", Commons.ERROR);
				e.printStackTrace();
			}
			Log.i("TAG", "result balikan : " + result);
			return JSONString;

		}
	}

}