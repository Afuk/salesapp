package com.andtechnology.indahjaya.database;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.andtechnology.indahjaya.lib.util.SessionManagement;
import com.andtechnology.indahjaya.model.ActivitiesDetailModel;
import com.andtechnology.indahjaya.model.ActivitiesModel;
import com.andtechnology.indahjaya.model.CategoryProductModel;
import com.andtechnology.indahjaya.model.LastUpdateModel;
import com.andtechnology.indahjaya.model.MappingSalesToCategoryProductModel;
import com.andtechnology.indahjaya.model.MappingSalesToStoreModel;
import com.andtechnology.indahjaya.model.MappingSalesToStoreTargetModel;
import com.andtechnology.indahjaya.model.MappingStoreModel;
import com.andtechnology.indahjaya.model.MarketSurveyModel;
import com.andtechnology.indahjaya.model.MessageModel;
import com.andtechnology.indahjaya.model.PromoModel;
import com.andtechnology.indahjaya.model.StoreRecommendationModel;
import com.andtechnology.indahjaya.model.SubCategoryProductModel;
import com.andtechnology.indahjaya.model.TypeActivitiesAnswerModel;
import com.andtechnology.indahjaya.model.TypeActivitiesModel;
import com.andtechnology.indahjaya.model.TypeActivitiesQuestionModel;
import com.andtechnology.indahjaya.restful.ConstantREST;

public class DatabaseHandler extends SQLiteOpenHelper {

	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 2;

	// Database Name .
	private static final String DATABASE_NAME = "indahjaya_db";

	private static final String TABLE_ACTIVITIES = "activities";
	private static final String TABLE_ACTIVITIES_DETAIL = "activities_detail";
	private static final String TABLE_CATEGORY_PRODUCT = "category_product";
	private static final String TABLE_MESSAGE = "message";
	private static final String TABLE_PROMO = "promo";
	private static final String TABLE_STORE_RECOMMENDATION = "store_recommendation";
	private static final String TABLE_SUB_CATEGORY_PRODUCT = "sub_category_product";
	private static final String TABLE_TYPE_ACTIVITIES = "type_activities";
	private static final String TABLE_TYPE_ACTIVITIES_ANSWER = "type_activities_answer";
	private static final String TABLE_TYPE_ACTIVITIES_QUESTION = "type_activities_question";
	private static final String TABLE_LAST_UPDATE = "last_updated";

	private static final String TABLE_MARKET_SURVEY = "market_intelligence";

	private static final String TABLE_MAPPING_STORE = "mapping_store";
	private static final String TABLE_MAPPING_SALES_TO_CATEGORY_PRODUCT = "mapping_sales_to_category_product";
	private static final String TABLE_MAPPING_SALES_TO_STORE = "mapping_sales_to_store";
	private static final String TABLE_MAPPING_SALES_TO_STORE_TARGET = "mapping_sales_to_store_target";
	Context ctx = null;

	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		ctx = context;
	}

	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	Calendar cal = Calendar.getInstance();

	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_ACTIVITIES_TABLE = "CREATE TABLE " + TABLE_ACTIVITIES
				+ " (" + "activities_id TEXT," + "mapping_store_id INTEGER,"
				+ "before_photo_store TEXT," + "after_photo_store TEXT,"
				+ "longtitude_sales TEXT," + "latitude_sales TEXT,"
				+ "status INTEGER," + "user_change TEXT,"
				+ "date_change DATETIME," + "stsrc TEXT,"

				+ "type_activity_id TEXT," + "type_activity_value TEXT,"
				+ "question_id TEXT," + "question_value TEXT,"
				+ "answer_id TEXT," + "answer_value TEXT,"
				+ "date_checkin TEXT," + "date_checkout TEXT)";

		String CREATE_ACTIVITIES_DETAIL_TABLE = "CREATE TABLE "
				+ TABLE_ACTIVITIES_DETAIL + " (" + "activities_id TEXT,"
				+ "type_activities_id INTEGER,"
				+ "sub_category_product_id INTEGER," + "description TEXT,"
				+ "user_change TEXT," + "date_change DATETIME," + "stsrc TEXT)";

		String CREATE_CATEGORY_PRODUCT_TABLE = "CREATE TABLE "
				+ TABLE_CATEGORY_PRODUCT + " ("
				+ "category_product_id INTEGER AUTO_INCREMENT,"
				+ "category_product_name TEXT," + "user_change TEXT,"
				+ "date_change DATETIME," + "stsrc TEXT)";

		String CREATE_MAPPING_SALES_TO_CATEGORY_PRODUCT_TABLE = "CREATE TABLE "
				+ TABLE_MAPPING_SALES_TO_CATEGORY_PRODUCT
				+ " ("
				+ "mapping_sales_to_category_product_id INTEGER AUTO_INCREMENT,"
				+ "user_sales_id TEXT," + "category_product_id TEXT,"
				+ "user_change TEXT," + "date_change DATETIME," + "stsrc TEXT)";

		String CREATE_MAPPING_SALES_TO_STORE_TABLE = "CREATE TABLE "
				+ TABLE_MAPPING_SALES_TO_STORE + " ("
				+ "mapping_sales_to_store_id INTEGER AUTO_INCREMENT,"
				+ "user_sales_id TEXT," + "store_id TEXT,"
				+ "user_change TEXT," + "date_change DATETIME," + "stsrc TEXT,"
				+ " store_name TEXT," + " address TEXT)";

		String CREATE_MAPPING_SALES_TO_STORE_TARGET_TABLE = "CREATE TABLE "
				+ TABLE_MAPPING_SALES_TO_STORE_TARGET + " ("
				+ "mapping_sales_to_store_target_id INTEGER AUTO_INCREMENT,"
				+ "user_sales_id TEXT," + "period TEXT," + "target TEXT,"
				+ "store_id TEXT," + "user_change TEXT,"
				+ "date_change DATETIME," + "stsrc TEXT)";

		String CREATE_MAPPING_STORE_TABLE = "CREATE TABLE "
				+ TABLE_MAPPING_STORE + " ("
				+ "mapping_store_id INTEGER AUTO_INCREMENT,"
				+ "user_rm_id TEXT," + "user_sales_id TEXT," + "store_id TEXT,"
				+ "date TEXT," + "user_change TEXT," + "date_change DATETIME,"
				+ "stsrc TEXT," + "activities_id TEXT," + "region_id TEXT,"
				+ "store_name TEXT," + "address TEXT," + "longtitude TEXT,"
				+ "latitude TEXT," + "radius TEXT,"
				+ "progress_status INTEGER)";

		String CREATE_MESSAGE_TABLE = "CREATE TABLE " + TABLE_MESSAGE + " ("
				+ "message_id INTEGER AUTO_INCREMENT," + "super_user_id TEXT,"
				+ "super_user_name TEXT," + "user_id TEXT," + "user_name TEXT,"
				+ "subject TEXT," + "body TEXT," + "status TEXT,"
				+ "date DATETIME," + "user_change TEXT,"
				+ "date_change DATETIME," + "stsrc TEXT)";

		String CREATE_PROMO_TABLE = "CREATE TABLE " + TABLE_PROMO + " ("
				+ "promo_id INTEGER," + "region_id INTEGER,"
				+ "sub_category_product_id INTEGER," + "promo_name TEXT,"
				+ "start_date DATETIME," + "end_date DATETIME,"
				+ "description TEXT," + "images TEXT," + "user_change TEXT,"
				+ "date_change DATETIME," + "stsrc TEXT)";

		String CREATE_STORE_RECOMMENDATION_TABLE = "CREATE TABLE "
				+ TABLE_STORE_RECOMMENDATION + " ("
				+ "store_recommendation_id TEXT," + "store_name TEXT,"
				+ "store_owner TEXT," + "store_address TEXT," + "contact TEXT,"
				+ "description TEXT," + "user_id TEXT," + "status INTEGER,"
				+ "date_recommendation DATETIME," + "image TEXT,"
				+ "image2 TEXT," + "longtitude TEXT," + "latitude TEXT,"
				+ "category_id TEXT," + "user_change TEXT,"
				+ "date_change DATETIME," + "stsrc TEXT)";

		String CREATE_SUB_CATEGORY_PRODUCT_TABLE = "CREATE TABLE "
				+ TABLE_SUB_CATEGORY_PRODUCT + " ("
				+ "sub_category_product_id INTEGER,"
				+ "category_product_id INTEGER,"
				+ "sub_category_product_name TEXT," + "description TEXT,"
				+ "images TEXT," + "user_change TEXT,"
				+ "date_change DATETIME," + "stsrc TEXT)";

		String CREATE_TYPE_ACTIVITIES_TABLE = "CREATE TABLE "
				+ TABLE_TYPE_ACTIVITIES + " ("
				+ "type_activities_id INTEGER AUTO_INCREMENT,"
				+ "type_activities_name TEXT," + "user_change TEXT,"
				+ "date_change DATETIME," + "stsrc TEXT)";

		String CREATE_TYPE_ACTIVITIES_TABLES_ANSWER = "CREATE TABLE "
				+ TABLE_TYPE_ACTIVITIES_ANSWER + " ("
				+ "type_activities_answer_id INTEGER AUTO_INCREMENT,"
				+ "type_activities_question_id INTEGER,"
				+ "type_activities_answer_name TEXT," + "user_change TEXT,"
				+ "date_change DATETIME," + "stsrc TEXT)";

		String CREATE_TYPE_ACTIVITIES_TABLE_QUESTION = "CREATE TABLE "
				+ TABLE_TYPE_ACTIVITIES_QUESTION + " ("
				+ "type_activities_question_id INTEGER AUTO_INCREMENT,"
				+ "type_activities_id INTEGER,"
				+ "type_activities_question_name TEXT," + "user_change TEXT,"
				+ "date_change DATETIME," + "stsrc TEXT)";

		String CREATE_LAST_UPDATE_TABLE = "CREATE TABLE " + TABLE_LAST_UPDATE
				+ " (" + "last_update DATETIME)";

		String CREATE_MARKET_SURVEY_TABLE = "CREATE TABLE "
				+ TABLE_MARKET_SURVEY + " (" + "market_intelligence_id TEXT,"
				+ "user_sales_id TEXT," + "importir TEXT," + "brand_name TEXT,"
				+ "product_name TEXT," + "photo_one TEXT," + "photo_two TEXT,"
				+ "photo_three TEXT," + "photo_four TEXT," + "photo_five TEXT,"
				+ "price TEXT," + "comment TEXT,"
				+ "date_market_intelligence DATETIME," + "user_change TEXT,"
				+ "date_change DATETIME," + "stsrc TEXT," + "status TEXT)";

		db.execSQL(CREATE_ACTIVITIES_TABLE);
		db.execSQL(CREATE_ACTIVITIES_DETAIL_TABLE);
		db.execSQL(CREATE_CATEGORY_PRODUCT_TABLE);
		db.execSQL(CREATE_MAPPING_SALES_TO_CATEGORY_PRODUCT_TABLE);
		db.execSQL(CREATE_MAPPING_SALES_TO_STORE_TABLE);
		db.execSQL(CREATE_MAPPING_SALES_TO_STORE_TARGET_TABLE);
		db.execSQL(CREATE_MAPPING_STORE_TABLE);
		db.execSQL(CREATE_MESSAGE_TABLE);
		db.execSQL(CREATE_PROMO_TABLE);
		db.execSQL(CREATE_STORE_RECOMMENDATION_TABLE);
		db.execSQL(CREATE_SUB_CATEGORY_PRODUCT_TABLE);
		db.execSQL(CREATE_TYPE_ACTIVITIES_TABLE);
		db.execSQL(CREATE_TYPE_ACTIVITIES_TABLE_QUESTION);
		db.execSQL(CREATE_TYPE_ACTIVITIES_TABLES_ANSWER);
		db.execSQL(CREATE_LAST_UPDATE_TABLE);
		db.execSQL(CREATE_MARKET_SURVEY_TABLE);

	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE " + TABLE_ACTIVITIES);
		db.execSQL("DROP TABLE " + TABLE_ACTIVITIES_DETAIL);
		db.execSQL("DROP TABLE " + TABLE_CATEGORY_PRODUCT);
		db.execSQL("DROP TABLE " + TABLE_MAPPING_SALES_TO_CATEGORY_PRODUCT);
		db.execSQL("DROP TABLE " + TABLE_MAPPING_SALES_TO_STORE);
		db.execSQL("DROP TABLE " + TABLE_MAPPING_SALES_TO_STORE_TARGET);
		db.execSQL("DROP TABLE " + TABLE_MAPPING_STORE);
		db.execSQL("DROP TABLE " + TABLE_MESSAGE);
		db.execSQL("DROP TABLE " + TABLE_PROMO);
		db.execSQL("DROP TABLE " + TABLE_STORE_RECOMMENDATION);
		db.execSQL("DROP TABLE " + TABLE_SUB_CATEGORY_PRODUCT);
		db.execSQL("DROP TABLE " + TABLE_TYPE_ACTIVITIES);
		db.execSQL("DROP TABLE " + TABLE_TYPE_ACTIVITIES_ANSWER);
		db.execSQL("DROP TABLE " + TABLE_TYPE_ACTIVITIES_QUESTION);
		db.execSQL("DROP TABLE " + TABLE_LAST_UPDATE);
		db.execSQL("DROP TABLE " + TABLE_MARKET_SURVEY);

		// Create tables again
		onCreate(db);
	}

	// Adding LAST UPDATE
	public void addLastUpdate(LastUpdateModel updatemodel) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put("last_update", updatemodel.getDate());

		// Inserting Row
		db.insert(TABLE_LAST_UPDATE, null, values);
		db.close(); // Closing database connection
	}

	public void updatemessage(int message_id) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("status", 1);
		db.update(TABLE_MESSAGE, values, "message_id = ?",
				new String[] { String.valueOf(message_id) });

		db.close();
	}

	public void updateProggressMappingStore(int mapping_store_id,
			int progress_status) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("progress_status", progress_status);
		db.update(TABLE_MAPPING_STORE, values, "mapping_store_id = ?",
				new String[] { String.valueOf(mapping_store_id) });

		db.close();
	}

	public void updateProggressMarketSurvey(String market_intelligence_id,
			int progress_status) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("status", progress_status);
		db.update(TABLE_MARKET_SURVEY, values, "market_intelligence_id = ?",
				new String[] { String.valueOf(market_intelligence_id) });

		db.close();
	}

	public void updateLatLongMappingStore(int mapping_store_id,
			String latitude, String longtitude) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("longtitude", longtitude);
		values.put("latitude", latitude);

		db.update(TABLE_MAPPING_STORE, values, "mapping_store_id = ?",
				new String[] { String.valueOf(mapping_store_id) });

		db.close();
	}

	public void updateProggressStoreRecommendation(
			String store_recommendation_id, int status) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("status", 2);
		values.put(
				"image",
				String.valueOf(ConstantREST.ImagePath2
						+ store_recommendation_id + "_1.jpg"));
		values.put(
				"image2",
				String.valueOf(ConstantREST.ImagePath2
						+ store_recommendation_id + "_2.jpg"));
		db.update(TABLE_STORE_RECOMMENDATION, values,
				"store_recommendation_id = ?",
				new String[] { String.valueOf(store_recommendation_id) });

		db.close();
	}

	public void updateImageActivities(String activities_id) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(
				"before_photo_store",
				String.valueOf(ConstantREST.ImagePath1 + activities_id
						+ "_1.jpg"));
		values.put(
				"after_photo_store",
				String.valueOf(ConstantREST.ImagePath1 + activities_id
						+ "_2.jpg"));
		db.update(TABLE_ACTIVITIES, values, "activities_id = ?",
				new String[] { String.valueOf(activities_id) });

		db.close();
	}

	public void addActivities(ActivitiesModel ResultModel) {
		int count = 0;
		SQLiteDatabase db = this.getWritableDatabase();
		SQLiteDatabase dbr = this.getReadableDatabase();

		String countQuery = "SELECT * FROM " + TABLE_ACTIVITIES
				+ " WHERE activities_id = '" + ResultModel.get_activities_id()
				+ "'";
		Cursor cursor = dbr.rawQuery(countQuery, null);
		if (cursor != null && !cursor.isClosed()) {
			count = cursor.getCount();
			cursor.close();
		}

		ContentValues values = new ContentValues();
		if (count == 0) {
			values.put("activities_id", ResultModel.get_activities_id());
			values.put("mapping_store_id", ResultModel.get_mapping_store_id());
			values.put("before_photo_store",
					ResultModel.get_before_photo_store());
			values.put("after_photo_store", ResultModel.get_after_photo_store());
			values.put("longtitude_sales", ResultModel.get_longtitude_sales());
			values.put("latitude_sales", ResultModel.get_latitude_sales());
			values.put("status", ResultModel.get_status());
			values.put("user_change", ResultModel.get_user_change());
			values.put("date_change", ResultModel.get_date_change());
			values.put("stsrc", ResultModel.get_stsrc());

			// TODO add new value for question
			values.put("type_activity_id", ResultModel.getType_activity_id());
			values.put("type_activity_value",
					ResultModel.getType_activity_value());
			values.put("question_id", ResultModel.getQuestion_id());
			values.put("question_value", ResultModel.getQuestion_value());
			values.put("answer_id", ResultModel.getAnswer_id());
			values.put("answer_value", ResultModel.getAnswer_value());

			values.put("date_checkin", ResultModel.getDate_checkin());
			values.put("date_checkout", ResultModel.getDate_checkout());

			db.insert(TABLE_ACTIVITIES, null, values);
		} else {
			values.put("mapping_store_id", ResultModel.get_mapping_store_id());
			values.put("before_photo_store",
					ResultModel.get_before_photo_store());
			values.put("after_photo_store", ResultModel.get_after_photo_store());
			values.put("longtitude_sales", ResultModel.get_longtitude_sales());
			values.put("latitude_sales", ResultModel.get_latitude_sales());
			values.put("status", ResultModel.get_status());
			values.put("user_change", ResultModel.get_user_change());
			values.put("date_change", ResultModel.get_date_change());
			values.put("stsrc", ResultModel.get_stsrc());

			// TODO add new value for question
			values.put("type_activity_id", ResultModel.getType_activity_id());
			values.put("type_activity_value",
					ResultModel.getType_activity_value());
			values.put("question_id", ResultModel.getQuestion_id());
			values.put("question_value", ResultModel.getQuestion_value());
			values.put("answer_id", ResultModel.getAnswer_id());
			values.put("answer_value", ResultModel.getAnswer_value());

			values.put("date_checkin", ResultModel.getAnswer_value());
			values.put("date_checkout", ResultModel.getAnswer_value());

			db.update(TABLE_ACTIVITIES, values, "activities_id = ?",
					new String[] { String.valueOf(ResultModel
							.get_activities_id()) });
		}
		db.close();
	}

	public void addActivitiesDetail(ActivitiesDetailModel ResultModel) {
		int count = 0;
		SQLiteDatabase db = this.getWritableDatabase();
		SQLiteDatabase dbr = this.getReadableDatabase();

		String countQuery = "SELECT * FROM " + TABLE_ACTIVITIES_DETAIL
				+ " WHERE activities_id = " + ResultModel.get_activities_id()
				+ " AND type_activities_id = "
				+ ResultModel.get_type_activities_id()
				+ " AND sub_category_product_id = "
				+ ResultModel.get_sub_category_product_id();
		Cursor cursor = dbr.rawQuery(countQuery, null);
		if (cursor != null && !cursor.isClosed()) {
			count = cursor.getCount();
			cursor.close();
		}

		ContentValues values = new ContentValues();
		if (count == 0) {
			values.put("activities_id", ResultModel.get_activities_id());
			values.put("type_activities_id",
					ResultModel.get_type_activities_id());
			values.put("sub_category_product_id",
					ResultModel.get_sub_category_product_id());
			values.put("description", ResultModel.get_description());
			values.put("user_change", ResultModel.get_user_change());
			values.put("date_change", ResultModel.get_date_change());
			values.put("stsrc", ResultModel.get_stsrc());
			db.insert(TABLE_ACTIVITIES_DETAIL, null, values);
		} else {
			values.put("activities_id", ResultModel.get_activities_id());
			values.put("type_activities_id",
					ResultModel.get_type_activities_id());
			values.put("sub_category_product_id",
					ResultModel.get_sub_category_product_id());
			values.put("description", ResultModel.get_description());
			values.put("user_change", ResultModel.get_user_change());
			values.put("date_change", ResultModel.get_date_change());
			values.put("stsrc", ResultModel.get_stsrc());
			db.update(TABLE_ACTIVITIES_DETAIL, values, "activities_id = ?",
					new String[] { String.valueOf(ResultModel
							.get_activities_id()) });
		}
		db.close();
	}

	public void addCategoryProduct(CategoryProductModel ResultModel) {
		int count = 0;
		SQLiteDatabase db = this.getWritableDatabase();
		SQLiteDatabase dbr = this.getReadableDatabase();

		String countQuery = "SELECT * FROM " + TABLE_CATEGORY_PRODUCT
				+ " WHERE category_product_id = "
				+ ResultModel.get_category_product_id();
		Cursor cursor = dbr.rawQuery(countQuery, null);
		if (cursor != null && !cursor.isClosed()) {
			count = cursor.getCount();
			cursor.close();
		}

		ContentValues values = new ContentValues();
		if (count == 0) {
			values.put("category_product_id",
					ResultModel.get_category_product_id());
			values.put("category_product_name",
					ResultModel.get_category_product_name());
			values.put("user_change", ResultModel.get_user_change());
			values.put("date_change", ResultModel.get_date_change());
			values.put("stsrc", ResultModel.get_stsrc());
			db.insert(TABLE_CATEGORY_PRODUCT, null, values);
		} else {
			values.put("category_product_name",
					ResultModel.get_category_product_name());
			values.put("user_change", ResultModel.get_user_change());
			values.put("date_change", ResultModel.get_date_change());
			values.put("stsrc", ResultModel.get_stsrc());
			db.update(TABLE_CATEGORY_PRODUCT, values,
					"category_product_id = ?", new String[] { String
							.valueOf(ResultModel.get_category_product_id()) });
		}
		db.close();
	}

	public void addMappingSalesToCategoryProduct(
			MappingSalesToCategoryProductModel ResultModel) {
		int count = 0;
		SQLiteDatabase db = this.getWritableDatabase();
		SQLiteDatabase dbr = this.getReadableDatabase();

		String countQuery = "SELECT * FROM "
				+ TABLE_MAPPING_SALES_TO_CATEGORY_PRODUCT
				+ " WHERE mapping_sales_to_category_product_id = "
				+ ResultModel.get_mapping_sales_to_category_product_id();
		Cursor cursor = dbr.rawQuery(countQuery, null);
		if (cursor != null && !cursor.isClosed()) {
			count = cursor.getCount();
			cursor.close();
		}

		ContentValues values = new ContentValues();
		if (count == 0) {
			values.put("mapping_sales_to_category_product_id",
					ResultModel.get_mapping_sales_to_category_product_id());
			values.put("user_sales_id", ResultModel.get_user_sales_id());
			values.put("category_product_id",
					ResultModel.get_category_product_id());
			values.put("user_change", ResultModel.get_user_change());
			values.put("date_change", ResultModel.get_date_change());
			values.put("stsrc", ResultModel.get_stsrc());
			db.insert(TABLE_MAPPING_SALES_TO_CATEGORY_PRODUCT, null, values);
		} else {
			values.put("user_sales_id", ResultModel.get_user_sales_id());
			values.put("category_product_id",
					ResultModel.get_category_product_id());
			values.put("user_change", ResultModel.get_user_change());
			values.put("date_change", ResultModel.get_date_change());
			values.put("stsrc", ResultModel.get_stsrc());
			db.update(TABLE_MAPPING_SALES_TO_CATEGORY_PRODUCT, values,
					"mapping_sales_to_category_product_id = ?",
					new String[] { String.valueOf(ResultModel
							.get_mapping_sales_to_category_product_id()) });
		}
		db.close();
	}

	public void addMappingSalesToStore(MappingSalesToStoreModel ResultModel) {
		int count = 0;
		SQLiteDatabase db = this.getWritableDatabase();
		SQLiteDatabase dbr = this.getReadableDatabase();

		String countQuery = "SELECT * FROM " + TABLE_MAPPING_SALES_TO_STORE
				+ " WHERE mapping_sales_to_store_id = "
				+ ResultModel.get_mapping_sales_to_store_id();
		Cursor cursor = dbr.rawQuery(countQuery, null);
		if (cursor != null && !cursor.isClosed()) {
			count = cursor.getCount();
			cursor.close();
		}

		ContentValues values = new ContentValues();
		if (count == 0) {
			values.put("mapping_sales_to_store_id",
					ResultModel.get_mapping_sales_to_store_id());
			values.put("user_sales_id", ResultModel.get_user_sales_id());
			values.put("store_id", ResultModel.get_store_id());
			values.put("user_change", ResultModel.get_user_change());
			values.put("date_change", ResultModel.get_date_change());
			values.put("stsrc", ResultModel.get_stsrc());
			values.put("store_name", ResultModel.get_store_name());
			values.put("address", ResultModel.get_address());
			db.insert(TABLE_MAPPING_SALES_TO_STORE, null, values);
		} else {
			values.put("user_sales_id", ResultModel.get_user_sales_id());
			values.put("store_id", ResultModel.get_store_id());
			values.put("user_change", ResultModel.get_user_change());
			values.put("date_change", ResultModel.get_date_change());
			values.put("stsrc", ResultModel.get_stsrc());
			values.put("store_name", ResultModel.get_store_name());
			values.put("address", ResultModel.get_address());
			db.update(TABLE_MAPPING_SALES_TO_STORE, values,
					"mapping_sales_to_store_id = ?", new String[] { String
							.valueOf(ResultModel
									.get_mapping_sales_to_store_id()) });
		}
		db.close();
	}

	public void addMappingSalesToStoreTarget(
			MappingSalesToStoreTargetModel ResultModel) {
		int count = 0;
		SQLiteDatabase db = this.getWritableDatabase();
		SQLiteDatabase dbr = this.getReadableDatabase();

		String countQuery = "SELECT * FROM "
				+ TABLE_MAPPING_SALES_TO_STORE_TARGET
				+ " WHERE mapping_sales_to_store_target_id = "
				+ ResultModel.get_mapping_sales_to_store_target_id();
		Cursor cursor = dbr.rawQuery(countQuery, null);
		if (cursor != null && !cursor.isClosed()) {
			count = cursor.getCount();
			cursor.close();
		}

		ContentValues values = new ContentValues();
		if (count == 0) {
			values.put("mapping_sales_to_store_target_id",
					ResultModel.get_mapping_sales_to_store_target_id());
			values.put("user_sales_id", ResultModel.get_user_sales_id());
			values.put("period", ResultModel.get_period());
			values.put("target", ResultModel.get_target());
			values.put("user_change", ResultModel.get_user_change());
			values.put("date_change", ResultModel.get_date_change());
			values.put("stsrc", ResultModel.get_stsrc());
			db.insert(TABLE_MAPPING_SALES_TO_STORE_TARGET, null, values);
		} else {
			values.put("user_sales_id", ResultModel.get_user_sales_id());
			values.put("period", ResultModel.get_period());
			values.put("target", ResultModel.get_target());
			values.put("user_change", ResultModel.get_user_change());
			values.put("date_change", ResultModel.get_date_change());
			values.put("stsrc", ResultModel.get_stsrc());
			db.update(TABLE_MAPPING_SALES_TO_STORE_TARGET, values,
					"mapping_sales_to_store_target_id = ?",
					new String[] { String.valueOf(ResultModel
							.get_mapping_sales_to_store_target_id()) });
		}
		db.close();
	}

	public void addMappingStore(MappingStoreModel ResultModel) {
		int count = 0;
		SQLiteDatabase db = this.getWritableDatabase();
		SQLiteDatabase dbr = this.getReadableDatabase();

		String countQuery = "SELECT * FROM " + TABLE_MAPPING_STORE
				+ " WHERE mapping_store_id = "
				+ ResultModel.get_mapping_store_id();
		Cursor cursor = dbr.rawQuery(countQuery, null);
		if (cursor != null && !cursor.isClosed()) {
			count = cursor.getCount();
			cursor.close();
		}

		ContentValues values = new ContentValues();
		if (count == 0) {
			values.put("mapping_store_id", ResultModel.get_mapping_store_id());
			values.put("user_rm_id", ResultModel.get_user_rm_id());
			values.put("user_sales_id", ResultModel.get_user_sales_id());
			values.put("store_id", ResultModel.get_store_id());
			values.put("date", ResultModel.get_date());
			values.put("user_change", ResultModel.get_user_change());
			values.put("date_change", ResultModel.get_date_change());
			values.put("stsrc", ResultModel.get_stsrc());
			values.put("progress_status", ResultModel.get_progress_status());
			values.put("activities_id", ResultModel.get_activities_id());
			values.put("region_id", ResultModel.get_region_id());
			values.put("store_name", ResultModel.get_store_name());
			values.put("address", ResultModel.get_address());
			values.put("longtitude", ResultModel.get_longtitude());
			values.put("latitude", ResultModel.get_latitude());
			values.put("radius", ResultModel.get_radius());
			db.insert(TABLE_MAPPING_STORE, null, values);
		} else {
			values.put("user_rm_id", ResultModel.get_user_rm_id());
			values.put("user_sales_id", ResultModel.get_user_sales_id());
			values.put("store_id", ResultModel.get_store_id());
			values.put("date", ResultModel.get_date());
			values.put("user_change", ResultModel.get_user_change());
			values.put("date_change", ResultModel.get_date_change());
			values.put("stsrc", ResultModel.get_stsrc());
			values.put("progress_status", ResultModel.get_progress_status());
			values.put("activities_id", ResultModel.get_activities_id());
			values.put("region_id", ResultModel.get_region_id());
			values.put("store_name", ResultModel.get_store_name());
			values.put("address", ResultModel.get_address());
			values.put("longtitude", ResultModel.get_longtitude());
			values.put("latitude", ResultModel.get_latitude());
			values.put("radius", ResultModel.get_radius());
			db.update(TABLE_MAPPING_STORE, values, "mapping_store_id = ?",
					new String[] { String.valueOf(ResultModel
							.get_mapping_store_id()) });
		}
		db.close();
	}

	public void addMessage(MessageModel ResultModel) {
		int count = 0;
		SQLiteDatabase db = this.getWritableDatabase();
		SQLiteDatabase dbr = this.getReadableDatabase();

		String countQuery = "SELECT * FROM " + TABLE_MESSAGE
				+ " WHERE message_id = " + ResultModel.get_message_id();
		Cursor cursor = dbr.rawQuery(countQuery, null);
		if (cursor != null && !cursor.isClosed()) {
			count = cursor.getCount();
			cursor.close();
		}

		ContentValues values = new ContentValues();
		if (count == 0) {
			values.put("message_id", ResultModel.get_message_id());
			values.put("super_user_id", ResultModel.get_super_user_id());
			values.put("super_user_name", ResultModel.get_super_user_name());
			values.put("user_id", ResultModel.get_user_id());
			values.put("user_name", ResultModel.get_user_name());
			values.put("subject", ResultModel.get_subject());
			values.put("body", ResultModel.get_body());
			values.put("status", ResultModel.get_status());
			values.put("date", ResultModel.get_date());
			values.put("user_change", ResultModel.get_user_change());
			values.put("date_change", ResultModel.get_date_change());
			values.put("stsrc", ResultModel.get_stsrc());
			db.insert(TABLE_MESSAGE, null, values);
		} else {
			values.put("super_user_id", ResultModel.get_super_user_id());
			values.put("super_user_name", ResultModel.get_super_user_name());
			values.put("user_id", ResultModel.get_user_id());
			values.put("user_name", ResultModel.get_user_name());
			values.put("subject", ResultModel.get_subject());
			values.put("body", ResultModel.get_body());
			values.put("status", ResultModel.get_status());
			values.put("date", ResultModel.get_date());
			values.put("user_change", ResultModel.get_user_change());
			values.put("date_change", ResultModel.get_date_change());
			values.put("stsrc", ResultModel.get_stsrc());
			db.update(
					TABLE_MESSAGE,
					values,
					"message_id = ?",
					new String[] { String.valueOf(ResultModel.get_message_id()) });
		}
		db.close();
	}

	public void addPromo(PromoModel ResultModel) {
		int count = 0;
		SQLiteDatabase db = this.getWritableDatabase();
		SQLiteDatabase dbr = this.getReadableDatabase();

		String countQuery = "SELECT * FROM " + TABLE_PROMO
				+ " WHERE promo_id = " + ResultModel.get_promo_id();
		Cursor cursor = dbr.rawQuery(countQuery, null);
		if (cursor != null && !cursor.isClosed()) {
			count = cursor.getCount();
			cursor.close();
		}

		ContentValues values = new ContentValues();
		if (count == 0) {
			values.put("promo_id", ResultModel.get_promo_id());
			values.put("region_id", ResultModel.get_region_id());
			values.put("sub_category_product_id",
					ResultModel.get_sub_category_product_id());
			values.put("promo_name", ResultModel.get_promo_name());
			values.put("start_date", ResultModel.get_start_date());
			values.put("end_date", ResultModel.get_end_date());
			values.put("description", ResultModel.get_description());
			values.put("images", ResultModel.get_images());
			values.put("user_change", ResultModel.get_user_change());
			values.put("date_change", ResultModel.get_date_change());
			values.put("stsrc", ResultModel.get_stsrc());
			db.insert(TABLE_PROMO, null, values);
		} else {
			values.put("region_id", ResultModel.get_region_id());
			values.put("sub_category_product_id",
					ResultModel.get_sub_category_product_id());
			values.put("promo_name", ResultModel.get_promo_name());
			values.put("start_date", ResultModel.get_start_date());
			values.put("end_date", ResultModel.get_end_date());
			values.put("description", ResultModel.get_description());
			values.put("images", ResultModel.get_images());
			values.put("user_change", ResultModel.get_user_change());
			values.put("date_change", ResultModel.get_date_change());
			values.put("stsrc", ResultModel.get_stsrc());
			db.update(TABLE_PROMO, values, "promo_id = ?",
					new String[] { String.valueOf(ResultModel.get_promo_id()) });
		}
		db.close();
	}

	public void addStoreRecommendation(StoreRecommendationModel ResultModel) {
		int count = 0;
		SQLiteDatabase db = this.getWritableDatabase();
		SQLiteDatabase dbr = this.getReadableDatabase();

		String countQuery = "SELECT * FROM " + TABLE_STORE_RECOMMENDATION
				+ " WHERE store_recommendation_id = " + "'"
				+ ResultModel.get_store_recommendation_id() + "'";
		Cursor cursor = dbr.rawQuery(countQuery, null);
		if (cursor != null && !cursor.isClosed()) {
			count = cursor.getCount();
			cursor.close();
		}

		ContentValues values = new ContentValues();
		if (count == 0) {
			values.put("store_recommendation_id",
					ResultModel.get_store_recommendation_id());
			values.put("store_name", ResultModel.get_store_name());
			values.put("store_owner", ResultModel.get_store_owner());
			values.put("store_address", ResultModel.get_store_address());
			values.put("contact", ResultModel.get_contact());
			values.put("description", ResultModel.get_description());
			values.put("user_id", ResultModel.get_user_id());
			values.put("status", ResultModel.get_status());
			values.put("date_recommendation",
					ResultModel.get_date_recommendation());
			values.put("image", ResultModel.get_image());
			values.put("image2", ResultModel.get_image2());
			values.put("longtitude", ResultModel.get_longtitude());
			values.put("latitude", ResultModel.get_latitude());
			values.put("category_id", ResultModel.get_category_id());

			values.put("user_change", ResultModel.get_user_change());
			values.put("date_change", ResultModel.get_date_change());
			values.put("stsrc", ResultModel.get_stsrc());
			db.insert(TABLE_STORE_RECOMMENDATION, null, values);
		} else {
			values.put("store_name", ResultModel.get_store_name());
			values.put("store_owner", ResultModel.get_store_owner());
			values.put("store_address", ResultModel.get_store_address());
			values.put("contact", ResultModel.get_contact());
			values.put("description", ResultModel.get_description());
			values.put("user_id", ResultModel.get_user_id());
			values.put("status", ResultModel.get_status());
			values.put("date_recommendation",
					ResultModel.get_date_recommendation());
			values.put("image", ResultModel.get_image());
			values.put("image2", ResultModel.get_image2());
			values.put("longtitude", ResultModel.get_longtitude());
			values.put("latitude", ResultModel.get_latitude());
			values.put("category_id", ResultModel.get_category_id());

			values.put("user_change", ResultModel.get_user_change());
			values.put("date_change", ResultModel.get_date_change());
			values.put("stsrc", ResultModel.get_stsrc());
			db.update(TABLE_STORE_RECOMMENDATION, values,
					"store_recommendation_id = ?",
					new String[] { String.valueOf(ResultModel
							.get_store_recommendation_id()) });
		}
		db.close();
	}

	public void addSubCategoryProduct(SubCategoryProductModel ResultModel) {
		int count = 0;
		SQLiteDatabase db = this.getWritableDatabase();
		SQLiteDatabase dbr = this.getReadableDatabase();

		String countQuery = "SELECT * FROM " + TABLE_SUB_CATEGORY_PRODUCT
				+ " WHERE sub_category_product_id = "
				+ ResultModel.get_sub_category_product_id();
		Cursor cursor = dbr.rawQuery(countQuery, null);
		if (cursor != null && !cursor.isClosed()) {
			count = cursor.getCount();
			cursor.close();
		}

		ContentValues values = new ContentValues();
		if (count == 0) {
			values.put("sub_category_product_id",
					ResultModel.get_sub_category_product_id());
			values.put("category_product_id",
					ResultModel.get_category_product_id());
			values.put("sub_category_product_name",
					ResultModel.get_sub_category_product_name());
			values.put("description", ResultModel.get_description());
			values.put("images", ResultModel.get_images());
			values.put("user_change", ResultModel.get_user_change());
			values.put("date_change", ResultModel.get_date_change());
			values.put("stsrc", ResultModel.get_stsrc());
			db.insert(TABLE_SUB_CATEGORY_PRODUCT, null, values);
		} else {
			values.put("category_product_id",
					ResultModel.get_category_product_id());
			values.put("sub_category_product_name",
					ResultModel.get_sub_category_product_name());
			values.put("description", ResultModel.get_description());
			values.put("images", ResultModel.get_images());
			values.put("user_change", ResultModel.get_user_change());
			values.put("date_change", ResultModel.get_date_change());
			values.put("stsrc", ResultModel.get_stsrc());
			db.update(TABLE_SUB_CATEGORY_PRODUCT, values,
					"sub_category_product_id = ?",
					new String[] { String.valueOf(ResultModel
							.get_sub_category_product_id()) });
		}
		db.close();
	}

	public void addTypeActivities(TypeActivitiesModel ResultModel) {
		int count = 0;
		SQLiteDatabase db = this.getWritableDatabase();
		SQLiteDatabase dbr = this.getReadableDatabase();

		String countQuery = "SELECT * FROM " + TABLE_TYPE_ACTIVITIES
				+ " WHERE type_activities_id = "
				+ ResultModel.get_type_activities_id();
		Cursor cursor = dbr.rawQuery(countQuery, null);
		if (cursor != null && !cursor.isClosed()) {
			count = cursor.getCount();
			cursor.close();
		}

		ContentValues values = new ContentValues();
		if (count == 0) {
			values.put("type_activities_id",
					ResultModel.get_type_activities_id());
			values.put("type_activities_name",
					ResultModel.get_type_activities_name());
			values.put("user_change", ResultModel.get_user_change());
			values.put("date_change", ResultModel.get_date_change());
			values.put("stsrc", ResultModel.get_stsrc());
			db.insert(TABLE_TYPE_ACTIVITIES, null, values);
		} else {
			values.put("type_activities_name",
					ResultModel.get_type_activities_name());
			values.put("user_change", ResultModel.get_user_change());
			values.put("date_change", ResultModel.get_date_change());
			values.put("stsrc", ResultModel.get_stsrc());
			db.update(TABLE_TYPE_ACTIVITIES, values, "type_activities_id = ?",
					new String[] { String.valueOf(ResultModel
							.get_type_activities_id()) });
		}
		db.close();
	}

	public void addTypeActivitiesAnswer(TypeActivitiesAnswerModel ResultModel) {
		int count = 0;
		SQLiteDatabase db = this.getWritableDatabase();
		SQLiteDatabase dbr = this.getReadableDatabase();

		String countQuery = "SELECT * FROM " + TABLE_TYPE_ACTIVITIES_ANSWER
				+ " WHERE type_activities_answer_id = "
				+ ResultModel.get_type_activities_answer_id();
		Cursor cursor = dbr.rawQuery(countQuery, null);
		if (cursor != null && !cursor.isClosed()) {
			count = cursor.getCount();
			cursor.close();
		}

		ContentValues values = new ContentValues();
		if (count == 0) {
			values.put("type_activities_answer_id",
					ResultModel.get_type_activities_answer_id());
			values.put("type_activities_question_id",
					ResultModel.get_type_activities_question_id());
			values.put("type_activities_answer_name",
					ResultModel.get_type_activities_answer_name());
			values.put("user_change", ResultModel.get_user_change());
			values.put("date_change", ResultModel.get_date_change());
			values.put("stsrc", ResultModel.get_stsrc());
			db.insert(TABLE_TYPE_ACTIVITIES_ANSWER, null, values);
		} else {
			values.put("type_activities_question_id",
					ResultModel.get_type_activities_question_id());
			values.put("type_activities_answer_name",
					ResultModel.get_type_activities_answer_name());
			values.put("user_change", ResultModel.get_user_change());
			values.put("date_change", ResultModel.get_date_change());
			values.put("stsrc", ResultModel.get_stsrc());
			db.update(TABLE_TYPE_ACTIVITIES_ANSWER, values,
					"type_activities_answer_id = ?", new String[] { String
							.valueOf(ResultModel
									.get_type_activities_answer_id()) });
		}
		db.close();
	}

	public void addTypeActivitiesQuestion(
			TypeActivitiesQuestionModel ResultModel) {
		int count = 0;
		SQLiteDatabase db = this.getWritableDatabase();
		SQLiteDatabase dbr = this.getReadableDatabase();

		String countQuery = "SELECT * FROM " + TABLE_TYPE_ACTIVITIES_QUESTION
				+ " WHERE type_activities_question_id = "
				+ ResultModel.get_type_activities_question_id();
		Cursor cursor = dbr.rawQuery(countQuery, null);
		if (cursor != null && !cursor.isClosed()) {
			count = cursor.getCount();
			cursor.close();
		}

		ContentValues values = new ContentValues();
		if (count == 0) {
			values.put("type_activities_question_id",
					ResultModel.get_type_activities_question_id());
			values.put("type_activities_id",
					ResultModel.getType_activities_id());
			values.put("type_activities_question_name",
					ResultModel.get_type_activities_question_name());
			values.put("user_change", ResultModel.get_user_change());
			values.put("date_change", ResultModel.get_date_change());
			values.put("stsrc", ResultModel.get_stsrc());
			db.insert(TABLE_TYPE_ACTIVITIES_QUESTION, null, values);
		} else {
			values.put("type_activities_id",
					ResultModel.getType_activities_id());
			values.put("type_activities_question_name",
					ResultModel.get_type_activities_question_name());
			values.put("user_change", ResultModel.get_user_change());
			values.put("date_change", ResultModel.get_date_change());
			values.put("stsrc", ResultModel.get_stsrc());
			db.update(TABLE_TYPE_ACTIVITIES_QUESTION, values,
					"type_activities_question_id = ?", new String[] { String
							.valueOf(ResultModel
									.get_type_activities_question_id()) });
		}
		db.close();
	}

	// public void updateProggressStoreRegistration(String
	// store_registration_id,
	// int proggress_status) {
	// SQLiteDatabase db = this.getWritableDatabase();
	// ContentValues values = new ContentValues();
	// values.put("proggress_status", proggress_status);
	// db.update(TABLE_OUTLET_REGISTRATION, values,
	// "store_registration_id = ?",
	// new String[] { String.valueOf(store_registration_id) });
	//
	// db.close();
	// }

	public List<ActivitiesModel> getAllActivities() {
		List<ActivitiesModel> ResultList = new ArrayList<ActivitiesModel>();

		SessionManagement session = new SessionManagement(ctx);
		HashMap<String, String> user = session.getUserDetails();
		
		// Select All Query
		String selectQuery = "SELECT a.* FROM " + TABLE_ACTIVITIES
				+ " a JOIN mapping_store b " +
				"ON a.mapping_store_id = b.mapping_store_id " +
				"WHERE b.user_sales_id = '"+user.get(session.USER_ID)+"' a.stsrc <> 'D' AND b. ORDER BY a.date_change DESC";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				int i = 0;
				ActivitiesModel ResultData = new ActivitiesModel();
				ResultData.set_activities_id(cursor.getString(i));
				ResultData.set_mapping_store_id(cursor.getInt(i += 1));
				ResultData.set_before_photo_store(cursor.getString(i += 1));
				ResultData.set_after_photo_store(cursor.getString(i += 1));
				ResultData.set_longtitude_sales(cursor.getString(i += 1));
				ResultData.set_latitude_sales(cursor.getString(i += 1));
				ResultData.set_status(cursor.getInt(i += 1));
				ResultData.set_user_change(cursor.getString(i += 1));
				ResultData.set_date_change(cursor.getString(i += 1));
				ResultData.set_stsrc(cursor.getString(i += 1));
				ResultData.setType_activity_id(cursor.getString(i += 1));
				ResultData.setQuestion_id(cursor.getString(i += 1));
				ResultData.setAnswer_id(cursor.getString(i += 1));
				ResultList.add(ResultData);
			} while (cursor.moveToNext());
		}
		db.close();
		return ResultList;
	}

	public ActivitiesModel getActivityByID(int mapping_store_id) {
		// List<ActivitiesModel> ResultList = new ArrayList<ActivitiesModel>();


		SessionManagement session = new SessionManagement(ctx);
		HashMap<String, String> user = session.getUserDetails();
		
		// Select All Query
		String selectQuery = "SELECT a.* FROM " + TABLE_ACTIVITIES
				+ " a JOIN mapping_store b " +
				"ON a.mapping_store_id = b.mapping_store_id " +
				"WHERE b.user_sales_id = '"+user.get(session.USER_ID)+"' AND a.stsrc <> 'D' AND a.mapping_store_id = "
				+ mapping_store_id + " ORDER BY a.date_change DESC";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		ActivitiesModel ResultData = new ActivitiesModel();
		if (cursor.moveToFirst()) {
			int i = 0;
			ResultData.set_activities_id(cursor.getString(i));
			ResultData.set_mapping_store_id(cursor.getInt(i += 1));
			ResultData.set_before_photo_store(cursor.getString(i += 1));
			ResultData.set_after_photo_store(cursor.getString(i += 1));
			ResultData.set_longtitude_sales(cursor.getString(i += 1));
			ResultData.set_latitude_sales(cursor.getString(i += 1));
			ResultData.set_status(cursor.getInt(i += 1));
			ResultData.set_user_change(cursor.getString(i += 1));
			ResultData.set_date_change(cursor.getString(i += 1));
			ResultData.set_stsrc(cursor.getString(i += 1));
			ResultData.setType_activity_id(cursor.getString(i += 1));
			ResultData.setType_activity_value(cursor.getString(i += 1));
			ResultData.setQuestion_id(cursor.getString(i += 1));
			ResultData.setQuestion_value(cursor.getString(i += 1));
			ResultData.setAnswer_id(cursor.getString(i += 1));
			ResultData.setAnswer_value(cursor.getString(i += 1));
			ResultData.setDate_checkin(cursor.getString(i += 1));
			ResultData.setDate_checkout(cursor.getString(i += 1));
		}
		// ResultList.add(ResultData);
		db.close();
		return ResultData;
	}

	public List<ActivitiesDetailModel> getAllActivitiesDetail() {
		List<ActivitiesDetailModel> ResultList = new ArrayList<ActivitiesDetailModel>();

		// Select All Query
		String selectQuery = "SELECT * FROM " + TABLE_ACTIVITIES_DETAIL
				+ " WHERE stsrc <> 'D' ORDER BY date_change DESC";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				int i = 0;
				ActivitiesDetailModel ResultData = new ActivitiesDetailModel();
				ResultData.set_activities_id(cursor.getString(i));
				ResultData.set_type_activities_id(cursor.getInt(i += 1));
				ResultData.set_sub_category_product_id(cursor.getInt(i += 1));
				ResultData.set_description(cursor.getString(i += 1));
				ResultData.set_user_change(cursor.getString(i += 1));
				ResultData.set_date_change(cursor.getString(i += 1));
				ResultData.set_stsrc(cursor.getString(i += 1));
				ResultList.add(ResultData);
			} while (cursor.moveToNext());
		}
		db.close();
		return ResultList;
	}

	public List<CategoryProductModel> getAllCategoryProduct() {
		List<CategoryProductModel> ResultList = new ArrayList<CategoryProductModel>();

		// Select All Query
		String selectQuery = "SELECT category_product_id, category_product_name FROM "
				+ TABLE_CATEGORY_PRODUCT
				+ " WHERE stsrc <> 'D' ORDER BY date_change DESC";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				int i = 0;
				CategoryProductModel ResultData = new CategoryProductModel();
				ResultData.set_category_product_id(cursor.getInt(i));
				ResultData.set_category_product_name(cursor.getString(i += 1));
				ResultList.add(ResultData);
			} while (cursor.moveToNext());
		}
		db.close();
		return ResultList;
	}

	public List<MappingSalesToCategoryProductModel> getAllMappingSalesToCategoryProduct() {
		List<MappingSalesToCategoryProductModel> ResultList = new ArrayList<MappingSalesToCategoryProductModel>();

		SessionManagement session = new SessionManagement(ctx);
		HashMap<String, String> user = session.getUserDetails();
		// Select All Query
		String selectQuery = "SELECT * FROM "
				+ TABLE_MAPPING_SALES_TO_CATEGORY_PRODUCT
				+ " WHERE stsrc <> 'D' AND user_sales_id = '"+user.get(session.USER_ID)+"' ORDER BY date_change DESC";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				int i = 0;
				MappingSalesToCategoryProductModel ResultData = new MappingSalesToCategoryProductModel();
				ResultData.set_mapping_sales_to_category_product_id(cursor
						.getInt(i));
				ResultData.set_user_sales_id(cursor.getString(i += 1));
				ResultData.set_category_product_id(cursor.getString(i += 1));
				ResultData.set_user_change(cursor.getString(i += 1));
				ResultData.set_date_change(cursor.getString(i += 1));
				ResultData.set_stsrc(cursor.getString(i += 1));
				ResultList.add(ResultData);
			} while (cursor.moveToNext());
		}
		db.close();
		return ResultList;
	}

	public List<MappingSalesToStoreModel> getAllMappingSalesToStore() {
		List<MappingSalesToStoreModel> ResultList = new ArrayList<MappingSalesToStoreModel>();

		SessionManagement session = new SessionManagement(ctx);
		HashMap<String, String> user = session.getUserDetails();
		
		// Select All Query
		String selectQuery = "SELECT * FROM "
				+ TABLE_MAPPING_SALES_TO_STORE
				+ " WHERE stsrc <> 'D' AND store_id NOT IN (SELECT store_id FROM "
				+ TABLE_MAPPING_STORE
				+ " where date = date('now') ) AND user_sales_id = '"+user.get(session.USER_ID)+"' ORDER BY date_change DESC";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				int i = 0;
				MappingSalesToStoreModel ResultData = new MappingSalesToStoreModel();
				ResultData.set_mapping_sales_to_store_id(cursor.getInt(i));
				ResultData.set_user_sales_id(cursor.getString(i += 1));
				ResultData.set_store_id(cursor.getString(i += 1));
				ResultData.set_user_change(cursor.getString(i += 1));
				ResultData.set_date_change(cursor.getString(i += 1));
				ResultData.set_stsrc(cursor.getString(i += 1));
				ResultData.set_store_name(cursor.getString(i += 1));
				ResultData.set_address(cursor.getString(i += 1));
				ResultList.add(ResultData);
			} while (cursor.moveToNext());
		}
		db.close();
		return ResultList;
	}

	public List<MappingSalesToStoreTargetModel> getAllMappingSalesToStoreTarget() {
		List<MappingSalesToStoreTargetModel> ResultList = new ArrayList<MappingSalesToStoreTargetModel>();

		SessionManagement session = new SessionManagement(ctx);
		HashMap<String, String> user = session.getUserDetails();
		
		// Select All Query
		String selectQuery = "SELECT * FROM "
				+ TABLE_MAPPING_SALES_TO_STORE_TARGET
				+ " WHERE stsrc <> 'D' AND user_sales_id = '"+user.get(session.USER_ID)+"' ORDER BY date_change DESC";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				int i = 0;
				MappingSalesToStoreTargetModel ResultData = new MappingSalesToStoreTargetModel();
				ResultData.set_mapping_sales_to_store_target_id(cursor
						.getInt(i));
				ResultData.set_user_sales_id(cursor.getString(i += 1));
				ResultData.set_period(cursor.getString(i += 1));
				ResultData.set_target(cursor.getString(i += 1));
				ResultData.set_user_change(cursor.getString(i += 1));
				ResultData.set_date_change(cursor.getString(i += 1));
				ResultData.set_stsrc(cursor.getString(i += 1));
				ResultList.add(ResultData);
			} while (cursor.moveToNext());
		}
		db.close();
		return ResultList;
	}

	public List<MappingStoreModel> getAllMappingStore(int status, String id,
			String date) {
		List<MappingStoreModel> ResultList = new ArrayList<MappingStoreModel>();

		// Select All Query

		String selectQuery = "SELECT *  FROM " + TABLE_MAPPING_STORE
				+ " WHERE stsrc <> 'D' AND progress_status = " + status
				+ " AND user_sales_id = '" + id + "' AND date <= '" + date
				+ "' ORDER BY date(date) DESC";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				int i = 0;
				MappingStoreModel ResultData = new MappingStoreModel();

				ResultData.set_mapping_store_id(cursor.getInt(i));
				ResultData.set_user_rm_id(cursor.getString(i += 1));
				ResultData.set_user_sales_id(cursor.getString(i += 1));
				ResultData.set_store_id(cursor.getString(i += 1));
				ResultData.set_date(cursor.getString(i += 1));
				ResultData.set_user_change(cursor.getString(i += 1));
				ResultData.set_date_change(cursor.getString(i += 1));
				ResultData.set_stsrc(cursor.getString(i += 1));
				ResultData.set_activities_id(cursor.getString(i += 1));
				ResultData.set_region_id(cursor.getString(i += 1));
				ResultData.set_store_name(cursor.getString(i += 1));
				ResultData.set_address(cursor.getString(i += 1));
				ResultData.set_longtitude(cursor.getString(i += 1));
				ResultData.set_latitude(cursor.getString(i += 1));
				ResultData.set_radius(cursor.getString(i += 1));
				ResultData.set_progress_status(Integer.parseInt(cursor
						.getString(i += 1)));

				ResultList.add(ResultData);
			} while (cursor.moveToNext());
		}
		db.close();
		return ResultList;
	}

	public List<MappingStoreModel> getAllDateMappingStore(int status, String id) {
		List<MappingStoreModel> MappingStoreList = new ArrayList<MappingStoreModel>();
		// Select All Query
		String selectQuery = "SELECT DISTINCT date  FROM "
				+ TABLE_MAPPING_STORE
				+ " WHERE stsrc <> 'D' AND progress_status = " + status
				+ " AND user_sales_id = '" + id + "' ORDER BY date DESC";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				MappingStoreModel mappingstore = new MappingStoreModel();
				mappingstore.set_date(cursor.getString(0));

				MappingStoreList.add(mappingstore);
			} while (cursor.moveToNext());
		}

		db.close();
		return MappingStoreList;
	}

	public List<MessageModel> getAllMessage() {
		List<MessageModel> ResultList = new ArrayList<MessageModel>();


		SessionManagement session = new SessionManagement(ctx);
		HashMap<String, String> user = session.getUserDetails();
		
		// Select All Query
		String selectQuery = "SELECT * FROM " + TABLE_MESSAGE
				+ " WHERE stsrc <> 'D' AND user_id = '"+user.get(session.USER_ID)+"' ORDER BY date_change DESC";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				int i = 0;
				MessageModel ResultData = new MessageModel();
				ResultData.set_message_id(cursor.getInt(i));
				ResultData.set_super_user_id(cursor.getString(i += 1));
				ResultData.set_super_user_name(cursor.getString(i += 1));
				ResultData.set_user_id(cursor.getString(i += 1));
				ResultData.set_user_name(cursor.getString(i += 1));
				ResultData.set_subject(cursor.getString(i += 1));
				ResultData.set_body(cursor.getString(i += 1));
				ResultData.set_status(cursor.getString(i += 1));
				ResultData.set_date(cursor.getString(i += 1));
				ResultData.set_user_change(cursor.getString(i += 1));
				ResultData.set_date_change(cursor.getString(i += 1));
				ResultData.set_stsrc(cursor.getString(i += 1));
				ResultList.add(ResultData);
			} while (cursor.moveToNext());
		}
		db.close();
		return ResultList;
	}

	public List<PromoModel> getAllPromo(String sub_category_id) {
		List<PromoModel> ResultList = new ArrayList<PromoModel>();

		// Select All Query
		String selectQuery = "SELECT * FROM "
				+ TABLE_PROMO
				+ " WHERE '"
				+ new Date()
				+ "' BETWEEN (date(start_date) AND date(end_date))  " +
				"AND stsrc <> 'D' AND sub_category_product_id = "+sub_category_id+" ORDER BY date_change DESC";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				int i = 0;
				PromoModel ResultData = new PromoModel();
				ResultData.set_promo_id(cursor.getInt(i));
				ResultData.set_region_id(cursor.getInt(i += 1));
				ResultData.set_sub_category_product_id(cursor.getInt(i += 1));
				ResultData.set_promo_name(cursor.getString(i += 1));
				ResultData.set_start_date(cursor.getString(i += 1));
				ResultData.set_end_date(cursor.getString(i += 1));
				ResultData.set_description(cursor.getString(i += 1));
				ResultData.set_images(cursor.getString(i += 1));
				ResultData.set_user_change(cursor.getString(i += 1));
				ResultData.set_date_change(cursor.getString(i += 1));
				ResultData.set_stsrc(cursor.getString(i += 1));
				ResultList.add(ResultData);
			} while (cursor.moveToNext());
		}
		db.close();
		return ResultList;
	}

	public List<StoreRecommendationModel> getAllStoreRecommendation() {
		List<StoreRecommendationModel> ResultList = new ArrayList<StoreRecommendationModel>();

		// Select All Query
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM");
		String formattedDate = df.format(new Date());
		
		SessionManagement session = new SessionManagement(ctx);
		HashMap<String, String> user = session.getUserDetails();

		String selectQuery = "SELECT * FROM " + TABLE_STORE_RECOMMENDATION
				+ " WHERE stsrc <> 'D' AND date_recommendation LIKE '%"+formattedDate+"%' AND user_id = '"+user.get(session.USER_ID)+"'  ORDER BY date_change DESC";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				int i = 0;
				StoreRecommendationModel ResultData = new StoreRecommendationModel();
				ResultData.set_store_recommendation_id(cursor.getString(i));
				ResultData.set_store_name(cursor.getString(i += 1));
				ResultData.set_store_owner(cursor.getString(i += 1));
				ResultData.set_store_address(cursor.getString(i += 1));
				ResultData.set_contact(cursor.getString(i += 1));
				ResultData.set_description(cursor.getString(i += 1));
				ResultData.set_user_id(cursor.getString(i += 1));
				ResultData.set_status(cursor.getInt(i += 1));
				ResultData.set_date_recommendation(cursor.getString(i += 1));
				ResultData.set_image(cursor.getString(i += 1));
				ResultData.set_image2(cursor.getString(i += 1));
				ResultData.set_longtitude(cursor.getString(i += 1));
				ResultData.set_latitude(cursor.getString(i += 1));
				ResultData.set_category_id(cursor.getString(i += 1));
				ResultData.set_user_change(cursor.getString(i += 1));
				ResultData.set_date_change(cursor.getString(i += 1));
				ResultData.set_stsrc(cursor.getString(i += 1));
				ResultList.add(ResultData);
			} while (cursor.moveToNext());
		}
		db.close();
		return ResultList;
	}

	public List<StoreRecommendationModel> getAllStoreRecommendationKPI() {
		List<StoreRecommendationModel> ResultList = new ArrayList<StoreRecommendationModel>();

		// Select All Query
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM");
		String formattedDate = df.format(new Date());

		SessionManagement session = new SessionManagement(ctx);
		HashMap<String, String> user = session.getUserDetails();

		String selectQuery = "SELECT * FROM " + TABLE_STORE_RECOMMENDATION
				+ " WHERE stsrc <> 'D' AND status IN (103, 102) AND date_recommendation LIKE '%"+formattedDate+"%' AND user_id = '"+user.get(session.USER_ID)+"' ORDER BY date_change DESC";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				int i = 0;
				StoreRecommendationModel ResultData = new StoreRecommendationModel();
				ResultData.set_store_recommendation_id(cursor.getString(i));
				ResultData.set_store_name(cursor.getString(i += 1));
				ResultData.set_store_owner(cursor.getString(i += 1));
				ResultData.set_store_address(cursor.getString(i += 1));
				ResultData.set_contact(cursor.getString(i += 1));
				ResultData.set_description(cursor.getString(i += 1));
				ResultData.set_user_id(cursor.getString(i += 1));
				ResultData.set_status(cursor.getInt(i += 1));
				ResultData.set_date_recommendation(cursor.getString(i += 1));
				ResultData.set_image(cursor.getString(i += 1));
				ResultData.set_image2(cursor.getString(i += 1));
				ResultData.set_longtitude(cursor.getString(i += 1));
				ResultData.set_latitude(cursor.getString(i += 1));
				ResultData.set_category_id(cursor.getString(i += 1));
				ResultData.set_user_change(cursor.getString(i += 1));
				ResultData.set_date_change(cursor.getString(i += 1));
				ResultData.set_stsrc(cursor.getString(i += 1));
				ResultList.add(ResultData);
			} while (cursor.moveToNext());
		}
		db.close();
		return ResultList;
	}

	public List<MappingSalesToStoreTargetModel> getAllMappingSalesToStoreTargetModel() {
		List<MappingSalesToStoreTargetModel> ResultList = new ArrayList<MappingSalesToStoreTargetModel>();

		// Select All Query
		SimpleDateFormat df = new SimpleDateFormat("yyyyMM");
		String formattedDate = df.format(new Date());
		
		SessionManagement session = new SessionManagement(ctx);
		HashMap<String, String> user = session.getUserDetails();
		
		String selectQuery = "SELECT * FROM " + TABLE_MAPPING_SALES_TO_STORE_TARGET
				+ " WHERE stsrc <> 'D' AND period = '"+formattedDate+"' AND user_sales_id = '"+user.get(session.USER_ID)+"' ORDER BY date_change DESC";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				int i = 0;
				MappingSalesToStoreTargetModel ResultData = new MappingSalesToStoreTargetModel();
				ResultData.set_mapping_sales_to_store_target_id(Integer.parseInt(cursor.getString(i)));
				ResultData.set_user_sales_id(cursor.getString(i += 1));
				ResultData.set_period(cursor.getString(i += 1));
				ResultData.set_target(cursor.getString(i += 1));
				ResultData.set_user_change(cursor.getString(i += 1));
				ResultData.set_date_change(cursor.getString(i += 1));
				ResultData.set_stsrc(cursor.getString(i += 1));
				ResultList.add(ResultData);
			} while (cursor.moveToNext());
		}
		db.close();
		return ResultList;
	}

	public List<SubCategoryProductModel> getAllSubCategoryProduct(String category) {
		List<SubCategoryProductModel> ResultList = new ArrayList<SubCategoryProductModel>();

		// Select All Query
		String selectQuery = "SELECT sub_category_product_id, sub_category_product_name FROM "
				+ TABLE_SUB_CATEGORY_PRODUCT
				+ " WHERE stsrc <> 'D' AND category_product_id = "
				+ category
				+ " ORDER BY date_change DESC";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				int i = 0;
				SubCategoryProductModel ResultData = new SubCategoryProductModel();
				ResultData.set_sub_category_product_id(cursor.getInt(i));
				ResultData.set_sub_category_product_name(cursor
						.getString(i += 1));
				ResultList.add(ResultData);
			} while (cursor.moveToNext());
		}
		db.close();
		return ResultList;
	}

	public List<TypeActivitiesModel> getAllTypeActivities() {
		List<TypeActivitiesModel> ResultList = new ArrayList<TypeActivitiesModel>();

		// Select All Query
		String selectQuery = "SELECT * FROM " + TABLE_TYPE_ACTIVITIES
				+ " WHERE stsrc <> 'D' ORDER BY date_change DESC";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				int i = 0;
				TypeActivitiesModel ResultData = new TypeActivitiesModel();
				ResultData.set_type_activities_id(cursor.getInt(i));
				ResultData.set_type_activities_name(cursor.getString(i += 1));
				ResultData.set_user_change(cursor.getString(i += 1));
				ResultData.set_date_change(cursor.getString(i += 1));
				ResultData.set_stsrc(cursor.getString(i += 1));
				ResultList.add(ResultData);
			} while (cursor.moveToNext());
		}
		db.close();
		return ResultList;
	}

	public List<TypeActivitiesQuestionModel> getAllTypeActivitiesQuestion(
			int typeActivity) {
		List<TypeActivitiesQuestionModel> ResultList = new ArrayList<TypeActivitiesQuestionModel>();

		// Select All Query
		String selectQuery = "SELECT * FROM " + TABLE_TYPE_ACTIVITIES_QUESTION
				+ " WHERE stsrc <> 'D' AND type_activities_id =" + typeActivity
				+ " ORDER BY date_change DESC";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				int i = 0;
				TypeActivitiesQuestionModel ResultData = new TypeActivitiesQuestionModel();
				ResultData.set_type_activities_question_id(cursor.getInt(i));
				ResultData.setType_activities_id(cursor.getInt(i += 1));
				ResultData.set_type_activities_question_name(cursor
						.getString(i += 1));
				ResultData.set_user_change(cursor.getString(i += 1));
				ResultData.set_date_change(cursor.getString(i += 1));
				ResultData.set_stsrc(cursor.getString(i += 1));
				ResultList.add(ResultData);
			} while (cursor.moveToNext());
		}
		db.close();
		return ResultList;
	}

	public List<TypeActivitiesAnswerModel> getAllTypeActivitiesAnswer(
			int question_id) {
		List<TypeActivitiesAnswerModel> ResultList = new ArrayList<TypeActivitiesAnswerModel>();

		// Select All Query
		String selectQuery = "SELECT * FROM " + TABLE_TYPE_ACTIVITIES_ANSWER
				+ " WHERE stsrc <> 'D' AND type_activities_question_id = "
				+ question_id + " ORDER BY date_change DESC";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				int i = 0;
				TypeActivitiesAnswerModel ResultData = new TypeActivitiesAnswerModel();
				ResultData.set_type_activities_answer_id(cursor.getInt(i));
				ResultData.set_type_activities_question_id(Integer
						.parseInt(cursor.getString(i += 1)));
				ResultData.set_type_activities_answer_name(cursor
						.getString(i += 1));
				ResultData.set_user_change(cursor.getString(i += 1));
				ResultData.set_date_change(cursor.getString(i += 1));
				ResultData.set_stsrc(cursor.getString(i += 1));
				ResultList.add(ResultData);
			} while (cursor.moveToNext());
		}
		db.close();
		return ResultList;
	}

	public List<StoreRecommendationModel> getDataStoreRecommendation(String id) {
		List<StoreRecommendationModel> ResultList = new ArrayList<StoreRecommendationModel>();

		// Select All Query
		String selectQuery = "SELECT * FROM " + TABLE_STORE_RECOMMENDATION
				+ " WHERE stsrc <> 'D' AND store_recommendation_id = '" + id
				+ "' ORDER BY date_change DESC";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				int i = 0;
				StoreRecommendationModel ResultData = new StoreRecommendationModel();
				ResultData.set_store_recommendation_id(cursor.getString(i));
				ResultData.set_store_name(cursor.getString(i += 1));
				ResultData.set_store_owner(cursor.getString(i += 1));
				ResultData.set_store_address(cursor.getString(i += 1));
				ResultData.set_contact(cursor.getString(i += 1));
				ResultData.set_description(cursor.getString(i += 1));
				ResultData.set_user_id(cursor.getString(i += 1));
				ResultData.set_status(cursor.getInt(i += 1));
				ResultData.set_date_recommendation(cursor.getString(i += 1));
				ResultData.set_image(cursor.getString(i += 1));
				ResultData.set_image2(cursor.getString(i += 1));
				ResultData.set_longtitude(cursor.getString(i += 1));
				ResultData.set_latitude(cursor.getString(i += 1));
				ResultData.set_category_id(cursor.getString(i += 1));
				ResultData.set_user_change(cursor.getString(i += 1));
				ResultData.set_date_change(cursor.getString(i += 1));
				ResultData.set_stsrc(cursor.getString(i += 1));
				ResultList.add(ResultData);
			} while (cursor.moveToNext());
		}
		db.close();
		return ResultList;
	}

	public void deletelastupdate() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_LAST_UPDATE, null, null);
		db.close();
	}

	public int getMessageCount() {
		int count = 0;
		String countQuery = "SELECT  * FROM " + TABLE_MESSAGE
				+ " WHERE status = 0 AND stsrc <> 'D'";

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);

		if (cursor != null && !cursor.isClosed()) {
			count = cursor.getCount();
			cursor.close();
		}

		db.close(); // Closing database connection
		return count;
	}

	public String getLastUpdated() {
		String selectQuery = "SELECT  * FROM " + TABLE_LAST_UPDATE
				+ " ORDER BY last_update DESC LIMIT 1";

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		String date = null;

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				date = cursor.getString(0);
			} while (cursor.moveToNext());
		}

		db.close();
		return date;

	}

	public List<StoreRecommendationModel> getDataSycnStoreRegistration() {
		List<StoreRecommendationModel> ResultList = new ArrayList<StoreRecommendationModel>();

		// Select All Query
		String selectQuery = "SELECT * FROM "
				+ TABLE_STORE_RECOMMENDATION
				+ " WHERE stsrc <> 'D' AND status = 1 ORDER BY date_change DESC";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				int i = 0;
				StoreRecommendationModel ResultData = new StoreRecommendationModel();
				ResultData.set_store_recommendation_id(cursor.getString(i));
				ResultData.set_store_name(cursor.getString(i += 1));
				ResultData.set_store_owner(cursor.getString(i += 1));
				ResultData.set_store_address(cursor.getString(i += 1));
				ResultData.set_contact(cursor.getString(i += 1));
				ResultData.set_description(cursor.getString(i += 1));
				ResultData.set_user_id(cursor.getString(i += 1));
				ResultData.set_status(cursor.getInt(i += 1));
				ResultData.set_date_recommendation(cursor.getString(i += 1));
				ResultData.set_image(cursor.getString(i += 1));
				ResultData.set_image2(cursor.getString(i += 1));
				ResultData.set_longtitude(cursor.getString(i += 1));
				ResultData.set_latitude(cursor.getString(i += 1));
				ResultData.set_category_id(cursor.getString(i += 1));
				ResultData.set_user_change(cursor.getString(i += 1));
				ResultData.set_date_change(cursor.getString(i += 1));
				ResultData.set_stsrc(cursor.getString(i += 1));
				ResultList.add(ResultData);
			} while (cursor.moveToNext());
		}
		db.close();
		return ResultList;
	}

	public List<ActivitiesModel> getDataSycnActivities() {
		List<ActivitiesModel> ResultList = new ArrayList<ActivitiesModel>();

		// Select All Query
		String selectQuery = "SELECT a.* FROM "
				+ TABLE_ACTIVITIES
				+ " a JOIN mapping_store b ON a.mapping_store_id = b.mapping_store_id WHERE a.stsrc <> 'D' AND b.progress_status = 1 ORDER BY date_change DESC";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				int i = 0;
				ActivitiesModel ResultData = new ActivitiesModel();
				ResultData.set_activities_id(cursor.getString(i));
				ResultData.set_mapping_store_id(cursor.getInt(i += 1));
				ResultData.set_before_photo_store(cursor.getString(i += 1));
				ResultData.set_after_photo_store(cursor.getString(i += 1));
				ResultData.set_longtitude_sales(cursor.getString(i += 1));
				ResultData.set_latitude_sales(cursor.getString(i += 1));
				ResultData.set_status(cursor.getInt(i += 1));
				ResultData.set_user_change(cursor.getString(i += 1));
				ResultData.set_date_change(cursor.getString(i += 1));
				ResultData.set_stsrc(cursor.getString(i += 1));
				ResultData.setType_activity_id(cursor.getString(i += 1));
				i += 1;
				ResultData.setQuestion_id(cursor.getString(i += 1));
				i += 1;
				ResultData.setAnswer_id(cursor.getString(i += 1));
				i += 1;
				ResultData.setDate_checkin(cursor.getString(i += 1));
				ResultData.setDate_checkout(cursor.getString(i += 1));

				ResultList.add(ResultData);
			} while (cursor.moveToNext());
		}
		db.close();
		return ResultList;
	}

	public List<MarketSurveyModel> getAllMarketSurvey() {
		List<MarketSurveyModel> resultList = new ArrayList<MarketSurveyModel>();

		// Select All Query
		/*String selectQuery = "SELECT * FROM " + TABLE_MARKET_SURVEY
				+ " WHERE stsrc <> 'D' ORDER BY date_change DESC";*/
		String selectQuery = "SELECT * FROM " + TABLE_MARKET_SURVEY
				+ " ORDER BY date_change DESC";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				int i = 0;
				MarketSurveyModel resultData = new MarketSurveyModel();
				resultData.setMarket_intelligence_id(cursor.getString(i));
				resultData.setUser_sales_id(cursor.getString(i += 1));
				resultData.setImportir(cursor.getString(i += 1));
				resultData.setBrand_name(cursor.getString(i += 1));
				resultData.setProduct_name(cursor.getString(i += 1));
				resultData.setPhoto_one(cursor.getString(i += 1));
				resultData.setPhoto_two(cursor.getString(i += 1));
				resultData.setPhoto_three(cursor.getString(i += 1));
				resultData.setPhoto_four(cursor.getString(i += 1));
				resultData.setPhoto_five(cursor.getString(i += 1));
				resultData.setPrice(cursor.getString(i += 1));
				resultData.setComment(cursor.getString(i += 1));
				resultData
						.setDate_market_intelligence(cursor.getString(i += 1));
				resultData.setUser_change(cursor.getString(i += 1));
				resultData.setDate_change(cursor.getString(i += 1));
				resultData.setStsrc(cursor.getString(i += 1));
				resultData.setStatus(cursor.getString(i += 1));

				resultList.add(resultData);
			} while (cursor.moveToNext());
		}
		db.close();
		return resultList;
	}
	public List<MarketSurveyModel> getDataSycnMarketSurvey() {
		List<MarketSurveyModel> resultList = new ArrayList<MarketSurveyModel>();

		// Select All Query
		/*String selectQuery = "SELECT * FROM " + TABLE_MARKET_SURVEY
				+ " WHERE stsrc <> 'D' ORDER BY date_change DESC";*/
		String selectQuery = "SELECT * FROM " + TABLE_MARKET_SURVEY
				+ " WHERE status = '1' ORDER BY date_change DESC";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				int i = 0;
				MarketSurveyModel resultData = new MarketSurveyModel();
				resultData.setMarket_intelligence_id(cursor.getString(i));
				resultData.setUser_sales_id(cursor.getString(i += 1));
				resultData.setImportir(cursor.getString(i += 1));
				resultData.setBrand_name(cursor.getString(i += 1));
				resultData.setProduct_name(cursor.getString(i += 1));
				resultData.setPhoto_one(cursor.getString(i += 1));
				resultData.setPhoto_two(cursor.getString(i += 1));
				resultData.setPhoto_three(cursor.getString(i += 1));
				resultData.setPhoto_four(cursor.getString(i += 1));
				resultData.setPhoto_five(cursor.getString(i += 1));
				resultData.setPrice(cursor.getString(i += 1));
				resultData.setComment(cursor.getString(i += 1));
				resultData
						.setDate_market_intelligence(cursor.getString(i += 1));
				resultData.setUser_change(cursor.getString(i += 1));
				resultData.setDate_change(cursor.getString(i += 1));
				resultData.setStsrc(cursor.getString(i += 1));
				resultData.setStatus(cursor.getString(i += 1));

				resultList.add(resultData);
			} while (cursor.moveToNext());
		}
		db.close();
		return resultList;
	}


	public void addMarketSurvey(MarketSurveyModel resultModel) {
		int count = 0;
		SQLiteDatabase db = this.getWritableDatabase();
		SQLiteDatabase dbr = this.getReadableDatabase();

		String countQuery = "SELECT * FROM " + TABLE_MARKET_SURVEY
				+ " WHERE market_intelligence_id = " + "'"
				+ resultModel.getMarket_intelligence_id() + "'";
		Cursor cursor = dbr.rawQuery(countQuery, null);
		if (cursor != null && !cursor.isClosed()) {
			count = cursor.getCount();
			cursor.close();
		}

		ContentValues values = new ContentValues();
		if (count == 0) {
			values.put("market_intelligence_id",
					resultModel.getMarket_intelligence_id());
			values.put("user_sales_id", resultModel.getUser_sales_id());
			values.put("importir", resultModel.getImportir());
			values.put("brand_name", resultModel.getBrand_name());
			values.put("product_name", resultModel.getProduct_name());
			values.put("photo_one", resultModel.getPhoto_one());
			values.put("photo_two", resultModel.getPhoto_two());
			values.put("photo_three", resultModel.getPhoto_three());
			values.put("photo_four", resultModel.getPhoto_four());
			values.put("photo_five", resultModel.getPhoto_five());
			values.put("price", resultModel.getPrice());
			values.put("comment", resultModel.getComment());
			values.put("date_market_intelligence",
					resultModel.getDate_market_intelligence());
			values.put("user_change", resultModel.getUser_change());
			values.put("date_change", resultModel.getDate_change());
			values.put("stsrc", resultModel.getStsrc());
			values.put("status", resultModel.getStatus());
			db.insert(TABLE_MARKET_SURVEY, null, values);
		} else {

			values.put("user_sales_id", resultModel.getUser_sales_id());
			values.put("importir", resultModel.getImportir());
			values.put("brand_name", resultModel.getBrand_name());
			values.put("product_name", resultModel.getProduct_name());
			values.put("photo_one", resultModel.getPhoto_one());
			values.put("photo_two", resultModel.getPhoto_two());
			values.put("photo_three", resultModel.getPhoto_three());
			values.put("photo_four", resultModel.getPhoto_four());
			values.put("photo_five", resultModel.getPhoto_five());
			values.put("price", resultModel.getPrice());
			values.put("comment", resultModel.getComment());
			values.put("date_market_intelligence",
					resultModel.getDate_market_intelligence());
			values.put("user_change", resultModel.getUser_change());
			values.put("date_change", resultModel.getDate_change());
			values.put("stsrc", resultModel.getStsrc());
			values.put("status", resultModel.getStatus());
			db.update(TABLE_MARKET_SURVEY, values,
					"market_intelligence_id = ?", new String[] { String
							.valueOf(resultModel.getMarket_intelligence_id()) });
		}
		db.close();
	}
	
	public MarketSurveyModel getMarketSurveyByID(String market_intelligence_id) {

		String selectQuery = "SELECT * FROM " + TABLE_MARKET_SURVEY
				+ " WHERE stsrc <> 'D' AND market_intelligence_id = '"
				+ market_intelligence_id + "' ORDER BY date_change DESC";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		MarketSurveyModel resultData = new MarketSurveyModel();
		if (cursor.moveToFirst()) {
			int i = 0;
			resultData.setMarket_intelligence_id(cursor.getString(i));
			resultData.setUser_sales_id(cursor.getString(i += 1));
			resultData.setImportir(cursor.getString(i += 1));
			resultData.setBrand_name(cursor.getString(i += 1));
			resultData.setProduct_name(cursor.getString(i += 1));
			resultData.setPhoto_one(cursor.getString(i += 1));
			resultData.setPhoto_two(cursor.getString(i += 1));
			resultData.setPhoto_three(cursor.getString(i += 1));
			resultData.setPhoto_four(cursor.getString(i += 1));
			resultData.setPhoto_five(cursor.getString(i += 1));
			resultData.setPrice(cursor.getString(i += 1));
			resultData.setComment(cursor.getString(i += 1));
			resultData
					.setDate_market_intelligence(cursor.getString(i += 1));
			resultData.setUser_change(cursor.getString(i += 1));
			resultData.setDate_change(cursor.getString(i += 1));
			resultData.setStsrc(cursor.getString(i += 1));
			resultData.setStatus(cursor.getString(i += 1));
		}
		db.close();
		return resultData;
	}
}
