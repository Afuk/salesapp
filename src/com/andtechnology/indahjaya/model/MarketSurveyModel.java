package com.andtechnology.indahjaya.model;

public class MarketSurveyModel {

	String market_intelligence_id;
	String user_sales_id;
	String importir;
	String brand_name;
	String product_name;
	String photo_one;
	String photo_two;
	String photo_three;
	String photo_four;
	String photo_five;
	String price;
	String comment;
	String date_market_intelligence;
	String user_change;
	String date_change;
	String stsrc;
	String status;

	public MarketSurveyModel() {

	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMarket_intelligence_id() {
		return market_intelligence_id;
	}

	public void setMarket_intelligence_id(String market_intelligence_id) {
		this.market_intelligence_id = market_intelligence_id;
	}

	public String getUser_sales_id() {
		return user_sales_id;
	}

	public void setUser_sales_id(String user_sales_id) {
		this.user_sales_id = user_sales_id;
	}

	public String getImportir() {
		return importir;
	}

	public void setImportir(String importir) {
		this.importir = importir;
	}

	public String getBrand_name() {
		return brand_name;
	}

	public void setBrand_name(String brand_name) {
		this.brand_name = brand_name;
	}

	public String getProduct_name() {
		return product_name;
	}

	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}

	public String getPhoto_one() {
		return photo_one;
	}

	public void setPhoto_one(String photo_one) {
		this.photo_one = photo_one;
	}

	public String getPhoto_two() {
		return photo_two;
	}

	public void setPhoto_two(String photo_two) {
		this.photo_two = photo_two;
	}

	public String getPhoto_three() {
		return photo_three;
	}

	public void setPhoto_three(String photo_three) {
		this.photo_three = photo_three;
	}

	public String getPhoto_four() {
		return photo_four;
	}

	public void setPhoto_four(String photo_four) {
		this.photo_four = photo_four;
	}

	public String getPhoto_five() {
		return photo_five;
	}

	public void setPhoto_five(String photo_five) {
		this.photo_five = photo_five;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getDate_market_intelligence() {
		return date_market_intelligence;
	}

	public void setDate_market_intelligence(String date_market_intelligence) {
		this.date_market_intelligence = date_market_intelligence;
	}

	public String getUser_change() {
		return user_change;
	}

	public void setUser_change(String user_change) {
		this.user_change = user_change;
	}

	public String getDate_change() {
		return date_change;
	}

	public void setDate_change(String date_change) {
		this.date_change = date_change;
	}

	public String getStsrc() {
		return stsrc;
	}

	public void setStsrc(String stsrc) {
		this.stsrc = stsrc;
	}

}
