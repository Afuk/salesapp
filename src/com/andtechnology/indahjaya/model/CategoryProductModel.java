package com.andtechnology.indahjaya.model;

import android.os.Parcel;
import android.os.Parcelable;

public class CategoryProductModel implements Parcelable {
	private int _category_product_id;
	private String _category_product_name;
	private String _user_change;
	private String _date_change;
	private String _stsrc;

	public CategoryProductModel() {

	}

	public CategoryProductModel(Parcel in) {
		readToParcel(in);
	}

//	@Override
//	public String toString() {
//		return this._category_product_name;
//	}
	
	@Override
	public String toString() {
		return _category_product_name;
	}

	public int get_category_product_id() {
		return _category_product_id;
	}

	public void set_category_product_id(int _category_product_id) {
		this._category_product_id = _category_product_id;
	}

	public String get_category_product_name() {
		return _category_product_name;
	}

	public void set_category_product_name(String _category_product_name) {
		this._category_product_name = _category_product_name;
	}

	public String get_user_change() {
		return _user_change;
	}

	public void set_user_change(String _user_change) {
		this._user_change = _user_change;
	}

	public String get_date_change() {
		return _date_change;
	}

	public void set_date_change(String _date_change) {
		this._date_change = _date_change;
	}

	public String get_stsrc() {
		return _stsrc;
	}

	public void set_stsrc(String _stsrc) {
		this._stsrc = _stsrc;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	private void readToParcel(Parcel in) {

		_category_product_id = in.readInt();
		_category_product_name = in.readString();
		_user_change = in.readString();
		_date_change = in.readString();
		_stsrc = in.readString();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(_category_product_id);
		dest.writeString(_category_product_name);
		dest.writeString(_user_change);
		dest.writeString(_date_change);
		dest.writeString(_stsrc);

	}

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		@Override
		public CategoryProductModel createFromParcel(Parcel in) {
			return new CategoryProductModel(in);
		}

		@Override
		public CategoryProductModel[] newArray(int size) {
			return new CategoryProductModel[size];
		}
	};
}