package com.andtechnology.indahjaya.model;

public class TypeActivitiesAnswerModel {
	private int _type_activities_answer_id;
	private int _type_activities_question_id;
	private String _type_activities_answer_name;
	private String _user_change;
	private String _date_change;
	private String _stsrc;

	public TypeActivitiesAnswerModel() {

	}

	public int get_type_activities_answer_id() {
		return _type_activities_answer_id;
	}

	public void set_type_activities_answer_id(int _type_activities_answer_id) {
		this._type_activities_answer_id = _type_activities_answer_id;
	}

	public String get_type_activities_answer_name() {
		return _type_activities_answer_name;
	}

	public void set_type_activities_answer_name(String _type_activities_answer_name) {
		this._type_activities_answer_name = _type_activities_answer_name;
	}

	public String get_user_change() {
		return _user_change;
	}

	public void set_user_change(String _user_change) {
		this._user_change = _user_change;
	}

	public String get_date_change() {
		return _date_change;
	}

	public void set_date_change(String _date_change) {
		this._date_change = _date_change;
	}

	public String get_stsrc() {
		return _stsrc;
	}

	public void set_stsrc(String _stsrc) {
		this._stsrc = _stsrc;
	}
	
	/*@Override
	public String toString() {
		return "TypeActivitiesModel ["
				+ (_type_activities_answer_id != 0 ? "_type_activities_answer_id=" + _type_activities_answer_id + ", " : "")
				+ (_type_activities_answer_name != null ? "_type_activities_answer_name=" + _type_activities_answer_name + ", " : "")
				+ (_user_change != null? "_user_change=" + _user_change + ", " : "")
				+ (_date_change != null? "_date_change=" + _date_change + ", " : "")
				+ (_stsrc != null? "_stsrc=" + _stsrc + ", " : "")
				+ "]";
	}*/
	
	public void set_type_activities_question_id(int _type_activities_question_id) {
		this._type_activities_question_id = _type_activities_question_id;
	}
	public int get_type_activities_question_id() {
		return _type_activities_question_id;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return _type_activities_answer_name;
	}

}