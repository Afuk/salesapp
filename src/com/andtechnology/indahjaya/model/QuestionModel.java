package com.andtechnology.indahjaya.model;

import android.os.Parcel;
import android.os.Parcelable;

public class QuestionModel implements Parcelable {
	private int id;
	private int type_activity_id;
	private String type_activity_value;
	private int question_id;
	private String question_value;
	private int answer_id;
	private String answer_value;

	public QuestionModel() {

	}

	public QuestionModel(Parcel in) {
		readToParcel(in);
	}

	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getType_activity_id() {
		return type_activity_id;
	}

	public void setType_activity_id(int type_activity_id) {
		this.type_activity_id = type_activity_id;
	}

	public String getType_activity_value() {
		return type_activity_value;
	}

	public void setType_activity_value(String type_activity_value) {
		this.type_activity_value = type_activity_value;
	}

	public int getQuestion_id() {
		return question_id;
	}

	public void setQuestion_id(int question_id) {
		this.question_id = question_id;
	}

	public String getQuestion_value() {
		return question_value;
	}

	public void setQuestion_value(String question_value) {
		this.question_value = question_value;
	}

	public int getAnswer_id() {
		return answer_id;
	}

	public void setAnswer_id(int answer_id) {
		this.answer_id = answer_id;
	}

	public String getAnswer_value() {
		return answer_value;
	}

	public void setAnswer_value(String answer_value) {
		this.answer_value = answer_value;
	}
	
	@Override
	public String toString() {
		return "QuestionModel ["
				+ (id != 0 ? "id=" + id + ", " : "")
				+ (type_activity_id != 0 ? "type_activity_id=" + type_activity_id + ", " : "")
				+ (type_activity_value != null ? "type_activity_value=" + type_activity_value + ", " : "")
				+ (question_id != 0 ? "question_id=" + question_id + ", " : "")
				+ (question_value != null? "question_value=" + question_value + ", " : "")
				+ (answer_id != 0 ? "answer_id=" + answer_id + ", " : "")
				+ (answer_value != null? "answer_value=" + answer_value + ", " : "")

				+ "]";
	}
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	private void readToParcel(Parcel in) {

		this.id=in.readInt();
		this.type_activity_id=in.readInt();
		this.type_activity_value= in.readString();
		this.question_id=in.readInt();
		this.question_value= in.readString();
		this.answer_id=in.readInt();
		this.answer_value= in.readString();
		
		
	
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(this.id);
		dest.writeInt(type_activity_id);
		dest.writeString(this.type_activity_value);
		dest.writeInt(this.question_id);
		dest.writeString(this.question_value);
		dest.writeInt(this.answer_id);
		dest.writeString(this.answer_value);

	}

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		@Override
		public QuestionModel createFromParcel(Parcel in) {
			return new QuestionModel(in);
		}

		@Override
		public QuestionModel[] newArray(int size) {
			return new QuestionModel[size];
		}
	};

}
