package com.andtechnology.indahjaya.model;

public class StoreRecommendationModel {
	private String _store_recommendation_id;
	private String _store_name;
	private String _store_owner;
	private String _store_address;
	private String _contact;
	private String _description;
	private String _user_id;
	private int _status;
	private String _date_recommendation;
	private String _image;
	private String _image2;
	private String _longtitude;
	private String _latitude;
	private String category_id;
	private String _user_change;
	private String _date_change;
	private String _stsrc;
	private int _proggress_status;

	public StoreRecommendationModel() {

	}

	public String get_store_recommendation_id() {
		return _store_recommendation_id;
	}

	public int get_proggress_status() {
		return _proggress_status;
	}

	public void set_proggress_status(int _proggress_status) {
		this._proggress_status = _proggress_status;
	}

	public void set_store_recommendation_id(String _store_recommendation_id) {
		this._store_recommendation_id = _store_recommendation_id;
	}

	public String get_store_name() {
		return _store_name;
	}

	public void set_store_name(String _store_name) {
		this._store_name = _store_name;
	}

	public String get_store_owner() {
		return _store_owner;
	}

	public void set_store_owner(String _store_owner) {
		this._store_owner = _store_owner;
	}

	public String get_store_address() {
		return _store_address;
	}

	public void set_store_address(String _store_address) {
		this._store_address = _store_address;
	}

	public String get_contact() {
		return _contact;
	}

	public void set_contact(String _contact) {
		this._contact = _contact;
	}

	public String get_description() {
		return _description;
	}

	public void set_description(String _description) {
		this._description = _description;
	}

	public String get_user_id() {
		return _user_id;
	}

	public void set_user_id(String _user_id) {
		this._user_id = _user_id;
	}

	public int get_status() {
		return _status;
	}

	public void set_status(int _status) {
		this._status = _status;
	}

	public String get_date_recommendation() {
		return _date_recommendation;
	}

	public void set_date_recommendation(String _date_recommendation) {
		this._date_recommendation = _date_recommendation;
	}

	public String get_image() {
		return _image;
	}

	public void set_image(String _image) {
		this._image = _image;
	}

	public String get_image2() {
		return _image2;
	}

	public void set_image2(String _image2) {
		this._image2 = _image2;
	}

	public String get_longtitude() {
		return _longtitude;
	}

	public void set_longtitude(String _longtitude) {
		this._longtitude = _longtitude;
	}

	public String get_latitude() {
		return _latitude;
	}

	public void set_latitude(String _latitude) {
		this._latitude = _latitude;
	}

	public String get_user_change() {
		return _user_change;
	}

	public void set_user_change(String _user_change) {
		this._user_change = _user_change;
	}

	public String get_date_change() {
		return _date_change;
	}

	public void set_date_change(String _date_change) {
		this._date_change = _date_change;
	}

	public String get_stsrc() {
		return _stsrc;
	}

	public void set_stsrc(String _stsrc) {
		this._stsrc = _stsrc;
	}

	public void set_category_id(String category_id) {
		this.category_id = category_id;
	}

	public String get_category_id() {
		return category_id;
	}

}