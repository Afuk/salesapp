package com.andtechnology.indahjaya.model;

public class MappingSalesToStoreModel {
	private int _mapping_sales_to_store_id;
	private String _user_sales_id;
	private String _store_id;
	private String _user_change;
	private String _date_change;
	private String _stsrc;
	private String _store_name;
	private String _address;

	public MappingSalesToStoreModel() {

	}

	public String get_store_name() {
		return _store_name;
	}

	public void set_store_name(String _store_name) {
		this._store_name = _store_name;
	}

	public String get_address() {
		return _address;
	}

	public void set_address(String _address) {
		this._address = _address;
	}

	public int get_mapping_sales_to_store_id() {
		return _mapping_sales_to_store_id;
	}

	public void set_mapping_sales_to_store_id(int _mapping_sales_to_store_id) {
		this._mapping_sales_to_store_id = _mapping_sales_to_store_id;
	}

	public String get_user_sales_id() {
		return _user_sales_id;
	}

	public void set_user_sales_id(String _user_sales_id) {
		this._user_sales_id = _user_sales_id;
	}

	public String get_store_id() {
		return _store_id;
	}

	public void set_store_id(String _store_id) {
		this._store_id = _store_id;
	}

	public String get_user_change() {
		return _user_change;
	}

	public void set_user_change(String _user_change) {
		this._user_change = _user_change;
	}

	public String get_date_change() {
		return _date_change;
	}

	public void set_date_change(String _date_change) {
		this._date_change = _date_change;
	}

	public String get_stsrc() {
		return _stsrc;
	}

	public void set_stsrc(String _stsrc) {
		this._stsrc = _stsrc;
	}
}