package com.andtechnology.indahjaya.model;

public class TypeActivitiesModel {
	private int _type_activities_id;
	private String _type_activities_name;
	private String _user_change;
	private String _date_change;
	private String _stsrc;

	public TypeActivitiesModel() {

	}

	public int get_type_activities_id() {
		return _type_activities_id;
	}

	public void set_type_activities_id(int _type_activities_id) {
		this._type_activities_id = _type_activities_id;
	}

	public String get_type_activities_name() {
		return _type_activities_name;
	}

	public void set_type_activities_name(String _type_activities_name) {
		this._type_activities_name = _type_activities_name;
	}

	public String get_user_change() {
		return _user_change;
	}

	public void set_user_change(String _user_change) {
		this._user_change = _user_change;
	}

	public String get_date_change() {
		return _date_change;
	}

	public void set_date_change(String _date_change) {
		this._date_change = _date_change;
	}

	public String get_stsrc() {
		return _stsrc;
	}

	public void set_stsrc(String _stsrc) {
		this._stsrc = _stsrc;
	}

//	@Override
//	public String toString() {
//		return "TypeActivitiesModel ["
//				+ (_type_activities_id != 0 ? "_type_activities_id=" + _type_activities_id + ", " : "")
//				+ (_type_activities_name != null ? "_type_activities_name=" + _type_activities_name + ", " : "")
//				+ (_user_change != null? "_user_change=" + _user_change + ", " : "")
//				+ (_date_change != null? "_date_change=" + _date_change + ", " : "")
//				+ (_stsrc != null? "_stsrc=" + _stsrc + ", " : "")
//				+ "]";
//	}
	
	@Override
	public String toString() {
		return this._type_activities_name;
	}

}