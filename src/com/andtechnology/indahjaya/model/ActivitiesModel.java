package com.andtechnology.indahjaya.model;

public class ActivitiesModel {
	private String _activities_id;
	private int _mapping_store_id;
	private String _before_photo_store;
	private String _after_photo_store;
	private String _longtitude_sales;
	private String _latitude_sales;
	private int _status;
	private String _user_change;
	private String _date_change;
	private String _stsrc;
	
	private String type_activity_id;
	private String type_activity_value;
	private String question_id;
	private String question_value;
	private String answer_id;
	private String answer_value;
	
	private String date_checkin;
	private String date_checkout;


	public ActivitiesModel() {

	}

	public String get_activities_id() {
		return _activities_id;
	}

	public void set_activities_id(String _activities_id) {
		this._activities_id = _activities_id;
	}

	public int get_mapping_store_id() {
		return _mapping_store_id;
	}

	public void set_mapping_store_id(int _mapping_store_id) {
		this._mapping_store_id = _mapping_store_id;
	}

	public String get_before_photo_store() {
		return _before_photo_store;
	}

	public void set_before_photo_store(String _before_photo_store) {
		this._before_photo_store = _before_photo_store;
	}

	public String get_after_photo_store() {
		return _after_photo_store;
	}

	public void set_after_photo_store(String _after_photo_store) {
		this._after_photo_store = _after_photo_store;
	}

	public String get_longtitude_sales() {
		return _longtitude_sales;
	}

	public void set_longtitude_sales(String _longtitude_sales) {
		this._longtitude_sales = _longtitude_sales;
	}

	public String get_latitude_sales() {
		return _latitude_sales;
	}

	public void set_latitude_sales(String _latitude_sales) {
		this._latitude_sales = _latitude_sales;
	}

	public int get_status() {
		return _status;
	}

	public void set_status(int _status) {
		this._status = _status;
	}

	public String get_user_change() {
		return _user_change;
	}

	public void set_user_change(String _user_change) {
		this._user_change = _user_change;
	}

	public String get_date_change() {
		return _date_change;
	}

	public void set_date_change(String _date_change) {
		this._date_change = _date_change;
	}

	public String get_stsrc() {
		return _stsrc;
	}

	public void set_stsrc(String _stsrc) {
		this._stsrc = _stsrc;
	}

	public String getType_activity_id() {
		return type_activity_id;
	}

	public void setType_activity_id(String type_activity_id) {
		this.type_activity_id = type_activity_id;
	}

	public String getType_activity_value() {
		return type_activity_value;
	}

	public void setType_activity_value(String type_activity_value) {
		this.type_activity_value = type_activity_value;
	}

	public String getQuestion_id() {
		return question_id;
	}

	public void setQuestion_id(String question_id) {
		this.question_id = question_id;
	}

	public String getQuestion_value() {
		return question_value;
	}

	public void setQuestion_value(String question_value) {
		this.question_value = question_value;
	}

	public String getAnswer_id() {
		return answer_id;
	}

	public void setAnswer_id(String answer_id) {
		this.answer_id = answer_id;
	}

	public String getAnswer_value() {
		return answer_value;
	}

	public void setAnswer_value(String answer_value) {
		this.answer_value = answer_value;
	}
	public void setDate_checkin(String date_checkin) {
		this.date_checkin = date_checkin;
	}
	public String getDate_checkin() {
		return date_checkin;
	}
	public void setDate_checkout(String date_checkout) {
		this.date_checkout = date_checkout;
	}
	public String getDate_checkout() {
		return date_checkout;
	}
}