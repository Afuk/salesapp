package com.andtechnology.indahjaya.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ModelStore implements Parcelable {
	private String id;
	private String store_name;
	private String status;
	private String description;

	public ModelStore() {

	}

	public ModelStore(Parcel in) {
		readToParcel(in);
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setStore_name(String store_name) {
		this.store_name = store_name;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getId() {
		return id;
	}

	public String getStore_name() {
		return store_name;
	}

	public String getStatus() {
		return status;
	}

	public String getDescription() {
		return description;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	private void readToParcel(Parcel in) {

		id = in.readString();
		store_name = in.readString();
		status = in.readString();
		description = in.readString();

	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(id);
		dest.writeString(store_name);
		dest.writeString(status);
		dest.writeString(description);
	}

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		@Override
		public ModelStore createFromParcel(Parcel in) {
			return new ModelStore(in);
		}

		@Override
		public ModelStore[] newArray(int size) {
			return new ModelStore[size];
		}
	};
}
