package com.andtechnology.indahjaya.model;

public class SubCategoryProductModel {
	private int _sub_category_product_id;
	private int _category_product_id;
	private String _sub_category_product_name;
	private String _description;
	private String _images;
	private String _user_change;
	private String _date_change;
	private String _stsrc;

	public SubCategoryProductModel() {

	}

	public int get_sub_category_product_id() {
		return _sub_category_product_id;
	}

	public void set_sub_category_product_id(int _sub_category_product_id) {
		this._sub_category_product_id = _sub_category_product_id;
	}

	public int get_category_product_id() {
		return _category_product_id;
	}

	public void set_category_product_id(int _category_product_id) {
		this._category_product_id = _category_product_id;
	}

	public String get_sub_category_product_name() {
		return _sub_category_product_name;
	}

	public void set_sub_category_product_name(String _sub_category_product_name) {
		this._sub_category_product_name = _sub_category_product_name;
	}

	public String get_description() {
		return _description;
	}

	public void set_description(String _description) {
		this._description = _description;
	}

	public String get_images() {
		return _images;
	}

	public void set_images(String _images) {
		this._images = _images;
	}

	public String get_user_change() {
		return _user_change;
	}

	public void set_user_change(String _user_change) {
		this._user_change = _user_change;
	}

	public String get_date_change() {
		return _date_change;
	}

	public void set_date_change(String _date_change) {
		this._date_change = _date_change;
	}

	public String get_stsrc() {
		return _stsrc;
	}

	public void set_stsrc(String _stsrc) {
		this._stsrc = _stsrc;
	}

	@Override
	public String toString() {
		return _sub_category_product_name;
	}
}