
  package com.andtechnology.indahjaya.model;
  public class PromoModel {
  private int _promo_id;
  private int _region_id;
  private int _sub_category_product_id;
  private String _promo_name;
  private String _start_date;
  private String _end_date;
  private String _description;
  private String _images;
  private String _user_change;
  private String _date_change;
  private String _stsrc;

  public PromoModel() {
  
  }

public int get_promo_id() {
	return _promo_id;
}

public void set_promo_id(int _promo_id) {
	this._promo_id = _promo_id;
}

public int get_region_id() {
	return _region_id;
}

public void set_region_id(int _region_id) {
	this._region_id = _region_id;
}

public int get_sub_category_product_id() {
	return _sub_category_product_id;
}

public void set_sub_category_product_id(int _sub_category_product_id) {
	this._sub_category_product_id = _sub_category_product_id;
}

public String get_promo_name() {
	return _promo_name;
}

public void set_promo_name(String _promo_name) {
	this._promo_name = _promo_name;
}

public String get_start_date() {
	return _start_date;
}

public void set_start_date(String _start_date) {
	this._start_date = _start_date;
}

public String get_end_date() {
	return _end_date;
}

public void set_end_date(String _end_date) {
	this._end_date = _end_date;
}

public String get_description() {
	return _description;
}

public void set_description(String _description) {
	this._description = _description;
}

public String get_images() {
	return _images;
}

public void set_images(String _images) {
	this._images = _images;
}

public String get_user_change() {
	return _user_change;
}

public void set_user_change(String _user_change) {
	this._user_change = _user_change;
}

public String get_date_change() {
	return _date_change;
}

public void set_date_change(String _date_change) {
	this._date_change = _date_change;
}

public String get_stsrc() {
	return _stsrc;
}

public void set_stsrc(String _stsrc) {
	this._stsrc = _stsrc;
}
}