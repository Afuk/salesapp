package com.andtechnology.indahjaya.model;

public class MappingSalesToStoreTargetModel {
	private int _mapping_sales_to_store_target_id;
	private String _user_sales_id;
	private String _period;
	private String _target;
	private String _user_change;
	private String _date_change;
	private String _stsrc;

	public MappingSalesToStoreTargetModel() {

	}

	public int get_mapping_sales_to_store_target_id() {
		return _mapping_sales_to_store_target_id;
	}

	public void set_mapping_sales_to_store_target_id(
			int _mapping_sales_to_store_target_id) {
		this._mapping_sales_to_store_target_id = _mapping_sales_to_store_target_id;
	}

	public String get_user_sales_id() {
		return _user_sales_id;
	}

	public void set_user_sales_id(String _user_sales_id) {
		this._user_sales_id = _user_sales_id;
	}

	public String get_period() {
		return _period;
	}

	public void set_period(String _period) {
		this._period = _period;
	}

	public String get_target() {
		return _target;
	}

	public void set_target(String _target) {
		this._target = _target;
	}

	public String get_user_change() {
		return _user_change;
	}

	public void set_user_change(String _user_change) {
		this._user_change = _user_change;
	}

	public String get_date_change() {
		return _date_change;
	}

	public void set_date_change(String _date_change) {
		this._date_change = _date_change;
	}

	public String get_stsrc() {
		return _stsrc;
	}

	public void set_stsrc(String _stsrc) {
		this._stsrc = _stsrc;
	}


}