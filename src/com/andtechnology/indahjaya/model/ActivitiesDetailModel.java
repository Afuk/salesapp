
  package com.andtechnology.indahjaya.model;
  public class ActivitiesDetailModel {
  private String _activities_id;
  private int _type_activities_id;
  private int _sub_category_product_id;
  private String _description;
  private String _user_change;
  private String _date_change;
  private String _stsrc;

  public ActivitiesDetailModel() {
  
  }

public String get_activities_id() {
	return _activities_id;
}

public void set_activities_id(String _activities_id) {
	this._activities_id = _activities_id;
}

public int get_type_activities_id() {
	return _type_activities_id;
}

public void set_type_activities_id(int _type_activities_id) {
	this._type_activities_id = _type_activities_id;
}

public int get_sub_category_product_id() {
	return _sub_category_product_id;
}

public void set_sub_category_product_id(int _sub_category_product_id) {
	this._sub_category_product_id = _sub_category_product_id;
}

public String get_description() {
	return _description;
}

public void set_description(String _description) {
	this._description = _description;
}

public String get_user_change() {
	return _user_change;
}

public void set_user_change(String _user_change) {
	this._user_change = _user_change;
}

public String get_date_change() {
	return _date_change;
}

public void set_date_change(String _date_change) {
	this._date_change = _date_change;
}

public String get_stsrc() {
	return _stsrc;
}

public void set_stsrc(String _stsrc) {
	this._stsrc = _stsrc;
}
}