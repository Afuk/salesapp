package com.andtechnology.indahjaya.model;

public class MappingSalesToCategoryProductModel {
	private int _mapping_sales_to_category_product_id;
	private String _user_sales_id;
	private String _category_product_id;
	private String _user_change;
	private String _date_change;
	private String _stsrc;

	public MappingSalesToCategoryProductModel() {

	}

	public int get_mapping_sales_to_category_product_id() {
		return _mapping_sales_to_category_product_id;
	}

	public void set_mapping_sales_to_category_product_id(
			int _mapping_sales_to_category_product_id) {
		this._mapping_sales_to_category_product_id = _mapping_sales_to_category_product_id;
	}

	public String get_user_sales_id() {
		return _user_sales_id;
	}

	public void set_user_sales_id(String _user_sales_id) {
		this._user_sales_id = _user_sales_id;
	}

	public String get_category_product_id() {
		return _category_product_id;
	}

	public void set_category_product_id(String _category_product_id) {
		this._category_product_id = _category_product_id;
	}

	public String get_user_change() {
		return _user_change;
	}

	public void set_user_change(String _user_change) {
		this._user_change = _user_change;
	}

	public String get_date_change() {
		return _date_change;
	}

	public void set_date_change(String _date_change) {
		this._date_change = _date_change;
	}

	public String get_stsrc() {
		return _stsrc;
	}

	public void set_stsrc(String _stsrc) {
		this._stsrc = _stsrc;
	}
}