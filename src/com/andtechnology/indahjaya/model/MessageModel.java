package com.andtechnology.indahjaya.model;

public class MessageModel {
	private int _message_id;
	private String _super_user_id;
	private String _super_user_name;
	private String _user_id;
	private String _user_name;
	private String _subject;
	private String _body;
	private String _status;
	private String _date;
	private String _user_change;
	private String _date_change;
	private String _stsrc;

	public MessageModel() {

	}

	public int get_message_id() {
		return _message_id;
	}

	public void set_message_id(int _message_id) {
		this._message_id = _message_id;
	}

	public String get_super_user_id() {
		return _super_user_id;
	}

	public void set_super_user_id(String _super_user_id) {
		this._super_user_id = _super_user_id;
	}

	public String get_super_user_name() {
		return _super_user_name;
	}

	public void set_super_user_name(String _super_user_name) {
		this._super_user_name = _super_user_name;
	}

	public String get_user_id() {
		return _user_id;
	}

	public void set_user_id(String _user_id) {
		this._user_id = _user_id;
	}

	public String get_user_name() {
		return _user_name;
	}

	public void set_user_name(String _user_name) {
		this._user_name = _user_name;
	}

	public String get_subject() {
		return _subject;
	}

	public void set_subject(String _subject) {
		this._subject = _subject;
	}

	public String get_body() {
		return _body;
	}

	public void set_body(String _body) {
		this._body = _body;
	}

	public String get_status() {
		return _status;
	}

	public void set_status(String _status) {
		this._status = _status;
	}

	public String get_date() {
		return _date;
	}

	public void set_date(String _date) {
		this._date = _date;
	}

	public String get_user_change() {
		return _user_change;
	}

	public void set_user_change(String _user_change) {
		this._user_change = _user_change;
	}

	public String get_date_change() {
		return _date_change;
	}

	public void set_date_change(String _date_change) {
		this._date_change = _date_change;
	}

	public String get_stsrc() {
		return _stsrc;
	}

	public void set_stsrc(String _stsrc) {
		this._stsrc = _stsrc;
	}

}