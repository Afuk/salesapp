package com.andtechnology.indahjaya.model;

public class LastUpdateModel {
	// private variables
	String _date;

	// Empty constructor
	public LastUpdateModel() {

	}

	// constructor
	public LastUpdateModel(String date) {
		this._date = date;
	}

	// getting ID
	public String getDate() {
		return this._date;
	}

	// setting ID
	public void setDate(String date) {
		this._date = date;
	}
}
