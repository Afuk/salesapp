package com.andtechnology.indahjaya.model;

public class MappingStoreModel {

	private int _mapping_store_id;
	private String _user_rm_id;
	private String _user_sales_id;
	private String _store_id;
	private String _date;
	private String _user_change;
	private String _date_change;
	private String _stsrc;
	private int _progress_status;
	private String _region_id;
	private String _store_name;
	private String _address;
	private String _longtitude;
	private String _latitude;
	private String _radius;
	private String _activities_id;

	public MappingStoreModel() {
		// TODO Auto-generated constructor stub
	}

	public String get_activities_id() {
		return _activities_id;
	}

	public void set_activities_id(String _activities_id) {
		this._activities_id = _activities_id;
	}

	public int get_mapping_store_id() {
		return _mapping_store_id;
	}

	public void set_mapping_store_id(int _mapping_store_id) {
		this._mapping_store_id = _mapping_store_id;
	}

	public String get_user_rm_id() {
		return _user_rm_id;
	}

	public void set_user_rm_id(String _user_rm_id) {
		this._user_rm_id = _user_rm_id;
	}

	public String get_user_sales_id() {
		return _user_sales_id;
	}

	public void set_user_sales_id(String _user_sales_id) {
		this._user_sales_id = _user_sales_id;
	}

	public String get_store_id() {
		return _store_id;
	}

	public void set_store_id(String _store_id) {
		this._store_id = _store_id;
	}

	public String get_date() {
		return _date;
	}

	public void set_date(String _date) {
		this._date = _date;
	}

	public String get_user_change() {
		return _user_change;
	}

	public void set_user_change(String _user_change) {
		this._user_change = _user_change;
	}

	public String get_date_change() {
		return _date_change;
	}

	public void set_date_change(String _date_change) {
		this._date_change = _date_change;
	}

	public String get_stsrc() {
		return _stsrc;
	}

	public void set_stsrc(String _stsrc) {
		this._stsrc = _stsrc;
	}

	public int get_progress_status() {
		return _progress_status;
	}

	public void set_progress_status(int _progress_status) {
		this._progress_status = _progress_status;
	}

	public String get_region_id() {
		return _region_id;
	}

	public void set_region_id(String _region_id) {
		this._region_id = _region_id;
	}

	public String get_store_name() {
		return _store_name;
	}

	public void set_store_name(String _store_name) {
		this._store_name = _store_name;
	}

	public String get_address() {
		return _address;
	}

	public void set_address(String _address) {
		this._address = _address;
	}

	public String get_longtitude() {
		return _longtitude;
	}

	public void set_longtitude(String _longtitude) {
		this._longtitude = _longtitude;
	}

	public String get_latitude() {
		return _latitude;
	}

	public void set_latitude(String _latitude) {
		this._latitude = _latitude;
	}

	public String get_radius() {
		return _radius;
	}

	public void set_radius(String _radius) {
		this._radius = _radius;
	}

	public MappingStoreModel(int _mapping_store_id, String _user_rm_id,
			String _user_sales_id, String _store_id, String _date,
			String _user_change, String _date_change, String _stsrc,
			int _proggress_status, String _region_id, String _store_name,
			String _address, String _longtitude, String _latitude,
			String _radius) {
		super();
		this._mapping_store_id = _mapping_store_id;
		this._user_rm_id = _user_rm_id;
		this._user_sales_id = _user_sales_id;
		this._store_id = _store_id;
		this._date = _date;
		this._user_change = _user_change;
		this._date_change = _date_change;
		this._stsrc = _stsrc;
		this._progress_status = _proggress_status;
		this._region_id = _region_id;
		this._store_name = _store_name;
		this._address = _address;
		this._longtitude = _longtitude;
		this._latitude = _latitude;
		this._radius = _radius;
	}

}