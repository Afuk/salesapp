package com.andtechnology.indahjaya.customview;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

public class ResizeableImageView extends ImageView{

	    public ResizeableImageView(Context context, AttributeSet attrs) {
	        super(context, attrs);
	    }

	    @Override 
	    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec){
	         Drawable d = getDrawable();

	         if(d!=null){
	                 // ceil not round - avoid thin vertical gaps along the left/right edges
	                 int width = MeasureSpec.getSize(widthMeasureSpec);
	                 int height = (int) Math.ceil((float) width * (float) d.getIntrinsicHeight() / (float) d.getIntrinsicWidth());
//	                 int height= MeasureSpec.getSize(widthMeasureSpec);
//	                 int  width = (int) Math.ceil((float) height * (float) d.getIntrinsicWidth() / (float) d.getIntrinsicHeight());
	                 
	                 
	                 setMeasuredDimension(width, height);
	         }else{
	                 super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	         }
	    }

	
}
