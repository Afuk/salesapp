package com.andtechnology.indahjaya.builder;

import java.util.ArrayList;
import java.util.List;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class SpinnerBuilder {
	private Activity activity;

	public SpinnerBuilder(Activity a) {
		this.activity = a;
	}

	public void createSpinner(int sId, List<?> mylist) {
//		mylist.add(0, "Please Select");
		ArrayAdapter<?> adapter = new ArrayAdapter(activity, android.R.layout.simple_spinner_item, mylist);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		((Spinner) activity.findViewById(sId)).setAdapter(adapter);
	}

	public void createSpinner(int sId, String[] array) {
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, array);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		((Spinner) activity.findViewById(sId)).setAdapter(adapter);
	}
	
	public void createSpinner(int sId, ArrayList<String> array) {
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, array);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		((Spinner) activity.findViewById(sId)).setAdapter(adapter);
	}
}
